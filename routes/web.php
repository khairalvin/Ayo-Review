<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::get('/aboutus', 'HomeController@aboutus');
Route::get('/syarat-ketentuan', 'HomeController@syaratketentuan');
Route::get('/kebijakan-privasi', 'HomeController@kebijakanprivasi');
Route::get('/news', 'BeritaUserController@index')->name('news');
Route::get('/news/detail/{id}', 'BeritaUserController@detail')->name('news');
Route::post('/search','HomeController@search');
Auth::routes();

// ACCESS USER PROFILE
Route::get('/profile/{id}','ProfileController@index');
Route::get('/profile_media/{id}','ProfileController@media');
Route::get('/profile_follower/{id}','ProfileController@follower');
Route::get('/profile_following/{id}','ProfileController@following');

// OAUTH
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

// EMAIL VERIFICATION
Route::get('verifyEmailFirst','Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');
Route::get('verify/{email}/{verifyToken}','Auth\RegisterController@sendEmailDone')->name('sendEmailDone');

// ACCOUNT RESET
Route::group(['as' => 'user-password-'], function () {
    Route::post('/password', ['as' => 'request', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::post('/password/reset', ['as' => 'reset', 'uses' => 'Auth\ResetPasswordController@reset']);
});

// KATEGORI USER
Route::get('/kategorireview/{id}','KategoriController@kategorireview');

// KATEGORI USER
Route::get('/subkategorireview','SubkategoriController@subkategorireview');

// REVIEW
Route::get('/reviewadmin/{id}','ReviewUserController@reviewadmin');
Route::get('/reviewadmin/{id}/{user}','ReviewUserController@getReviewUser');


// NEED LOGIN
Route::middleware('auth')->group(function () {
    
    Route::get('/markas', 'AdminController@index');
    Route::get('/logout', 'HomeController@logout');


    //PANEL ADMIN CONTROLLER
    Route::resource('kategori','KategoriController');
    Route::resource('subkategori','SubkategoriController');

    // ADMIN REVIEW
    Route::group(['as' => 'admin-review-'], function () {
        Route::get('/review', ['as' => 'index', 'uses' => 'ReviewController@index']);
        Route::get('/review/create', ['as' => 'create', 'uses' => 'ReviewController@create']);
        Route::post('/review/create', ['as' => 'store', 'uses' => 'ReviewController@store']);
        Route::get('/review/{id?}/edit', ['as' => 'edit', 'uses' => 'ReviewController@edit']);
        Route::post('/review/{id?}/edit', ['as' => 'update', 'uses' => 'ReviewController@update']);
    });

    // ADMIN INDIKATOR
    Route::group(['as' => 'admin-indikator-'], function () {
        Route::get('/indikator', ['as' => 'index', 'uses' => 'IndikatorController@index']);
        Route::get('/indikator/create', ['as' => 'create', 'uses' => 'IndikatorController@create']);
        Route::post('/indikator/create', ['as' => 'store', 'uses' => 'IndikatorController@store']);
        Route::get('/indikator/{id?}/edit', ['as' => 'edit', 'uses' => 'IndikatorController@edit']);
        Route::post('/indikator/{id?}/edit', ['as' => 'update', 'uses' => 'IndikatorController@update']);
    });

    Route::group(['as' => 'admin-brand-'], function () {
        Route::get('/brands', ['as' => 'index', 'uses' => 'BrandController@index']);
        Route::get('/brands/create', ['as' => 'create', 'uses' => 'BrandController@create']);
        Route::post('/brands/create', ['as' => 'store', 'uses' => 'BrandController@store']);
        Route::get('/brands/{id?}/edit', ['as' => 'edit', 'uses' => 'BrandController@edit']);
        Route::post('/brands/{id?}/edit', ['as' => 'update', 'uses' => 'BrandController@update']);
    });


    // USER REVIEW
    Route::group(['as' => 'user-review-'], function () {
        Route::post('/review-user/create', ['as' => 'store', 'uses' => 'ReviewUserController@storeUser']);
    });
    
    //  BERITA USER
    Route::resource('berita','BeritaController');
    Route::get('/berita_top-news','BeritaController@top_news');
    Route::post('/berita_top-news','BeritaController@top_news_post');
    Route::get('/berita_search','BeritaController@search');

    // IKLAN
    Route::resource('iklan','IklanController');
    
    // PROFILE CONTROLLER
    Route::get('/profile_pass','ProfileController@pass');
    Route::get('/profile_edit','ProfileController@edit');

    // ACCOUNT SETTINGS
    Route::post('/change_password','AkunController@change_password');

    // ACTION
    Route::post('/updateakun','AkunController@edit');
    Route::post('/change_password','AkunController@change_password');

    // FOLLOWING
    Route::post('/follow','FollowController@follow');
    Route::post('/unfollow','FollowController@unfollow');

    // SARAN & REQUEST
    Route::post('/sendSaran','RequestController@sendSaran');
    Route::get('/getSaran','RequestController@getSaran');
    Route::post('/sendRequest','RequestController@sendRequest');
    Route::get('/getRequest','RequestController@getRequest');
    Route::post('/toRead','RequestController@toRead');

    // ROLE & ROLE MAPPING
    Route::group(['as' => 'role-'], function () {
        Route::get('/role', ['as' => 'index', 'uses' => 'RoleController@index']);
        Route::get('/role/create', ['as' => 'create', 'uses' => 'RoleController@create']);
        Route::post('/role/create', ['as' => 'store', 'uses' => 'RoleController@store']);
        Route::get('/role/{id?}/edit', ['as' => 'edit', 'uses' => 'RoleController@edit']);
        Route::put('/role/{id?}/edit', ['as' => 'update', 'uses' => 'RoleController@update']);
        Route::delete('/role/{id?}/delete', ['as' => 'delete', 'uses' => 'RoleController@destroy']);
    });

    Route::group(['as' => 'role-mapping-'], function () {
        Route::get('/role/mapping', ['as' => 'index', 'uses' => 'RoleController@indexMapping']);
        Route::get('/role/mapping/{id?}/{role_id?}/edit', ['as' => 'edit', 'uses' => 'RoleController@editMapping']);
        Route::put('/role/mapping/{id?}/{role_id?}/edit', ['as' => 'update', 'uses' => 'RoleController@updateMapping']);
    });
    
    // HELPER
    Route::get('/kategori-helper', 'HelperController@kategori', [ 'term' => '']);
    Route::get('/brand-helper', 'HelperController@brand', [ 'term' => '']);
    Route::get('/subkategori-helper/{id?}', 'HelperController@subkategori', [ 'term' => '']);
});

