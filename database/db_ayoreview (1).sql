-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Jul 2018 pada 09.00
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ayoreview`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `konten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tags` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `viewer` int(10) NOT NULL DEFAULT '0',
  `urutan_topnews` int(10) NOT NULL DEFAULT '0',
  `urlname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `judul`, `isActive`, `status`, `img`, `konten`, `kategori_id`, `user_id`, `tags`, `created_at`, `updated_at`, `viewer`, `urutan_topnews`, `urlname`) VALUES
(1, 'Tiga lansiran terbaru Seiko Black Series Prospex Limited Edition', 1, 'oke', 'Tiga lansiran terbaru Seiko Black Series Prospex Limited Edition.jpg', '<p>Seiko secara diam diam memperkenalkan jajaran limited editionnya yang bernama &ldquo;Black Series&rdquo; Prospex dive watches, yang dikeluarkan pada kuarter pertama 2018. Ketika mendengar kata &ldquo;Black Series&rdquo; saya teringat dengan seri Seiko black BFK dan black urchin yang bernuansa serba hitam namun tentu tidak semenarik ketiga seri baru ini. Seiko menghadirkan seri spesial bernuansa serba hitam dengan marking pada dial yang terkesan tanned akibat sering terpapar sinar matahari yang seolah olah memberi kesan vintage . Seiko berkata bahwa Black Series ini terinspirasi dari diving malam hari dan didesain untuk mempermudah proses pembacaan waktu ketika melihat jam. Apapun maksud dan tujuannya saya rasa Seiko berhasil menarik perhatian dari para pencinta Seiko yang tentu berminat menambahkan koleksinya.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/seiko black 1.jpg\" alt=\"\" width=\"483\" height=\"483\" /></p>\r\n<p>Seri yang mendapat edisi terbatas &ldquo;Black series&rdquo; ini pertama adalah Seiko &ldquo;Turtle&rdquo; reissue dengan kode SRPC49K1. Secara keseluruhan baik mesin dan penggunaan material sama persis dengan seri standarnya. Dengan ukuran 45mm dan memiliki kemampuan menyelam 200m, bezel yang berputar satu arah dan scew down crown. Menggunakan kaliber mesin 4R36 yang memiliki bitrate 21.600 bph (bit per hour) dan power reserve selama 41 jam. Yang menjadi perbedaan tentu casenya yang diberi coating warna hitam yang menurut saya terkesan gagah.</p>\r\n<p style=\"text-align: center;\"><img src=\"/filemanager/photos/1/seiko black 2.jpg\" alt=\"\" width=\"483\" height=\"483\" /></p>\r\n<p>Seri kedua merupakan seri dengan mesin Solar, Seiko Prospex Solar Diver SNE493P1, dengan dimater yang sedikit lebih kecil dari Seiko Turtle yaitu sebesar 43mm SNE493P1 ini memiliki kemampuan menyelam 200m, bezel yang berputar satu arah&nbsp; dan screw down crown. Dengan mesin Solar V157 memiliki power reserve selama 10 bulan.</p>\r\n<p style=\"text-align: center;\"><img src=\"/filemanager/photos/1/seiko black 3.jpg\" alt=\"\" width=\"483\" height=\"483\" /></p>\r\n<p>Terakhir seri ketiga yang juga bermesin Solar adalah Seiko Prospex Solar Diver SSC673P1, dengan spesifikasi yang hampir sama dengan SNE493P1 berdiameter 43mm, kemampuan menyelam 200m, bezel perputar satu arah dan screw down crown. Menggunakan mesin solar yang juga sama V157 dengan power reserve 6 bulan. Namun dengan fitur tambahan chronograph.</p>\r\n<p>Ketiga jam edisi terbatas ini masing masing dibandrol seharga 5 jutaan untuk SRPC49K1, 3 jutaan untuk SNE493P1 dan terakhir juga seharga 3 jutaan untuk SSC673P1. Bagaimana apakah anda sudah memilikinya? Karena ketiga seri ini terbatas jadi dipastikan tidak banyak jumlahnya.</p>\r\n<p><strong>Sumber</strong> : https://www.ablogtowatch.com/seiko-black-series-prospex-limited-edition-dive-watches/</p>\r\n<p>Seiko secara diam diam memperkenalkan jajaran limited editionnya yang bernama &ldquo;Black Series&rdquo; Prospex dive watches, yang dikeluarkan pada kuarter pertama 2018. Ketika mendengar kata &ldquo;Black Series&rdquo; saya teringat dengan seri Seiko black BFK dan black urchin yang bernuansa serba hitam namun tentu tidak semenarik ketiga seri baru ini. Seiko menghadirkan seri spesial bernuansa serba hitam dengan marking pada dial yang terkesan tanned akibat sering terpapar sinar matahari yang seolah olah memberi kesan vintage . Seiko berkata bahwa Black Series ini terinspirasi dari diving malam hari dan didesain untuk mempermudah proses pembacaan waktu ketika melihat jam. Apapun maksud dan tujuannya saya rasa Seiko berhasil menarik perhatian dari para pencinta Seiko yang tentu berminat menambahkan koleksinya.</p>\r\n<p>Seri yang mendapat edisi terbatas &ldquo;Black series&rdquo; ini pertama adalah Seiko &ldquo;Turtle&rdquo; reissue dengan kode SRPC49K1. Secara keseluruhan baik mesin dan penggunaan material sama persis dengan seri standarnya. Dengan ukuran 45mm dan memiliki kemampuan menyelam 200m, bezel yang berputar satu arah dan scew down crown. Menggunakan kaliber mesin 4R36 yang memiliki bitrate 21.600 bph (bit per hour) dan power reserve selama 41 jam. Yang menjadi perbedaan tentu casenya yang diberi coating warna hitam yang menurut saya terkesan gagah.</p>\r\n<p>Seri kedua merupakan seri dengan mesin Solar, Seiko Prospex Solar Diver SNE493P1, dengan dimater yang sedikit lebih kecil dari Seiko Turtle yaitu sebesar 43mm SNE493P1 ini memiliki kemampuan menyelam 200m, bezel yang berputar satu arah&nbsp; dan screw down crown. Dengan mesin Solar V157 memiliki power reserve selama 10 bulan.</p>\r\n<p>Terakhir seri ketiga yang juga bermesin Solar adalah Seiko Prospex Solar Diver SSC673P1, dengan spesifikasi yang hampir sama dengan SNE493P1 berdiameter 43mm, kemampuan menyelam 200m, bezel perputar satu arah dan screw down crown. Menggunakan mesin solar yang juga sama V157 dengan power reserve 6 bulan. Namun dengan fitur tambahan chronograph.</p>\r\n<p>Ketiga jam edisi terbatas ini masing masing dibandrol seharga 5 jutaan untuk SRPC49K1, 3 jutaan untuk SNE493P1 dan terakhir juga seharga 3 jutaan untuk SSC673P1. Bagaimana apakah anda sudah memilikinya? Karena ketiga seri ini terbatas jadi dipastikan tidak banyak jumlahnya.</p>\r\n<p><strong>Sumber</strong> : https://www.ablogtowatch.com/seiko-black-series-prospex-limited-edition-dive-watches/</p>', 4, 1, '#NNN', '2018-05-12 01:49:20', '2018-05-22 07:43:08', 0, 1, ''),
(2, 'Apa itu Hi-Res Audio dan Perbandingan antar Format Audio', 1, 'oke', 'Apa itu Hi-Res Audio dan Perbandingan antar Format Audio.jpg', '<p>Jika kamu suka mendengarkan lagu dengan kualitas suara yang baik, mungkin istilah Hi-Res Audio ini tak asing lagi bagimu. Dua tahun belakangan ini memang semakin kita dengar dan lihat istilah Hi-Res Audio digunakan baik di perangkat audio ataupun muncul logonya saat menyetel sebuah lagu berformat 24 bit atau lebih. Sebenarnya apa itu Hi-Res Audio? Mari kita bahas bersama.</p>\r\n<p><strong>Awal Mula</strong></p>\r\n<p><strong><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/JEITA.png\" alt=\"\" width=\"238\" height=\"114\" /></strong></p>\r\n<p>Hi-Resolution Audio atau sering disingkat Hi-Res Audio sebenarnya adalah istilah marketing yang digunakan oleh para produsen audio yang menandakan perangkat mereka mampu memaksimalkan rekaman format rekaman 24 bit atau lebih. Istilah ini muncul dari sebuah asosiasi bernama JEITA di Jepang yang memberikan standar penggunaan Hi-Res Audio pada tahun 2014.</p>\r\n<p><strong>Apa itu Hi-Res Audio?</strong></p>\r\n<p style=\"text-align: center;\"><strong><img src=\"/filemanager/photos/1/hires.jpg\" alt=\"\" width=\"127\" height=\"127\" /></strong></p>\r\n<p>Seperti namanya Hi-Res Audio menyajikan pengalaman mendengarkan musik yang lebih baik ketimbang musik pada umumnya. Pengalaman yang mungkin kamu rasakan adalah mendengarkan instrumen instrumen dengan lebih jelas dan beberapa detail kecil dalam suatu musik mulai muncul yang biasanya sulit terdengar.</p>\r\n<p>Hi-Res Audio memiliki standar rekaman digital minimal berformat 96kHz/24bit dengan ekstensi file yang dapat berupa FLAC dan WAV.</p>\r\n<p>Dengan kelebihan Hi-Res Audio seperti yang disebutkan sebelumnya, kelemahan utamanya tentu saja ukuran file yang besar. File Hi-Res audio biasanya berkisar 40MB.</p>\r\n<p>Jadi sudah cukup jelas bahwa Hi-Res Audio hanya sekedar ikon marketing untuk lagu yang berformat minimal 96kHz/24bit dan perangkat audio yang dapat memutarnya.</p>\r\n<p><strong>Format Audio</strong></p>\r\n<p style=\"text-align: center;\"><img src=\"/filemanager/photos/1/formataudio.jpg\" alt=\"\" width=\"481\" height=\"165\" /></p>\r\n<p>Sebelumnya tadi kita sudah sedikit menyinggung format FLAC dan WAV. Sebenarnya apa yang membuat format tersebut mampu menyajikan musik secara Hi-Res ? Berikut penjelasan beberapa format audio yang sering kita dengar</p>\r\n<p>Losless Format</p>\r\n<p>Pada bagian ini format musik yang digunakan bersifat losless yang berarti kualitas musik yang asli dari CD tetap sama tidak ada yang hilang. Beberapa format losless antara lain</p>\r\n<ul>\r\n<li>WAV</li>\r\n</ul>\r\n<p>Format standar yang digunakan file musik didalam CD 16bit/44.1kHz. Ukuran file yang cukup besar dan kemampuan penyimpanan metada (nama artis, album, cover art dll) yang kurang baik</p>\r\n<ul>\r\n<li>FLAC</li>\r\n</ul>\r\n<p>Format kompresi losless yang digunakan untuk file musik yang mendukung baik 16/24 bit. Mampu menyimpan metada lebih baik dari WAV</p>\r\n<ul>\r\n<li>ALAC</li>\r\n</ul>\r\n<p>Format kompresi losless milik Apple mirip dengan FLAC namun lebih user friendly dengan perangkat Apple yang selalu memiliki segala ke eksklusifannya</p>\r\n<ul>\r\n<li>DSD</li>\r\n</ul>\r\n<p>Format yang digunakan pada &ldquo;Super Audio CD&rdquo;. Format yang lebih baik lagi ketimbang FLAC / WAV dalam menyajikan kualitas suara. Dikarenakan sampling ratenya yang sangat tinggi 1bit/5.6MHz.</p>\r\n<p>Lossy Format</p>\r\n<p>Format musik ini mengutamakan ukuran file yang lebih ringkas dengan kompresi data yang cukup signifikan sehingga menyebabkan penurunan kualitas suara dari sumbernya. Beberapa formatnya antara lain</p>\r\n<ul>\r\n<li>MP3</li>\r\n</ul>\r\n<p>Format musik yang sangat populer karena ukuran filenya yang sangat kecil sehingga cocok untuk disimpan pada perangkat yang memiliki keterbatasan penyimpanan. Seperti sudah disinggung sebelumnya, MP3 merupakan format lossy yang kurang baik dalam hal kualitas suara. MP3 pun memiliki beberapa standar rate yaitu 128/256/320 kbps, semakin tinggi ratenya semakin membaik kualitas suaranya</p>\r\n<ul>\r\n<li>AAC</li>\r\n</ul>\r\n<p>Format musik alternatif dari MP3 buatan Apple dengan bit rate 256kbps</p>\r\n<p><strong>Apakah Hi-Res jauh lebih baik?</strong></p>\r\n<p>Jawaban ini tentunya hanya kamu sendiri yang bisa menjawab, apakah kamu mampu mendengarkan perbedaannya? Tentu saja perangkat audio yang mumpunipun akan semakin membantu kita mendengarkan perbedaan kualitas diantaranya.</p>\r\n<p>Perangkat apa yang kita butuhkan? Jika kamu hanya bermodalkan PC kamu bisa menggunakan Music Player seperti Foobar2000. Namun jika ingin lebih maksimal lagi pengalaman mendengarkan musiknya kamu bisa mempertimbangkan membeli perangkat Digital Audio Player (DAP) yang sudah mendukung file musik 24 bit.</p>\r\n<p>&nbsp;</p>\r\n<p>Sumber :</p>\r\n<ul>\r\n<li>https://www.whathifi.com/advice/high-resolution-audio-everything-you-need-to-know</li>\r\n<li>https://www.jas-audio.or.jp/english/hi-res-logo-en</li>\r\n</ul>', 3, 1, '#NNN', '2018-05-12 01:54:32', '2018-05-22 07:43:08', 0, 2, ''),
(3, 'Seiko Menghetikan seri SARBnya yang Fenomenal', 1, 'oke', 'Seiko Menghetikan seri SARBnya yang Fenomenal.jpg', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/seiko sarb 3.jpg\" alt=\"\" width=\"483\" /></p>\r\n<p>Seiko beberapa bulan lalu memberikan pengumuman yang mengejutkan sekaligus mungkin berita yang buruk bagi penggemar Seiko khususnya penyuka seri Dress watch. Karena seiko mengumumkan jajaran seri beberapa seri SARBnya yang populer yaitu SARB017, SARB033, SARB035.</p>\r\n<p>Seperti lagu Peterpan &ndash; tak ada yang abadi, mungkin ini adalah saatnya Seiko untuk melalukan inovasi kembali dengan tidak melihat kebelakang. Seri SARB merupakan seri dress watch yang memiliki value for money yang sangat baik. Dengan kisaran harga 5 &ndash; 6 juta kita sudah bisa merasakan kualitas poor man Grand Seiko. Dengan finishing dan detail detail kecil pada seri SARB ditambah mesin kaliber 6R yang memiliki beatrate 21.600 bph serta kristal saphire tentu harga yang ditawarkan sangat amat worthed.</p>\r\n<p>Seri SARB sudah keluar sejak tahun akhir tahun 2007 dengan debut SARB017 pada November 2007, SARB033 / SARB035 pada Juni 2008 ketiga tipe jam tersebut sudah sangat dikenal para pencinta Seiko selama 10 tahun.</p>\r\n<p>Saya sendiri sangat menyayangkan keputusan ini, karena ketiga jam tersebut sangatlah ikonik dan merupakan rekomendasi para pencinta jam untuk dress watch yang harganya masih terjangkau. Namun saya berharap apa yang akan Seiko berikan sebagai pengganti ke tiga seri tersebut akan lebih baik lagi. Tentunya tidak hanya sekedar kenaikan harga pada seri penggantinya, namun kualitas dan finishing yang sebanding dengan apa yand diberikan.<strong>&nbsp;</strong></p>\r\n<p><strong>&nbsp;<img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/seiko sarb2.jpg\" alt=\"\" width=\"545\" height=\"363\" /></strong></p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong>Sumber : </strong>https://www.seiyajapan.com/blogs/news/sayonara</p>', 4, 1, '#NNN', '2018-05-12 01:58:47', '2018-05-22 07:43:08', 0, 3, ''),
(5, 'Seiko Prospex Special Edition PADI SPB071J1', 1, 'oke', 'Seiko Prospex Special Edition PADI SPB071J1.jpg', '<p>Seiko kembali memperkenalkan jajaran jam diver edisi spesial terbarunya yang menjalin kerjasama dengan PADI (Profesional Assoction of Diving Instructor). Tipe terbaru itu berkode SPB071J1. Kerjasama Seiko dan PADI sudah terjalin sejak tahun 2016 dengan dikeluarkannya edisi spesial dari Seiko New Turtle (Turtle Re issue) SRPA21 dan Seiko Kinetic GMT SUN065. Menurut Seiko edisi edisi spesial PADI ini adalah wujud kerjasama mereka dalam mendukung program PADI untuk menjaga kebersihan lingkungan laut.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/seiko blue1.jpg\" alt=\"\" width=\"333\" height=\"327\" /></p>\r\n<p>Kembali ke seri SPB071J1, jam ini sendiri juga merupakan salah satu re-issue jam legendaris Seiko 62MAS yang merupakan jam diver pertama mereka yang dikeluarkan pada tahun 1965. Seri SPB071J1 memiliki beberapa fitur yang bisa dikatakan spesial. Pertama tentu saja sebagaimana seri jam Seiko x Padi sebelumnya terdapat logo PADI pada dialnya. Penggunaan kombinasi merah &ndash; biru juga tak absen pada semua jam edisi spesial Seiko x PADI. Pada model SPB071J1 ini penunjuk menitnya memiliki tepian warna merah menyala. Warna merah juga terdapat di chapter ring jam 9, 12, 3 dan 6. Dialnya berwarna biru gradasi dari terang ke gelap dan jika diperhatikan lebih jelas lagi dialnya memiliki motif &ldquo;^&rdquo; yang menjadi fitur yang paling menonjol. Warna biru metalik juga hadir pada cincil luar rotating bezelnya.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/seiko blue2.jpg\" alt=\"http://localhost:8000/filemanager/photos/1/seiko blue1.jpg\" width=\"332\" height=\"326\" /></p>\r\n<p>Seperti seri high end Seiko Prospex lainnya, seri SPB071J1 ini menggunakan mesin kaliber otomatis 6r15. Mesin ini memiliki beat rate 21.600 bph (beat per hour) dengan power reserve 50 jam dan memiliki fitur &ldquo;hacked&rsquo; dan &ldquo;hand-wound&rdquo;. Seri SPB071J1 memiliki diameter 42mm, mampu menyelam hingga kedalaman 200m dan memakai kristal saphire tahan gores untuk kacanya. Material Lumibrite yang sangat terang khas semua Seiko Diver tentu tak absen pada seri SPB071J1 ini. Semua marker dan handnya berwarna lume biru sangat terang. Terakhir seri SPB071J1 ini menggunakan strap silikon Z22 yang sangat nyaman berwarna biru navy.</p>\r\n<p>Pada akhirnya seri Seiko Prospex Special Edition PADI SPB071J1 ini tentu dibrandrol dengan harga yang sedikit premium dari seri non PADInya SPB053. Saat perkenalannya SPB071J1 ini dibandrol dengan harga retail &euro;900 yang di Indonesia sendiri mungkin bisa berkisar 12 jutaan. Bagaimana apakah anda tertarik untuk memasukkan kedalam daftar koleksi Seiko anda?</p>\r\n<p><strong>Sumber</strong> : https://www.ablogtowatch.com/seiko-prospex-special-edition-padi-spb071j1-watch/</p>', 4, 1, '#NNN', '2018-05-12 02:09:14', '2018-05-12 02:09:14', 0, 0, ''),
(6, 'Tipe Tipe Driver pada Earphone', 1, 'oke', 'Tipe Tipe Driver pada Earphone Yang mana Favoritmu.png', '<p>Pernah tidak sengaja earphone kamu rusak dan melihat benda ini didalamnya? Naah benda inilah yang dinamakan driver. Jika kamu mulai masuk lebih dalam kedalam dunia Audio, ternyata driver memiliki beragam bentuk dan ukuran. Untuk lebih jelasnya mari kita bahas.</p>\r\n<p><strong>Apa itu Driver ?</strong></p>\r\n<p>Driver merupakan bagian didalam earphone yang pada dasarnya terdiri dari magnet, koil suara dan diafragma. Driver inilah yang bertugas mengubah sinyal elektrik menjadi suara yang kamu dengar. Jenis driverpun beragam yang tentunya memiliki suara khasnya masing masing.&nbsp;</p>\r\n<p><strong>Ada Berapa Macam Driver ?</strong></p>\r\n<ol>\r\n<li>Dynamic Driver</li>\r\n</ol>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/dynamicdriver.jpg\" alt=\"\" /></p>\r\n<p>Dynamic driver adalah driver yang paling umum dan tak asing lagi karena digunakan mayoritas earbud dan earphone. Seperti namanya yang berarti dinamis, driver jenis ini diciptakan untuk memenuhi semua range frekuensi suara. Yang mana dapat menciptakan suara yang cenderung netral dan mungkin tidak terlalu detail bagi sebagian orang.</p>\r\n<p>Pada desain dynamic driver, diafragmanya menempel langsung ke koil suara. Saat ada arus melewatinya, koil suara bergerak diantara dua magnet yang menyebabkan diafragmanya ikut bergerak dan menghasilkan suara.</p>\r\n<p>Desain dynamic driver juga sering mengutamakan aliran udara (ventilasi) dan hal inilah yang menyebabkan respon bass dari dynamic driver lebih baik dari balanced armature.</p>\r\n<ol start=\"2\">\r\n<li>Balanced Armature (BA)</li>\r\n</ol>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/balance.jpg\" alt=\"http://localhost:8000/filemanager/photos/1/dynamicdriver.jpg\" width=\"306\" height=\"306\" /></p>\r\n<p>Balanced armature (BA) adalah driver yang ukurannya sangat kecil jika dibandingkan driver lainnya. Karena ukurannya yang kurang lebih hanya sebesar biji beras, driver ini sering digunakan untuk In ear monitor. Tidak seperti dynamic driver yang dirancang untuk mencakup semua range frekuensi suara. Sebuah driver BA sering dituning untuk unggul pada frekuensi tertentu. Oleh karena itulah IEM dapat terdiri dari gabungan beberapa driver BA yang setiap drivernya dapat fokus pada frekuensi low, middle dan high.</p>\r\n<p>Pada desain driver BA, arus yang masuk akan melewati koil yang membungkus armature. Posisi koil terletak diantara dua magnet dan perubahan arus akan menyebabkan pergerakan antara koil dan magnet. Diafragma yang terhubung ke armature inilah yang menghasilkan suara.</p>\r\n<ol start=\"3\">\r\n<li>Planar</li>\r\n</ol>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/planar.jpg\" alt=\"http://localhost:8000/filemanager/photos/1/dynamicdriver.jpg\" width=\"580\" height=\"200\" /></p>\r\n<p>Tipe driver ini biasanya ada pada headphone yang cukup high end. Desain diafragmanya yang diapit diantara magnet. Dan terdapt sebuah kabel yang dibuat melewati diafragmanya dengan bentuk yang dinamakan &ldquo;serpentine&rdquo; yang dapat menghasilkan medan elektromagnetik yang kemudia berinteraksi dengan medan magnet yang menghasilkan suara.</p>\r\n<p>Terdengar rumit bukan? Namun kerumitan konstruksi driver tersebut menghasilkan respon bass dan soundstage yang lebih baik dari dynamic driver. Driver planar biasanya dipilih oleh orang orang audiophile yang kritis dengan apa yang mereka dengarkan.</p>\r\n<ol start=\"4\">\r\n<li>Electrostatic</li>\r\n</ol>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/electro.jpg\" alt=\"\" /></p>\r\n<p>Jika anda rasa driver planar sudah sangat rumit dan nyeleneh, maka seperti kata pepatah masih ada langit diatas langit. Elekctrostatic driver adalah driver yang menggunakan difragma yang tipis dan teraliri arus listrik. Diafragma tersebut terletak diantara dua elektroda. Butuh ampli khusus untuk mengeluarkan suaranya.</p>\r\n<p>&nbsp;</p>\r\n<p>Desain driver yang unik itu dapat menghasilkan frekuensi suara yang terdengar sangat akurat sehingga menciptakan kesan soundstage yang terkesan nyata. Dan desain driver ini tentu saja perlu dibayar dengan harga yang luar biasa mahal.</p>\r\n<p>&nbsp;</p>\r\n<ol start=\"5\">\r\n<li>Hybrid</li>\r\n</ol>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/filemanager/photos/1/hybrid.jpg\" alt=\"http://localhost:8000/filemanager/photos/1/dynamicdriver.jpg\" width=\"580\" height=\"316\" /></p>\r\n<p>Jika ingin menciptakan suatu produk yang memiliki keunggulan masing masing dynamic driver dan balanced armature maka jawabannya adalah menggabungkan keduanya sehingga menjadi produk hybrid.</p>\r\n<p>&nbsp;</p>\r\n<p>Sumber :</p>\r\n<ul>\r\n<li>https://www.dominiontechreviews.com/dynamic-planar-magnetic-electrostatic-headphones-best/</li>\r\n<li>https://www.rtings.com/headphones/learn/dynamic-vs-planar-magnetic</li>\r\n</ul>', 3, 1, '#NNN', '2018-05-12 02:16:15', '2018-05-12 02:26:09', 0, 0, ''),
(7, 'Seiko SARB033 Discontinued Menyedihkan', 1, 'oke', 'Seiko SARB033 Discontinued.jpg', '<p>Astaghfirullah Seiko SARB033 sudah tidak diproduksi lagi, ini pasti ulah zionis, remason dan amerika.<br /><br /></p>\r\n<p>Like, Share dan Amin!</p>', 4, 1, '#NNN', '2018-05-12 02:18:50', '2018-05-12 02:26:43', 0, 0, ''),
(8, 'Seiko PADI Warna Biru', 1, 'oke', 'Seiko PADI Warna Biru.jpg', '<p>Alhamdulillah Seiko akhirnya mau mengeluarkan versi mawas dirinya yaitu seri PADI</p>\r\n<p>Like, share dan Amin!</p>', 4, 1, '#NNN', '2018-05-12 02:19:54', '2018-05-12 02:19:54', 0, 0, ''),
(9, 'Hi-Res Audio hanyalah ilusi semata', 1, 'oke', 'Hi-Res Audio hanyalah ilusi semata.jpg', '<p>Sepertri anda ketaui semua, stiker itu adalah sekedar ilusi untuk mengelabuhi kuping kalian semua.</p>\r\n<p>Like, share dan Amin!</p>', 3, 1, '#NNN', '2018-05-12 02:20:46', '2018-05-12 02:20:46', 0, 0, ''),
(10, 'Terungkap isi dari IEM Balanced Armature', 1, 'oke', 'Terungkap isi dari IEM Balanced Armature.jpg', '<p>Jadi seperti inilah ternyata isi IEM Driver Armature, ternyata hanya begitu astaghfirullah</p>\r\n<p>Like, share dan Amin!</p>', 3, 1, '#NNN', '2018-05-12 02:22:48', '2018-05-12 02:22:48', 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `brands`
--

INSERT INTO `brands` (`id`, `nama`, `deskripsi`, `urlname`) VALUES
(1, 'Sony', 'Test', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cabang`
--

CREATE TABLE `cabang` (
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `sub_kategori_id` int(10) UNSIGNED NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `fitur`
--

CREATE TABLE `fitur` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `konten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPositive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `fitur`
--

INSERT INTO `fitur` (`id`, `review_id`, `user_id`, `konten`, `isPositive`) VALUES
(1, 5, 1, 'ADAS', 0),
(2, 5, 1, 'DDD', 1),
(3, 6, 1, 'Tidak Erat', 0),
(4, 6, 1, 'Longgar', 0),
(5, 6, 1, 'Tahan Lama', 1),
(6, 6, 1, 'Pink', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `following`
--

CREATE TABLE `following` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_user_follow` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `following`
--

INSERT INTO `following` (`id`, `id_user`, `id_user_follow`, `created_at`, `updated_at`) VALUES
(3, 1, 2, '2018-05-26 00:38:03', '2018-05-26 00:38:03'),
(4, 21, 1, '2018-05-26 01:11:06', '2018-05-26 01:11:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `indikator`
--

CREATE TABLE `indikator` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `indikator`
--

INSERT INTO `indikator` (`id`, `nama`, `deskripsi`) VALUES
(1, 'Kualitas', 'Kualitas'),
(2, 'Harga', 'Harga'),
(3, 'Keaslian', 'Keaslian'),
(4, 'Keinjekan', 'Sempak'),
(5, 'Keeuyan', 'Euy!');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `urlname`, `status`, `deskripsi`, `img`) VALUES
(3, 'Audiophile', 'audiophile', 'TOP NEWS', 'audio', 'Audiophile.jpg'),
(4, 'Jam Tangan', 'jamtangan', 'TOP NEWS', 'jam tangan', 'Jam Tangan.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE `komentar` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `review_id` int(10) UNSIGNED NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `message`
--

CREATE TABLE `message` (
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `konten` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_16_092922_create_kategori_table', 1),
(4, '2017_12_16_093130_create_berita_table', 1),
(5, '2017_12_16_094240_create_cabang_table', 1),
(6, '2018_01_16_230203_create_indikator_table', 1),
(7, '2018_01_17_122947_create_subscribe_table', 1),
(8, '2018_01_17_123003_create_review_table', 1),
(9, '2018_01_17_123021_create_message_table', 1),
(10, '2018_01_17_225228_create_rating_table', 1),
(11, '2018_01_17_230809_create_fitur_table', 1),
(12, '2018_01_25_122900_create_komentar_table', 1),
(13, '2018_01_26_144548_create_sub_komentar_table', 1),
(14, '2018_04_21_010722_entrust_setup_tables', 1),
(15, '2018_05_01_234200_create_following', 1),
(16, '2018_05_01_234221_create_provinsi_tbl', 1),
(17, '2018_05_05_102433_create_review_image_table', 1),
(18, '2018_05_12_104514_create_sub_kategori_table', 2),
(19, '2018_05_05_123044_create_review_subkategori_table', 3),
(20, '2018_05_05_130627_create_social_media_table', 3),
(21, '2018_05_19_021230_create_rank_table', 4),
(22, '2018_05_02_104514_create_sub_kategori_table', 5),
(23, '2018_05_19_161440_create_request_table', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('khairalvin@gmail.com', '$2y$10$eN5odrGc22jkZyLskM5tmuQ2W6ghbpmTn08fmP39/pOROhv/DIOce', '2018-05-30 15:38:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi_tbl`
--

CREATE TABLE `provinsi_tbl` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `singkatan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `provinsi_tbl`
--

INSERT INTO `provinsi_tbl` (`id`, `kode`, `nama`, `singkatan`) VALUES
(1, '13', 'SUMATERA BARAT', 'SUMBAR'),
(2, '12', 'SUMATERA UTARA', 'SUMUT'),
(3, '11', 'ACEH', 'ACEH'),
(4, '14', 'RIAU', 'RIAU'),
(5, '15', 'JAMBI', 'JAMBI'),
(6, '16', 'SUMATERA SELATAN', 'SUMSEL'),
(7, '17', 'BENGKULU', 'BENGKULU'),
(8, '18', 'LAMPUNG', 'LAMPUNG'),
(9, '19', 'KEP. BANGKA BELITUNG', 'BABEL'),
(10, '20', 'KEPULAUAN RIAU', 'RIAU'),
(11, '31', 'DKI JAKARTA', 'DKI'),
(12, '32', 'JAWA BARAT', 'JABAR'),
(13, '33', 'JAWA TENGAH', 'JATENG'),
(14, '34', 'DI YOGYAKARTA', 'YOGYAKARTA'),
(15, '35', 'JAWA TIMUR', 'JATIM'),
(16, '36', 'BANTEN', 'BANTEN'),
(17, '51', 'BALI', 'BALI'),
(18, '52', 'NUSA TENGGARA BARAT', 'NTB'),
(19, '53', 'NUSA TENGGARA TIMUR', 'NTT'),
(20, '61', 'KALIMANTAN BARAT', 'KALBAR'),
(21, '62', 'KALIMANTAN TENGAH', 'KALTENG'),
(22, '63', 'KALIMANTAN SELATAN', 'KALSEL'),
(23, '64', 'KALIMANTAN TIMUR', 'KALTIM'),
(24, '71', 'SULAWESI UTARA', 'SULUT'),
(25, '72', 'SULAWESI TENGAH', 'SULTENG'),
(26, '73', 'SULAWESI SELATAN', 'SULSEL'),
(27, '74', 'SULAWESI TENGGARA', 'SULTRA'),
(28, '75', 'GORONTALO', 'GORONTALO'),
(29, '76', 'SULAWESI BARAT', 'SULBAR'),
(30, '81', 'MALUKU', 'MALUKU'),
(31, '82', 'MALUKU UTARA', 'MALUT'),
(32, '91', 'PAPUA BARAT', 'PABAR'),
(33, '94', 'PAPUA', 'PAPUA'),
(34, '65', 'KALIMANTAN UTARA', 'KALUT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `indikator_id` int(10) UNSIGNED NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `request`
--

CREATE TABLE `request` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `isi` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE `review` (
  `id` int(10) UNSIGNED NOT NULL,
  `urlname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `konten` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  `status` enum('enable','disable') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `review`
--

INSERT INTO `review` (`id`, `urlname`, `judul`, `user_id`, `kategori_id`, `konten`, `tagar`, `harga`, `brand_id`, `status`, `created_at`, `updated_at`) VALUES
(5, 'g-shock', 'G-SHOCK', 1, 4, '<p>TEST G-SHCOK</p>', 'G,SHOCK,AEA', 100, 1, 'enable', '2018-06-22 19:59:35', '2018-06-22 19:59:35'),
(6, 'v-shock', 'V-Shock', 1, 4, '<p>Shock di V</p>', 'V,Shock,WAW', 100, 1, 'enable', '2018-06-26 20:42:03', '2018-06-26 20:42:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `review-user`
--

CREATE TABLE `review-user` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `konten` longtext NOT NULL,
  `status` enum('enable','disable') DEFAULT NULL,
  `isRecommended` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review-user`
--

INSERT INTO `review-user` (`id`, `review_id`, `user_id`, `konten`, `status`, `isRecommended`, `created_at`, `updated_at`) VALUES
(6, 6, 1, '<p><strong>HIASAN</strong></p>', 'enable', 0, '2018-06-30 17:39:51', '2018-06-30 17:39:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `review-user_fitur`
--

CREATE TABLE `review-user_fitur` (
  `id` int(10) UNSIGNED NOT NULL,
  `review-user_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `konten` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPositive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review-user_fitur`
--

INSERT INTO `review-user_fitur` (`id`, `review-user_id`, `user_id`, `konten`, `isPositive`) VALUES
(1, 6, 1, 'K1', 1),
(2, 6, 1, 'K2', 1),
(3, 6, 1, 'K3', 1),
(4, 6, 1, 'K4', 1),
(5, 6, 1, 'M1', 0),
(6, 6, 1, 'M2', 0),
(7, 6, 1, 'M3', 0),
(8, 6, 1, 'M4', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `review-user_images`
--

CREATE TABLE `review-user_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `review-user_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `isThumbnail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review-user_images`
--

INSERT INTO `review-user_images` (`id`, `review-user_id`, `user_id`, `name`, `isThumbnail`) VALUES
(1, 6, 1, '550591_4442358262878_1685331812_n.jpg', 1),
(2, 6, 1, '12088286_10205158407535996_7648718892849599816_n.jpg', 0),
(3, 6, 1, '23659495_1553571824734080_8048278214875039116_n.jpg', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `review-user_indikator`
--

CREATE TABLE `review-user_indikator` (
  `review-user_id` int(10) UNSIGNED NOT NULL,
  `indikator_id` int(10) UNSIGNED NOT NULL,
  `nilai` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review-user_indikator`
--

INSERT INTO `review-user_indikator` (`review-user_id`, `indikator_id`, `nilai`) VALUES
(6, 1, 1),
(6, 2, 2),
(6, 3, 3),
(6, 4, 4),
(6, 5, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_image`
--

CREATE TABLE `review_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isThumbnail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `review_image`
--

INSERT INTO `review_image` (`id`, `review_id`, `user_id`, `name`, `isThumbnail`) VALUES
(1, 5, 1, 'seiko black 1.jpg', 1),
(2, 6, 1, 'images (1).jpg', 1),
(3, 6, 1, 'JEITA.png', 0),
(4, 6, 1, 'Logo_Kota_Tasikmalaya.png', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_indikator`
--

CREATE TABLE `review_indikator` (
  `review_id` int(10) UNSIGNED NOT NULL,
  `indikator_id` int(10) UNSIGNED NOT NULL,
  `nilai` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `review_indikator`
--

INSERT INTO `review_indikator` (`review_id`, `indikator_id`, `nilai`) VALUES
(5, 1, 10),
(5, 2, 10),
(5, 3, 10),
(6, 1, 4),
(6, 2, 6),
(6, 3, 2),
(6, 4, 8),
(6, 5, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `review_subkategori`
--

CREATE TABLE `review_subkategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_id` int(10) UNSIGNED NOT NULL,
  `subkategori_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `review_subkategori`
--

INSERT INTO `review_subkategori` (`id`, `review_id`, `subkategori_id`) VALUES
(1, 5, 1),
(2, 6, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'regular', 'User Regular', 'Hanya Sebagai User Biasa', '2018-05-12 13:38:01', '2018-05-12 13:38:07'),
(2, 'admin', 'Admin', 'Administrator', '2018-05-12 13:39:50', '2018-05-12 13:39:53'),
(3, 'moderator', 'Moderator', 'Moderator', '2018-05-12 13:40:10', '2018-05-12 13:40:13'),
(5, 'superadmin', 'Super Administrator', 'Dewa cuy', '2018-05-18 18:31:48', '2018-05-18 18:31:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 2),
(2, 3),
(24, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rusak`
--

CREATE TABLE `rusak` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `konten` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(10) DEFAULT NULL,
  `status` enum('enable','disable') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `social_media`
--

CREATE TABLE `social_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_id` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subkategori`
--

CREATE TABLE `subkategori` (
  `id` int(10) UNSIGNED NOT NULL,
  `subkategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kat` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `subkategori`
--

INSERT INTO `subkategori` (`id`, `subkategori`, `urlname`, `status`, `deskripsi`, `img`, `id_kat`, `created_at`, `updated_at`) VALUES
(1, 'Jam A', '', 'TOP AE', 'Jam A', '.jpg', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Jam B', '', 'TOP AE', 'jam b', '.png', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `subkomentar`
--

CREATE TABLE `subkomentar` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `komentar_id` int(10) UNSIGNED NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscribe`
--

CREATE TABLE `subscribe` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `region` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biodata` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verifyToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `gender`, `tanggal_lahir`, `region`, `hp`, `biodata`, `web`, `email`, `password`, `provider`, `provider_id`, `verifyToken`, `status`, `img`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'spiderman', 'Peter', 'Parker', 'L', '2018-04-22', '16', '082216418599', 'Karena Menjadi Spiderman adalah Jalan Ninjaku Euy!', 'www.injek.com', 'spiderman@gmail.com', '$2y$10$J29WpQ6uARDqLO/kmHih4ek/kPrkDSL8YIs08xbpyg1c5HFPdz7OW', '', '', '', 1, 'spiderman.jpg', 'zgASrq8GJjRZ33Bt0FGhV0AvHv7iR8ShOL2AslXCKTlf1qggktPHiPZy97k0', '2018-02-11 15:37:38', '2018-05-25 23:02:40'),
(2, 'wolverine', 'Logan', 'Paul', 'L', '2018-04-29', '17', '08231231', 'Makan Ayam Geprek adalah Jalannya dia', 'www.exmen.kom', 'wolverine@xmen.com', '$2y$10$d67oXWQ8DzMbZozRBIoOXubvTSeamfB.zdg72pk9e0syDboB.QjhK', '', '', '', 0, '', 'c06qP1KIsXmE6rgG53tKkBpsNGYVtRrmnLFKMWApH77pKEBOLiSGehhkyYrx', '2018-04-28 10:31:40', '2018-04-28 10:45:38'),
(24, 'alvinkhair', '', NULL, 'L', NULL, NULL, NULL, NULL, NULL, 'khairalvin@gmail.com', '$2y$10$JwRwFTFeCZOX9xK8DA1suO7Vf3frf7BPWMAw/U2WHU89fe4KCdjMe', NULL, NULL, NULL, 1, '', NULL, '2018-05-28 21:16:36', '2018-05-28 21:17:21');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`),
  ADD KEY `berita_kategori_id_foreign` (`kategori_id`),
  ADD KEY `berita_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cabang`
--
ALTER TABLE `cabang`
  ADD KEY `cabang_kategori_id_foreign` (`kategori_id`),
  ADD KEY `cabang_sub_kategori_id_foreign` (`sub_kategori_id`);

--
-- Indeks untuk tabel `fitur`
--
ALTER TABLE `fitur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fitur_review_id_foreign` (`review_id`),
  ADD KEY `fitur_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `following`
--
ALTER TABLE `following`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `indikator`
--
ALTER TABLE `indikator`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komentar_user_id_foreign` (`user_id`),
  ADD KEY `komentar_review_id_foreign` (`review_id`);

--
-- Indeks untuk tabel `message`
--
ALTER TABLE `message`
  ADD KEY `message_sender_id_foreign` (`sender_id`),
  ADD KEY `message_receiver_id_foreign` (`receiver_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `provinsi_tbl`
--
ALTER TABLE `provinsi_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rating_user_id_foreign` (`user_id`),
  ADD KEY `rating_indikator_id_foreign` (`indikator_id`);

--
-- Indeks untuk tabel `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_brand_id_foreign` (`brand_id`),
  ADD KEY `review_kategori_id_foreign` (`kategori_id`),
  ADD KEY `review_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `review-user`
--
ALTER TABLE `review-user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review-user_review_id` (`review_id`) USING BTREE,
  ADD KEY `review-user_user_id` (`user_id`) USING BTREE;

--
-- Indeks untuk tabel `review-user_fitur`
--
ALTER TABLE `review-user_fitur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fitur_review-user_id_foreign` (`review-user_id`) USING BTREE,
  ADD KEY `fitur-user_user_id_foreign` (`user_id`) USING BTREE;

--
-- Indeks untuk tabel `review-user_images`
--
ALTER TABLE `review-user_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review-user_image_review_id_foreign` (`review-user_id`) USING BTREE,
  ADD KEY `review-user_image_user_id_foreign` (`user_id`) USING BTREE;

--
-- Indeks untuk tabel `review-user_indikator`
--
ALTER TABLE `review-user_indikator`
  ADD PRIMARY KEY (`review-user_id`,`indikator_id`),
  ADD KEY `review-user_indikator_indikator_id_foreign` (`indikator_id`) USING BTREE;

--
-- Indeks untuk tabel `review_image`
--
ALTER TABLE `review_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_image_review_id_foreign` (`review_id`),
  ADD KEY `review_image_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `review_indikator`
--
ALTER TABLE `review_indikator`
  ADD PRIMARY KEY (`review_id`,`indikator_id`),
  ADD KEY `review_indikator_indikator_id_foreign` (`indikator_id`);

--
-- Indeks untuk tabel `review_subkategori`
--
ALTER TABLE `review_subkategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_subkategori_review_id_foreign` (`review_id`),
  ADD KEY `review_subkategori_subkategori_id_foreign` (`subkategori_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `rusak`
--
ALTER TABLE `rusak`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_media_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `subkomentar`
--
ALTER TABLE `subkomentar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subkomentar_user_id_foreign` (`user_id`),
  ADD KEY `subkomentar_komentar_id_foreign` (`komentar_id`);

--
-- Indeks untuk tabel `subscribe`
--
ALTER TABLE `subscribe`
  ADD KEY `subscribe_user_id_foreign` (`user_id`),
  ADD KEY `subscribe_kategori_id_foreign` (`kategori_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `user_username_unique` (`username`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `fitur`
--
ALTER TABLE `fitur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `following`
--
ALTER TABLE `following`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `indikator`
--
ALTER TABLE `indikator`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `provinsi_tbl`
--
ALTER TABLE `provinsi_tbl`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `request`
--
ALTER TABLE `request`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `review`
--
ALTER TABLE `review`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `review-user`
--
ALTER TABLE `review-user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `review-user_fitur`
--
ALTER TABLE `review-user_fitur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `review-user_images`
--
ALTER TABLE `review-user_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `review_image`
--
ALTER TABLE `review_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `review_subkategori`
--
ALTER TABLE `review_subkategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `rusak`
--
ALTER TABLE `rusak`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `subkomentar`
--
ALTER TABLE `subkomentar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`),
  ADD CONSTRAINT `berita_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `cabang`
--
ALTER TABLE `cabang`
  ADD CONSTRAINT `cabang_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`),
  ADD CONSTRAINT `cabang_sub_kategori_id_foreign` FOREIGN KEY (`sub_kategori_id`) REFERENCES `kategori` (`id`);

--
-- Ketidakleluasaan untuk tabel `fitur`
--
ALTER TABLE `fitur`
  ADD CONSTRAINT `fitur_ibfk_1` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fitur_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `komentar_review_id_foreign` FOREIGN KEY (`review_id`) REFERENCES `rusak` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `komentar_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_receiver_id_foreign` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `message_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_indikator_id_foreign` FOREIGN KEY (`indikator_id`) REFERENCES `indikator` (`id`),
  ADD CONSTRAINT `rating_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review-user`
--
ALTER TABLE `review-user`
  ADD CONSTRAINT `foreign_review` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `foreign_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review-user_fitur`
--
ALTER TABLE `review-user_fitur`
  ADD CONSTRAINT `fitur-user_ibfk_1` FOREIGN KEY (`review-user_id`) REFERENCES `review-user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fitur-user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review-user_images`
--
ALTER TABLE `review-user_images`
  ADD CONSTRAINT `review-user_image_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_user_review_foreing` FOREIGN KEY (`review-user_id`) REFERENCES `review-user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review-user_indikator`
--
ALTER TABLE `review-user_indikator`
  ADD CONSTRAINT `review-user_indikator_ibfk_1` FOREIGN KEY (`indikator_id`) REFERENCES `indikator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review-user_indikator_ibfk_2` FOREIGN KEY (`review-user_id`) REFERENCES `review-user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review_image`
--
ALTER TABLE `review_image`
  ADD CONSTRAINT `review_image_ibfk_1` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_image_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review_indikator`
--
ALTER TABLE `review_indikator`
  ADD CONSTRAINT `review_indikator_ibfk_1` FOREIGN KEY (`indikator_id`) REFERENCES `indikator` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_indikator_ibfk_2` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `review_subkategori`
--
ALTER TABLE `review_subkategori`
  ADD CONSTRAINT `review_subkategori_ibfk_1` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_subkategori_ibfk_2` FOREIGN KEY (`subkategori_id`) REFERENCES `subkategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `social_media`
--
ALTER TABLE `social_media`
  ADD CONSTRAINT `social_media_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `subkomentar`
--
ALTER TABLE `subkomentar`
  ADD CONSTRAINT `subkomentar_komentar_id_foreign` FOREIGN KEY (`komentar_id`) REFERENCES `komentar` (`id`),
  ADD CONSTRAINT `subkomentar_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `subscribe`
--
ALTER TABLE `subscribe`
  ADD CONSTRAINT `subscribe_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`),
  ADD CONSTRAINT `subscribe_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
