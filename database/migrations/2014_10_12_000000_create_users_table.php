<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',50);
            $table->string('first_name',50)->default("");
            $table->string('last_name',50)->default("");
            $table->string('gender',50);
            $table->date('tanggal_lahir')->nullable($value = true);
            $table->string('region',20)->default("");
            $table->string('hp',14)->default("");
            $table->string('biodata')->default("");
            $table->string('web')->default("");
            $table->string('email')->unique();
            $table->string('password');
            $table->string('img')->default("");
            $table->string('provider');
            $table->string('provider_id');
            $table->string('verifyToken');
            $table->boolean('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
