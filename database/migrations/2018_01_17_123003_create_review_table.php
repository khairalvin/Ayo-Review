<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        DB::beginTransaction();

        // Review Schema
        Schema::create('review', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('kategori_id')->unsigned();
            $table->foreign('kategori_id')->references('id')->on('kategori'); 
            $table->longText('konten'); 
            $table->text('tagar');
            $table->enum('status', ['enable', 'disable']);
            $table->timestamps();
        });

        // Review - Indikator Schema (Many to Many)
        Schema::create('review_indikator', function (Blueprint $table) {
            $table->integer('review_id')->unsigned();
            $table->integer('indikator_id')->unsigned();
            $table->integer('nilai')->unsigned();

            $table->foreign('review_id')->references('id')->on('review')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('indikator_id')->references('id')->on('indikator')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['review_id', 'indikator_id']);
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('review');
        Schema::drop('review_indikator');
    }
}
