<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');;
            $table->integer('review_id')->unsigned();
            $table->foreign('review_id')->references('id')->on('review')->onDelete('CASCADE')->onUpdate('CASCADE');;
            $table->text('pesan');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('review_id');
        });
        
        Schema::dropIfExists('users');
        Schema::dropIfExists('review');
        Schema::drop('komentar');
    }
}
