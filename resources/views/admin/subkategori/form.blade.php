@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
        @if ($jenis=="create")
        <form enctype="multipart/form-data" action="/subkategori" method="post">
            {{ csrf_field() }}
        @elseif ($jenis=="edit")
        <form enctype="multipart/form-data" action="/subkategori/{{$subkategori->id}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
        @endif
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Sub Kategori
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="@isset($subkategori->id){{$subkategori->id}}@endisset">
                        <div class="card-body">
                            <h4 class="card-title">{{$jenis}} Sub Kategori</h4>
                            <div class="form-group">
                                <label for="exampleSelect1">Kategori</label>
                                <select class="form-control" id="kategori" required name="kategori">
                                    <option selected disabled>Pilih</option>
                                    @foreach($categories as $kat)
                                        @isset($subkategori->id_kat)
                                            @if($subkategori->id_kat == $kat->id)
                                            <option value="{{$kat->id}}"selected>{{$kat->nama}}</option>
                                            @else
                                            <option value="{{$kat->id}}">{{$kat->nama}}</option>
                                            @endif
                                        @else
                                            <option value="{{$kat->id}}">{{$kat->nama}}</option>
                                        @endisset
                                            
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Sub Kategori</label>
                                <input type="text" required class="form-control" name="subkategori" id="subkategori" placeholder="Nama Sub Kategori" value="@isset($subkategori->subkategori){{$subkategori->subkategori}}@endisset">
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" required id="deskripsi" name="deskripsi" rows="3">@isset($subkategori->deskripsi){{$subkategori->deskripsi}}@endisset</textarea>
                            </div>

                            <div class="form-group">
                                @isset($subkategori->img)
                                <input id="imgupload" type="file" name="img"><br><br>
                                <hr> Current Image <hr>
                                <img id="blah" src="{{asset('admin/img/subkategori/'.$subkategori->img)}}" style="width: 200px;margin-top: 10px;" />
                                @else
                                <input id="imgupload" type="file" name="img" required><br><br>
                                <img id="blah" src="#" style="width: 200px;margin-top: 10px;"/>
                                @endisset
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection
@section('custom_js')
<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
}
$("#imgupload").change(function(){
    readURL(this);
});
</script>
@endsection