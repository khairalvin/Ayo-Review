@extends('admin.admin_layout.app')
@section('content')
@php
    use App\User;
    use App\Saran;
@endphp

<main class="py-md-3 px-md-2 sw-content" role="main">
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-commenting-o"></i> Index Saran
                    </div> {{--  card-header  --}}

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <th width="5%">No</th>
                                    <th width="20%">User</th>
                                    <th width="18%">Tanggal Saran</th>
                                    <th width="10%">Status</th>
                                    <th width="20%">Aksi</th>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($sarans as $val)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{User::find($val->id_user)->username}}</td>
                                        <td>{{$val->created_at}}</td>
                                        @if($val->status == "N")
                                    <td id="status{{$val->id}}">Belum Dibaca</td>
                                        @else
                                            <td>Sudah Dibaca</td>
                                        @endif
                                        <td><a class="btn btn-success" onclick="coba({{$val->id}})">Lihat Saran</a></td>
                                        {{ csrf_field() }}
                                    </tr>
                                    <?php $i++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> {{-- card body   --}}
                </div> {{-- card mb-3   --}}
            </div> {{--   col-sm-12 --}}
        </div> {{--  row  --}} 
    </section>
    <div id="konfirmasi" class="modal fade" id="modalSaran" tabindex="-1" role="dialog" aria-labelledby="modalSaran" aria-hidden="true">
    </div>
</main>
@endsection
@section('custom_js')
    <script>
        function coba(id){
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '/toRead',
                dataType: 'html',
                type    : 'POST',
                data		: {'_token' : _token,'id' : id},
                success: function(data){
                    $("#konfirmasi").html(data).modal('show');
                    document.getElementById("status"+id).innerHTML = "Sudah Dibaca";
                    
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    if (XMLHttpRequest.status === 200) {
                        bootbox.alert(textStatus+' errornya '+errorThrown);
                    }else{
                        unloading();
                        unloading(); bootbox.alert('Maaf, Terjadi kesalahan dalam sistem!!');
                    }
                }
            });
        }
    </script>
@endsection