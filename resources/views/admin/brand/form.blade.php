@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <form enctype="multipart/form-data" action="{{ $action }}" method="post" novalidate>
        {{ csrf_field() }}
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Brands
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="@isset($brand->id){{$brand->id}}@endisset">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" required class="form-control" id="nama" name="nama" placeholder="Nama Brand" value="@isset($brand->nama){{$brand->nama}}@endisset" required>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" required id="deskripsi" name="deskripsi" rows="3" required>@isset($brand->deskripsi){{$brand->deskripsi}}@endisset</textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection
@section('custom_js')
<script type="text/javascript">
	function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgupload").change(function(){
    readURL(this);
});
</script>
@endsection