@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    @if ($jenis=="create")
    <form enctype="multipart/form-data" action="/kategori" method="post">
        {{ csrf_field() }}
    @elseif ($jenis=="edit")
    <form enctype="multipart/form-data" action="/kategori/{{$kategori->id}}" method="post">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
    @endif
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Kategori
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="@isset($kategori->id){{$kategori->id}}@endisset">
                        <div class="card-body">
                        <h4 class="card-title">{{$jenis}} Kategori</h4>
                            <div class="form-group">
                                <label>Kategori</label>
                                <input type="text" required class="form-control" id="nama" name="nama" placeholder="Nama Kategori" value="@isset($kategori->nama){{$kategori->nama}}@endisset" required>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" required id="deskripsi" name="deskripsi" rows="3" required>@isset($kategori->deskripsi){{$kategori->deskripsi}}@endisset</textarea>
                            </div>

                            <div class="form-group">
                                @isset($kategori->img)
                                <input id="imgupload" type="file" name="img"><br><br>
                                <hr> Current Image <hr>
                                <img id="blah" src="{{asset('admin/img/kategori/'.$kategori->img)}}" style="width: 200px;margin-top: 10px;" />
                                @else
                                <input id="imgupload" type="file" name="img" required><br><br>
                                <img id="blah" src="#" style="width: 200px;margin-top: 10px;"/>
                                @endisset
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection
@section('custom_js')
<script type="text/javascript">
	function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgupload").change(function(){
    readURL(this);
});
</script>
@endsection