@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <form enctype="multipart/form-data" action="/kategori/{{$kategori->id}}" method="post">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Kategori
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="{{$kategori->id}}">
                        <div class="card-body">
                            <h4 class="card-title">Edit Kategori</h4>
                            <div class="form-group">
                                <label>Kategori</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Kategori" <input name="id" type="hidden" value="{{$kategori->nama}}">
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" <input name="id" type="hidden">{{$kategori->deskripsi}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection