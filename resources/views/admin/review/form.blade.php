@php
    use App\Kategori;
@endphp
@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <form enctype="multipart/form-data" action="{{ $action }}" method="post">
        {{ csrf_field() }}
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Review
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="{{  $review->id?:old('id') }}">
                        <div class="card-body">
                        <h4 class="card-title">Informasi Produk</h4>
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <input type="text" class="form-control" id="judul" name="judul" placeholder="Nama Produk" value="{{  $review->judul?:old('judul') }}" required>
                            </div>

                            <div class="form-group">
                                <label>Kategori</label>
                                <select class="form-control" id="kategori" name="kategori" required>
                                    @isset($review->kategori_id)
                                        <option value="{{$review->kategori_id}}">{{Kategori::getName($review->kategori_id)}}</option>
                                    @endisset
                                </select>
                            </div>                            

                            <div class="form-group">
                                <label>Sub Kategori</label>
                                <select class="form-control" id="subkategori" name="subkategori[]" multiple="multiple" required>
                                    @isset($review->subkategori)
                                        @foreach($review->subkategori as $subs)
                                            <option selected="selected">{{$subs->subkategori}}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Brand</label>
                                <select class="form-control" id="brand" name="brand" required>
                                    @isset($review->brand)
                                        <option selected="selected">{{$review->brand->nama}}</option>
                                    @endisset
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Range Harga 1</label>
                                <input type="number" class="form-control" id="harga1" name="harga1" placeholder="Kisaran Range Harga Awal" onkeyup="valHarga(this.value)" value="{{  $review->harga?:old('harga') }}" required>
                            </div>

                            <div class="form-group">
                                <label>Range Harga 2</label>
                                <input type="number" class="form-control" id="harga2" name="harga2" placeholder="Kisaran Range Harga Akhir" value="{{  $review->harga2?:old('harga2') }}" required>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3">{{  $review->konten?:old('deskripsi') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="tags" class="control-label">Tagar</label>
                                <div>
                                    <input type="text" class="form-control" id="tags" name="tags" data-role="tagsinput" value="{{  $review->tagar?:old('tags') }}">
                                </div>
                            </div>                            

                            <div class="form-group">
                                <label>Gambar Produk</label>
                                <div class="col-md-12">
                                    <div class="c-upload">
                                        <label class="pull-left">
                                            <input type="file" id="fileInput" name="gambar[]" multiple="multiple" required>
                                            + Lampirkan gambar
                                        </label>
                                    </div>
                                    <br>
                                    <div class="c-result" id="fileResult">
                                        <br>
                                        @if(isset($review->gambar))
                                            @if($review->gambar->count() > 0)
                                                @foreach($review->gambar as $gambar)
                                                    <img src="{{ asset('admin/img/review/'.$gambar->name) }}" />
                                                @endforeach
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-6 --}}

                <div class="col-sm-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            Indikator Penilaian
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div> -->
                        <div class="card-body">
                        <h4 class="card-title">Indikator Penilaian</h4>
                            <div class="form-group">
                                <input type="hidden" id="indikatorPenilaianHidden" name="indikatorPenilaian[]">
                                <table id="indikatorPenilaian"></table>
                            </div>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-6 --}}

                <div class="col-sm-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            Indikator Penilaian
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div> -->
                        <div class="card-body">
                        <h4 class="card-title">Kelebihan dan Kekurangan</h4>
                            <div class="form-group">
                                <input type="hidden" id="kelebihanKekuranganHidden" name="kelebihanKekurangan[]">
                                <table id="kelebihanKekurangan"></table>
                            </div>

                            <button type="submit" id="btnSubmit" onclick="_submit_()" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-6 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection

@section('custom_js')
{{-- TINY MCE --}}
<script src="{{asset('admin/vendor/tinymce/tinymce.min.js')}}"></script>
{{-- APPLYING FILEMANAGER --}}
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

<script type="text/javascript">
$('#brand').select2({
  ajax: {
    url: "{{ url('brand-helper') }}",
    type: "GET",
        quietMillis: 50,
        data: function (params) {
            var queryParameters = {
                term: params.term
            }
            return queryParameters;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id,
                    }
                })
            };
        }
  }
});
 $('#tags').tagsinput();

$('#kategori').select2({
  ajax: {
    url: "{{ url('kategori-helper') }}",
    type: "GET",
        quietMillis: 50,
        data: function (params) {
            var queryParameters = {
                term: params.term
            }
            return queryParameters;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id,
                    }
                })
            };
        }
  }
}).on("change", function(e) { 
   // what you would like to happen
   $("#subkategori").prop('disabled', false);
   $('#subkategori').select2({
        ajax: {
            url: "{{ url('subkategori-helper') }}/" + $(e.currentTarget).select2("val"),
            type: "GET",
            quietMillis: 50,
            data: function (params) {
                var queryParameters = {
                    term: params.term
                }
                
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.subkategori,
                            id: item.id,
                        }
                    })
                };
            }
        }
   });
});

$("#subkategori").select2({
  tags: true
});

@isset($review->indikator)
    $(function () {
        var st ="";
        $('#indikatorPenilaian').appendGrid({
            columns: [
                { name: 'Indikator', display: 'Indikator', type: 'select', 
                    onChange: function (evt, rowIndex) {
                        var indikator = $('#indikatorPenilaian').appendGrid('getAllValue');
                        $('#indikatorPenilaianHidden').val(JSON.stringify(indikator));
                    },
                    ctrlOptions: '@foreach($indikators as $indikator){{ $indikator->id }}:{{ $indikator->nama }};@endforeach' },
                { name: 'Nilai', display: 'Nilai', type: 'range', 
                    onChange: function (evt, rowIndex) {
                        var indikator = $('#indikatorPenilaian').appendGrid('getAllValue');
                        $('#indikatorPenilaianHidden').val(JSON.stringify(indikator));
                    },
                    ctrlCss: { width: '80px' }, ctrlAttr: { min: 0, max: 10, step: 1 } },
            ],
            initData: <?php echo $review->indikator;?>,
            maxRowsAllowed: 5,
            maxNumRowsReached: function () {
                // Show an alert message
                alert('You cannot add more than 5 rows!');
            }
        });
    });
@else
    $(function () {
        $('#indikatorPenilaian').appendGrid({ 
            initRows: 0,
            columns: [
                { name: 'Indikator', display: 'Indikator', type: 'select', 
                    onChange: function (evt, rowIndex) {
                        var indikator = $('#indikatorPenilaian').appendGrid('getAllValue');
                        $('#indikatorPenilaianHidden').val(JSON.stringify(indikator));
                    },
                    ctrlOptions: '@foreach($indikators as $indikator){{ $indikator->id }}:{{ $indikator->nama }};@endforeach' },
                { name: 'Nilai', display: 'Nilai', type: 'range', 
                    onChange: function (evt, rowIndex) {
                        var indikator = $('#indikatorPenilaian').appendGrid('getAllValue');
                        $('#indikatorPenilaianHidden').val(JSON.stringify(indikator));
                    },
                    ctrlCss: { width: '80px' }, ctrlAttr: { min: 0, max: 10, step: 1 } },
            ],
            maxRowsAllowed: 5,
            maxNumRowsReached: function () {
                // Show an alert message
                alert('You cannot add more than 5 rows!');
            }
        });
    });
@endisset

@isset($review->fitur)
    $(function () {
        $('#kelebihanKekurangan').appendGrid({ 
            initRows: 0,
            columns: [
                { name: 'Deskripsi', display: 'Deskripsi', type: 'text', 
                    onChange: function (evt, rowIndex) {
                        var kelebihan = $('#kelebihanKekurangan').appendGrid('getAllValue');
                        $('#kelebihanKekuranganHidden').val(JSON.stringify(kelebihan));
                    },                
                    ctrlAttr: { maxlength: 100 }, ctrlCss: { width: '160px'} },
                { name: 'Tipe', display: 'Tipe Penilaian', type: 'select', 
                    onChange: function (evt, rowIndex) {
                        var kelebihan = $('#kelebihanKekurangan').appendGrid('getAllValue');
                        $('#kelebihanKekuranganHidden').val(JSON.stringify(kelebihan));
                    }, 
                    ctrlOptions: { 0: 'Kekurangan', 1: 'Kelebihan'} },
            ],
            initData: <?php echo $review->fitur;?>
        });
    });
@else
    $(function () {
        $('#kelebihanKekurangan').appendGrid({ 
            initRows: 0,
            columns: [
                { name: 'Deskripsi', display: 'Deskripsi', type: 'text', 
                    onChange: function (evt, rowIndex) {
                        var kelebihan = $('#kelebihanKekurangan').appendGrid('getAllValue');
                        $('#kelebihanKekuranganHidden').val(JSON.stringify(kelebihan));
                    },                
                    ctrlAttr: { maxlength: 100 }, ctrlCss: { width: '160px'} },
                { name: 'Tipe', display: 'Tipe Penilaian', type: 'select', 
                    onChange: function (evt, rowIndex) {
                        var kelebihan = $('#kelebihanKekurangan').appendGrid('getAllValue');
                        $('#kelebihanKekuranganHidden').val(JSON.stringify(kelebihan));
                    }, 
                    ctrlOptions: { 0: 'Kekurangan', 1: 'Kelebihan'} },
            ]
        });
    });
@endif

function valHarga(id) {
    $("#harga2").val(id);
}

function _submit_(){
    var harga1 = document.getElementById("harga1").value;
    var harga2 = document.getElementById("harga2").value;
    if(harga2 < harga1){
        event.preventDefault();
        alert("Harga kedua tidak boleh lebih rendah dari Harga Pertama!")
    }else{
        $("#btnSubmit").submit();
    }
}

//Check File API support
if (window.File && window.FileList && window.FileReader) {
    var filesInput = document.getElementById("fileInput");

    filesInput.addEventListener("change", function (event) {

    var files = event.target.files; //FileList object
    var output = document.getElementById("fileResult");

    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        //Only pics
        if (!file.type.match('image'))
        continue;

        var picReader = new FileReader();

        picReader.addEventListener("load", function (event) {

        var picFile = event.target;

        var div = document.createElement("div");
        div.style.display = "inline-block";

        div.innerHTML = "<img class='thumbnail' height='100px' width='100x' src='" + picFile.result + "'" +
            "title='" + picFile.name + "'/>";

        output.insertBefore(div, null);

        });

        //Read the image
        picReader.readAsDataURL(file);
    }

    });
}
else {
    console.log("Your browser does not support File API");
}
</script>
@endsection