@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
        @if ($jenis=="create")
        <form enctype="multipart/form-data" action="/iklan" method="post">
            {{ csrf_field() }}
        @elseif ($jenis=="edit")
        <form enctype="multipart/form-data" action="/iklan/{{$iklan->id}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
        @endif
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Sub Kategori
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="@isset($iklan->id){{$iklan->id}}@endisset">
                        <div class="card-body">
                            <h4 class="card-title">{{$jenis}} Sub Kategori</h4>
                            <div class="form-group">
                                <label>Nama Iklan</label>
                                <input type="text" required class="form-control" name="nama" id="nama" placeholder="Nama Iklan" value="@isset($iklan->nama){{$iklan->nama}}@endisset">
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect1">Jenis Iklan</label>
                                <select class="form-control" id="kategori" name="kategori" required>
                                    <option selected disabled>Pilih</option>
                                    <option>Gold</option>
                                    <option>Siver</option>
                                    <option>Premium</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect1">Tempat Iklan</label>
                                <select class="form-control" id="tempat" name="tempat" required>
                                    <option selected disabled>Pilih</option>
                                    @foreach($tempat_iklan as $tempat)
                                        @isset($iklan->tempat)
                                            @if($subkategori->id_kat == $kat->id)
                                            <option value="{{$tempat->id}}"selected>{{$tempat->nama}}</option>
                                            @else
                                            <option value="{{$tempat->id}}">{{$tempat->nama}}</option>
                                            @endif
                                        @else
                                            <option value="{{$tempat->id}}">{{$tempat->nama}}</option>
                                        @endisset
                                            
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pengaju</label>
                                <input type="text" required class="form-control" name="pengaju" id="pengaju" placeholder="Pengaju" value="@isset($iklan->pengaju){{$iklan->pengaju}}@endisset">
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection