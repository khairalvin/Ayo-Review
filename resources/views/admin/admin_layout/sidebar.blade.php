<div id="sidebar" class="sw-sidebar">
    <form method="post" class="sw-search d-flex align-items-center" action="/search">
        <input type="search" class="form-control" id="search-input" name="keyword" placeholder="Search..." aria-label="Search for..." autocomplete="off">
        <button class="btn btn-light sw-search-docs-toggle d-md-none p-0 ml-3" type="submit" data-toggle="collapse" data-target="#sw-docs-nav" aria-controls="sw-docs-nav" aria-expanded="false" aria-label="Toggle docs navigation">
        </button>
    </form>

    <ul class="sw-links" id="sw-docs-nav" data-children=".sw-toc-item">
        <!-- DASHBOARD -->
        <li class="sw-toc-item mt-1 mb-1">
            <a class="sw-toc-link" href="#dashboard">
                <i class="fa fa-dashboard sidebar-fixed"></i>
                <span class="text">Dashboard</span>
            </a>
        </li>

        <!-- KATEGORI -->
        <li class="sw-toc-item mt-1 mb-1 mb-1" is-active-menu="" asp-controllers="Kategori">
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseKategoriSubMenus" role="button" aria-expanded="false" aria-controls="collapseArticleSubMenus">
                <i class="fa fa-tags sidebar-fixed"></i>
                <span class="text">Kategori</span>
            </div>
            <ul id="collapseKategoriSubMenus" class="collapse list-unstyled" role="tabpanel" data-parent="#sw-docs-nav">
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/kategori/create">
                        <i class="fa fa-plus"></i>
                        <span class="text">Tambah Kategori</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/kategori">
                        <i class="fa fa-edit"></i>
                        <span class="text">Index Kategori</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/subkategori">
                        <i class="fa fa-edit"></i>
                        <span class="text">Sub Kategori</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- REVIEW -->
        <li class="sw-toc-item mt-1 mb-1 mb-1" is-active-menu="" asp-controllers="Review">
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseArticleSubMenus" role="button" aria-expanded="false" aria-controls="collapseArticleSubMenus">
                <i class="fa fa-pencil sidebar-fixed"></i>
                <span class="text">Review</span>
            </div>
            <ul id="collapseArticleSubMenus" class="collapse list-unstyled" role="tabpanel" data-parent="#sw-docs-nav">
                <li>
                    <a class="sw-toc-sub-link mb-1" href="{{ url('review/create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="text">Tambah Review</span>
                    </a>
                </li>
                 <li>
                    <a class="sw-toc-sub-link mb-1" href="{{ url('review') }}">
                        <i class="fa fa-edit"></i>
                        <span class="text">Index Review</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/indikator">
                        <i class="fa fa-bullseye"></i>
                        <span class="text">Indikator</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/brands">
                        <i class="fa fa-edit"></i>
                        <span class="text">Brands</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- BERITA -->
        <li class="sw-toc-item mt-1 mb-1 mb-1" is-active-menu="" asp-controllers="Berita">
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseBeritaSubMenus" role="button" aria-expanded="false" aria-controls="collapseBeritaSubMenus">
                <i class="fa fa-newspaper-o sidebar-fixed"></i>
                <span class="text">Berita</span>
            </div>
            <ul id="collapseBeritaSubMenus" class="collapse list-unstyled" role="tabpanel" data-parent="#sw-docs-nav">
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/berita/create">
                        <i class="fa fa-plus"></i>
                        <span class="text">Tambah Berita</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/berita">
                        <i class="fa fa-edit"></i>
                        <span class="text">Index Berita</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/berita_top-news">
                        <i class="fa fa-fire"></i>
                        <span class="text">Atur TopNews</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- USER -->
        <li class="sw-toc-item mt-1 mb-1" is-active-route="" asp-controller="User" asp-action="Create">

            <div class="inner">
                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                    <a class="btn btn-light">
                        <span class="fa fa-plus"></span>
                    </a>
                    <a class="btn btn-light">
                        <span class="fa fa-edit"></span>
                    </a>
                    <a class="btn btn-light">
                        <span class="fa fa-remove"></span>
                    </a>
                </div>
            </div>
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapsePageSubMenus" role="button" aria-expanded="false" aria-controls="collapsePageSubMenus">
                <i class="fa fa-user-circle-o sidebar-fixed" ></i>
                <span class="text">User</span>
            </div>
            <ul id="collapsePageSubMenus" class="collapse list-unstyled" role="tabpanel" data-parent="#sw-docs-nav">
                <li>
                    <a class="sw-toc-sub-link mb-1" href="#banned">
                        <i class="fa fa-times-rectangle-o"></i>
                        <span class="text">Banned</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="#givestatus">
                        <i class="fa fa-plus"></i>
                        <span class="text">Give Status</span>
                    </a>
                </li>
                    <li class="hide-on-sb-inactive" is-active-menu="" asp-controllers="Pages" asp-action="AddData,EditData,Contents,Edit" asp-route-id="5">
                        <div class="sw-toc-sub-link mb-1">
                            <i class="fa fa-send-o"></i>
                            <span class="text">Broadcast Global Message</span>
                        </div>
                    </li>
            </ul>
        </li>

        <!-- ADVERTISEMENT -->
        <li class="sw-toc-item mt-1 mb-1" is-active-menu="" asp-controllers="Module">

            <div class="inner">
                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                    <a class="btn btn-light">
                        <span class="fa fa-plus"></span>
                    </a>
                    <a class="btn btn-light">
                        <span class="fa fa-edit"></span>
                    </a>
                    <a class="btn btn-light">
                        <span class="fa fa-remove"></span>
                    </a>

                </div>
            </div>
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseModuleSubMenus" aria-expanded="false" aria-controls="collapseModuleSubMenus">
                <i class="fa fa-money sidebar-fixed" ></i>
                <span class="text">Advertisement</span>
            </div>
            <ul id="collapseModuleSubMenus" class="collapse list-unstyled" data-parent="#sw-docs-nav">
                <li>
                    <a is-active-route="" class="sw-toc-sub-link mb-1" href="#AddAds">
                        <i class="fa fa-plus"></i>
                        <span class="text">Add Ads</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="#editads">
                        <i class="fa fa-edit"></i>
                        <span class="text">Edit Ads</span>
                    </a>
                </li>
                <li>
                    <a data-target="#" class="sw-toc-sub-link mb-1">
                        <i class="fa fa-remove"></i>
                        <span class="text">Remove Ads</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- Komentar -->
        <li class="sw-toc-item mt-1 mb-1">

            <div class="inner">
                <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                    <a class="btn btn-light">
                        <span class="fa fa-edit"></span>
                    </a>
                    <a class="btn btn-light">
                        <span class="fa fa-remove"></span>
                    </a>
                </div>
            </div>
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseAppearanceSubMenus" aria-expanded="false" aria-controls="collapseAppearanceSubMenus">
                <i class="fa fa-comments-o sidebar-fixed"></i>
                <span class="text">Comment</span>
            </div>
            <ul id="collapseAppearanceSubMenus" class="collapse list-unstyled" data-parent="#sw-docs-nav">
                <li>
                    <a class="sw-toc-sub-link mb-1" href="#removecoment">
                        <i class="fa fa-bars"></i>
                        <span class="text">List Comment</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="#removecoment">
                        <i class="fa fa-remove"></i>
                        <span class="text">Remove Comment</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- Saran -->
        <li class="sw-toc-item mt-1 mb-1">
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseSaranSubMenus" aria-expanded="false" aria-controls="collapseSaranSubMenus">
                <i class="fa fa-feed sidebar-fixed" ></i>
                <span class="text">Saran | Request</span>
            </div>
            <ul id="collapseSaranSubMenus" class="collapse list-unstyled" data-parent="#sw-docs-nav">
                <li>
                    <a href="/getSaran" class="sw-toc-sub-link mb-1">
                        <i class="fa fa-commenting-o"></i>
                        <span class="text">Saran</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="/getRequest">
                        <i class="fa fa-openid"></i>
                        <span class="text">Request</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- TEAM -->
        <li class="sw-toc-item mt-1 mb-1">
            <div class="sw-toc-link" data-toggle="collapse" data-target="#collapseTeamSubMenus" aria-expanded="false" aria-controls="collapseTeamSubMenus">
                <i class="fa fa-users sidebar-fixed" ></i>
                <span class="text">Team AR</span>
            </div>
            <ul id="collapseTeamSubMenus" class="collapse list-unstyled" data-parent="#sw-docs-nav">
                <li>
                    <a class="sw-toc-sub-link mb-1" href="{{route('role-index')}}">
                        <i class="fa fa-sitemap"></i>
                        <span class="text">Roles</span>
                    </a>
                </li>
                <li>
                    <a class="sw-toc-sub-link mb-1" href="{{route('role-mapping-index')}}">
                        <i class="fa fa-map-signs"></i>
                        <span class="text">Roles Mapping</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>