<header class="navbar navbar-expand flex-column flex-md-row sw-navbar">
    <a class="mr-0 mr-md-2" href="/markas">
        <img src="{{asset('../user/images/Logo-color.png') }}"  height="35px">
    </a>

    <button type="button" id="sidebarCollapse" class="btn btn-link" style="margin: 0 1.7rem 0 1.2rem">
        <span class="fa fa-exchange"></span>
    </button>

    <div class="navbar-nav-scroll">
        <ul class="navbar-nav sw-navbar-nav flex-row">
            <li class="nav-item"><a class="nav-link" href="#">AyoReview</a></li>
            <span class="nav-item-bawah fa fa-angle-right"></span>
            <li class="nav-item"><a class="nav-link" href="#">Berita</a></li>
            <span class="nav-item-bawah fa fa-angle-right"></span>
            <li class="nav-item active"><span class="nav-link">Index Berita</span></li>
        </ul>
    </div>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">

        <li class="nav-item dropdown mr-2">
            <a class="nav-link p-2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-inbox"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sw-versions">
                <a class="dropdown-item active" href="#kedelaikuda">Apa weh yang aktif</a>
                <a class="dropdown-item" href="#telekkuda">apa weh</a>
            </div>
        </li>
        <li class="nav-item dropdown mr-2">
            <a class="nav-item nav-link text-success" href="#" id="sw-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-envelope-o"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sw-versions">
                <a class="dropdown-item active" href="#">Apa weh yang aktif</a>
                <a class="dropdown-item" href="#">Merah</a>
                <a class="dropdown-item" href="#">Kuning</a>
                <a class="dropdown-item" href="#">Hijau</a>
            </div>
            
        </li>
        <div class="profile-align"> 
               <a href="/profile/{{Auth::user()->username}}"> {{Auth::user()->username}} </a>
        </div>

        <li class="nav-item dropdown mr-2">
            <a class="sw-avatar nav-link text-success" href="#" id="sw-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if(Auth::user()->img <> NULL || Auth::user()->img <> '')
                    @if(Auth::user()->provider <> NULL || Auth::user()->provider <> '')
                        <img class="rounded-circle" alt="200x200" src="{{Auth::user()->img}}" data-holder-rendered="true" style="width: 36px; height: 36px;">
                    @else
                        <img class="rounded-circle" alt="200x200" src="{{asset('user/images/user_profile/'.Auth::user()->img.'')}}" data-holder-rendered="true" style="width: 36px; height: 36px;">
                    @endif
                @else
                    @if(Auth::user()->gender == "L")
                        <img class="rounded-circle" alt="200x200" src="{{asset('user/images/ava_default/merah0ranger.png')}}" data-holder-rendered="true" style="width: 36px; height: 36px;">
                    @elseif(Auth::user()->gender == "P")
                        <img class="rounded-circle" alt="200x200" src="{{asset('user/images/ava_default/pink0ranger.png') }}" data-holder-rendered="true" style="width: 36px; height: 36px;">
                    @endif
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sw-versions">
            <a class="dropdown-item active" href="/profile/{{Auth::user()->username}}"><i class="fa fa-user-circle"></i> {{Auth::user()->username}}</a>
                <a class="dropdown-item" href="/">Menuju Website</a>
                {{-- <a class="dropdown-item" href="#">Change Password</a> --}}
                <a class="dropdown-item" href="/logout"><i class="fa fa-power-off" style="color:red;"></i> Logout</a>
            </div>
        </li>
    </ul>
</header>
