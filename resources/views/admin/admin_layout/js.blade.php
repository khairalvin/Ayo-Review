<script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="{{asset('admin/js/jquery-ui.js')}}"></script>

<!-- bootstrap & ayoreview -->
<script src="{{asset('admin/js/adminayoreview.js')}}"></script>
<!-- Table & chart-->
<script src="{{asset('admin/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('admin/js/sb-admin-datatables.min.js')}}"></script>
<script src="{{asset('admin/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<!-- JQUERY UPLOADER// -->
<script src="{{asset('admin/js/vendor/jquery.ui.widget.js')}}"></script>
<script src="{{asset('admin/js/jquery.iframe-transport.js')}}"></script>
<script src="{{asset('admin/js/jquery.fileupload.js')}}"></script> 

<!-- TAGS AND AUTOCOMPLETE -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<!-- Type aheaed -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.2.1/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.2.1/bloodhound.min.js" type="text/javascript"></script>
<!-- Append Grid -->
<script src="{{asset('admin/js/jquery.appendGrid-1.7.1.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('admin/js/select2.min.js')}}"></script>
<!-- Bootstrap Taginput -->                 
