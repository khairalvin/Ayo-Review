<div class="modal modal-default fade show" id="modal-files">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-11">
                    <h2 class="modal-title">Upload file</h2>
                </div>
                <div class="col-md-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="col-lg-4">

                            <input type="file" id="file">
                        </div>
                    </div>
                    <div class="col-lg-2" style="margin-bottom: 10px;">
                        <a href="#" class="btn btn-success btn-block" onclick="SW.Common.loadFileStream('Images')">Submit file...</a>
                    </div>
                </div>
                <table class="table table-condensed">
                    <thead class="thead-light">
                        <tr>
                            <th>
                                Folder
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                FullPath
                            </th>
                            <th>
                                Content
                            </th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn- btn-primary btn-warning" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>