<head>
    <link rel="stylesheet" href="{{asset('admin/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{asset('admin/css/adminayoreview.css') }}">
    <link rel="stylesheet" href="{{asset('admin/css/select2.min.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="../user/images/favicon.ico">
    <link href="{{asset('admin/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/css/jquery.appendGrid-1.7.1.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <title>Admin - Ayoreview</title>
    <!-- Tags form berita-->
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.0.0/flatly/bootstrap.min.css">-->
    <link rel="stylesheet" href="{{asset('admin/css/magicsuggest.css') }}">
</head>