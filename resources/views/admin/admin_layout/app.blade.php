<!DOCTYPE html>
<html>
    @include('admin.admin_layout.head')
    @yield('custom_css')
<body>
<!-- Page Header -->
    @include('admin.admin_layout.header')

    <!-- Page Content Holder -->
    <div class="container-fluid wrapper">
        <div class="row flex-xl-nowrap">

            <!-- Sidebar Holder -->
            @include('admin.admin_layout.sidebar')
            {{--  @include('admin.admin_layout.main')  --}}
            <!-- /.Sidebar Holder -->

            <!-- Main -->
            @yield('content')
            <!-- /Main -->

        </div>
    </div>
    <!-- /Page Content Holder -->

    <style>
        .ace_razor {
            background-color: yellow;
        }
        .ace_punctuation.ace_short.ace_razor {
            color: black;
        }

        .ace_punctuation.ace_block.ace_razor {
            color: black;
        }
    </style>

    <!-- Modal Error -->
        @include('admin.admin_layout.modal_error')
    <!-- Modal Error -->

    <!-- Modal Upload -->
        @include('admin.admin_layout.modal_error')
    <!-- /Modal Upload -->

    <!-- Bootstrap core JavaScript-->
</body>
        @include('admin.admin_layout.js')
        @yield('custom_js')
</html>