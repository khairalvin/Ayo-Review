@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-table"></i> Index Indikator
                        <div align="right">
                                <a class="btn btn-success" href="/indikator/create"><i class="fa fa-plus-circle"></i> Tambah Data</a>
                        </div>
                    </div> {{--  card-header  --}}

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <th width="10%">No</th>
                                    <th width="20%">Nama</th>
                                    <th width="20%">Deskripsi</th>
                                    <th colspan="2" width="20%"></th>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($indikators as $indikator)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$indikator->nama}}</td>
                                        <td>{{$indikator->deskripsi}}</td>
                                        <td>
                                            <a class="btn btn-info" href="indikator/{{$indikator->id}}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ubah</a>
                                            <form action="/indikator/{{$indikator->id}}" method="POST">
                                            {{ csrf_field()}}
                                            {{ method_field('DELETE') }}
                                                <button class="btn btn-danger" onclick="return confirm('Anda Yakin ingin Menghapus?');"><i class="fa fa-window-close" aria-hidden="true"></i> Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> {{-- card body   --}}

                    <div class="card-footer small text-muted">
                        Updated yesterday at 11:59 PM
                    </div> {{--  card footer  --}}
                </div> {{-- card mb-3   --}}
            </div> {{--   col-sm-12 --}}
        </div> {{--  row  --}} 
    </section>
</main>
@endsection