@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <form enctype="multipart/form-data" action="{{ $action }}" method="post">
        {{ csrf_field() }}
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Indikator
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="{{  $indikator->id?:old('id') }}">
                        <div class="card-body">
                        <h4 class="card-title">Form Indikator</h4>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Indikator" value="{{  $indikator->nama?:old('nama') }}" required>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" required>{{  $indikator->deskripsi?:old('deksripsi') }}</textarea>
                            </div>

                            <a href="{{ url('admin-indikator-index') }}" class="btn btn-default">Kembali</a>                            
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection