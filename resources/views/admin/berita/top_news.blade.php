@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-fire"></i> Atur Top News
                    </div> {{--  card-header  --}}

                    <div class="card-body">
                        <div class="table-responsive">
                            <form enctype="multipart/form-data" action="/berita_top-news" method="post">
                                <table class="table table-bordered" width="100%" cellspacing="0">
                                    <thead>
                                        <th width="30%">No</th>
                                        <th width="70%">Berita</th>
                                    </thead>
                                    <tbody>
                                            {{ csrf_field() }}
                                            @for ($i = 1; $i < 5; $i++)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>
                                                    <select class="js-example-basic-single js-states form-control" id="berita{{$i}}" name="berita{{$i}}">
                                                        <option value="{{$news[$i-1]->id}}" selected>{{$news[$i-1]->judul}}</option>
                                                    </select>
                                                    </td>
                                                </tr>
                                            @endfor
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        </div>
                    </div> {{-- card body   --}}

                    {{-- <div class="card-footer small text-muted">
                        Updated yesterday at 11:59 PM
                    </div> -- }} {{--  card footer  --}}
                </div> {{-- card mb-3   --}}
            </div> {{--   col-sm-12 --}}
        </div> {{--  row  --}} 
    </section>
</main>
@endsection
@section('custom_js')
<script>
    $(document).ready(function() {
        $('#berita1').select2({
            placeholder:"Pilih Judul Berita",
            ajax:{
                url: "/berita_search",
                dataType:'json',    
                delay:250,
                data:function(params){
                    return{
                        judul:params.term,
                    };
                },
                processResults:function(data){
                    var item = $.map(data, (value)=>{ //map buat ngemap object data kyk foreach
                        return { id: value.id, text: value.text };
                    });

                    return {
                        results: item
                    }
                },
                cache: true
            },
            minimumInputLength: 1,
        });
        $('#berita2').select2({
            placeholder:"Pilih Judul Berita",
            ajax:{
                url: "/berita_search",
                dataType:'json',    
                delay:250,
                data:function(params){
                    return{
                        judul:params.term,
                    };
                },
                processResults:function(data){
                    var item = $.map(data, (value)=>{ //map buat ngemap object data kyk foreach
                        return { id: value.id, text: value.text };
                    });

                    return {
                        results: item
                    }
                },
                cache: true
            },
            minimumInputLength: 1,
        });
        $('#berita3').select2({
            placeholder:"Pilih Judul Berita",
            ajax:{
                url: "/berita_search",
                dataType:'json',    
                delay:250,
                data:function(params){
                    return{
                        judul:params.term,
                    };
                },
                processResults:function(data){
                    var item = $.map(data, (value)=>{ //map buat ngemap object data kyk foreach
                        return { id: value.id, text: value.text };
                    });

                    return {
                        results: item
                    }
                },
                cache: true
            },
            minimumInputLength: 1,
        });
        $('#berita4').select2({
            placeholder:"Pilih Judul Berita",
            ajax:{
                url: "/berita_search",
                dataType:'json',    
                delay:250,
                data:function(params){
                    return{
                        judul:params.term,
                    };
                },
                processResults:function(data){
                    var item = $.map(data, (value)=>{ //map buat ngemap object data kyk foreach
                        return { id: value.id, text: value.text };
                    });

                    return {
                        results: item
                    }
                },
                cache: true
            },
            minimumInputLength: 1,
        });
    });
</script>
@endsection