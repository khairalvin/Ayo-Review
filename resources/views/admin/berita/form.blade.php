@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
        @if ($jenis=="create")
        <form enctype="multipart/form-data" action="/berita" method="post">
            {{ csrf_field() }}
        @elseif ($jenis=="edit")
        <form enctype="multipart/form-data" action="/berita/{{$news->id}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
        @endif
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                                <i class="fa fa-plus"></i> Data Berita
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <input name="id" type="hidden" value="@isset($news->id){{$news->id}}@endisset">
                        <div class="card-body">
                            <h4 class="card-title">{{$jenis}} Berita</h4>
                            <div class="form-group">
                                <label>Judul</label>
                                <input type="text" required class="form-control" name="judul" id="judul" placeholder="Judul Berita" value="@isset($news->judul){{$news->judul}}@endisset">
                            </div>
                            <div class="form-group">
                                <label>Tags Berita</label> 
                                <input type="text" id="ms1" name="ms1" class="form-control"></div>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect1">Kategori</label>
                                <select class="form-control" id="kategori" name="kategori" required>
                                    @foreach($categories as $kat)
                                        @isset($news->kategori_id)
                                            @if($news->kategori_id == $kat->id)
                                            <option value="{{$kat->id}}"selected>{{$kat->nama}}</option>
                                            @else
                                            <option value="{{$kat->id}}">{{$kat->nama}}</option>
                                            @endif
                                        @else
                                            <option value="{{$kat->id}}">{{$kat->nama}}</option>
                                        @endisset
                                            
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                @isset($news->img)
                                <input id="imgupload" type="file" name="img"><br><br>
                                <hr> Thumbnail <hr>
                                <img id="blah" src="{{asset('admin/img/berita_thumb/'.$news->img)}}" style="width: 200px;margin-top: 10px;" />
                                @else
                                <input id="imgupload" type="file" name="img" required><br><br>
                                <img id="blah" src="#" style="width: 200px;margin-top: 10px;"/>
                                @endisset
                            </div>
                            <textarea id="konten" class="form-control" name="konten" rows="10" cols="50">@isset($news->konten){{$news->konten}}@endisset</textarea>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection
@section('custom_js')
<script src="{{asset('admin/js/magicsuggest.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#kategori').select2({
            placeholder:"Pilih Kategori",
        });
        $('.js-example-basic-multiple').select2();
        $('#ms1').magicSuggest();
        $( "#ms1" ).keyup(function() {
            // var id = $("#ms1").value;
            // alert(id);
            $('#ms1').magicSuggest({
                data: [{"id":0, "name":"Paris"}, {"id":1, "name":"New York"}]
            });
        });
    });

</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
{{-- TINY MCE --}}
<script src="{{asset('admin/vendor/tinymce/tinymce.min.js')}}"></script>
{{-- APPLYING FILEMANAGER --}}
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

{{-- THUMBNAIL UPLOAD --}}
<script type="text/javascript">
	function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgupload").change(function(){
    readURL(this);
});
</script>
@endsection