@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i> Index Berita
                    </div> {{--  card-header  --}}

                    <div class="card-body">
                    <div class="add-top">
                                <a class="btn btn-tambah" href="/berita/create"><i class="fa fa-plus-circle"></i> Tambah Data</a>
                    </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <th width="5%">No</th>
                                    <th width="25%">Judul</th>
                                    <th width="10%">Status</th>
                                    <th width="17%">Tgl Buat</th>
                                    <th width="10%">Viewer</th>
                                    <th width="18%">Aksi</th>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($beritas as $news)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$news->judul}}</td>
                                        <td>{{$news->status}}</td>
                                        <td>{{$news->created_at}}</td>
                                        <td>{{$news->viewer}}</td>
                                        <td>
                                            <form action="/berita/{{$news->id}}" method="POST">
                                            {{ csrf_field()}}
                                            {{ method_field('DELETE') }}
                                                <a class="btn btn-primary" href="berita/{{$news->id}}/edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ubah</a>
                                                <button class="btn btn-danger" onclick="return confirm('Anda Yakin ingin Menghapus?');"><i class="fa fa-close" aria-hidden="true"></i> Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> {{-- card body   --}}

                    <div class="card-footer small text-muted">
                        Updated yesterday at 11:59 PM
                    </div> {{--  card footer  --}}
                </div> {{-- card mb-3   --}}
            </div> {{--   col-sm-12 --}}
        </div> {{--  row  --}} 
    </section>
</main>
@endsection