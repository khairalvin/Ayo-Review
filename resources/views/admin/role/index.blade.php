@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-sitemap"></i> Index Roles
                    </div> {{--  card-header  --}}

                    <div class="card-body">
                            <div class="add-top">
                                    <a class="btn btn-tambah" href="/berita/create"><i class="fa fa-plus-circle"></i> Tambah Data</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <th width="5%">No</th>
                                    <th width="20%">Role</th>
                                    <th width="30%">Deskripsi</th>
                                    <th width="15%">Aksi</th>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$role->display_name}}</td>
                                        <td>{{$role->description}}</td>
                                        <td>
                                            <form action="{{route('role-delete',$role->id)}}" method="POST">
                                            {{ csrf_field()}}
                                            {{ method_field('DELETE') }}
                                                <a class="btn btn-primary" href="{{route('role-edit',$role->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ubah</a>
                                                <button class="btn btn-danger" onclick="return confirm('Anda Yakin ingin Menghapus?');"><i class="fa fa-window-close" aria-hidden="true"></i> Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> {{-- card body   --}}
                </div> {{-- card mb-3   --}}
            </div> {{--   col-sm-12 --}}
        </div> {{--  row  --}} 
    </section>
</main>
@endsection