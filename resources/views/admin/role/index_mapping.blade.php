@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
    <section class="content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-map-signs"></i> Mapping Role
                    </div> {{--  card-header  --}}

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <th width="5%">No</th>
                                    <th width="30%">User</th>
                                    <th width="30%">Role</th>
                                    <th width="5%">Aksi</th>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$role->username}}</td>
                                        <td>{{$role->display_name}}</td>
                                        <td>
                                            <a class="btn btn-primary" href="{{route('role-mapping-edit',['user_id' => $role->user_id,'role_id'=>$role->role_id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Ubah</a>
                                        </td>
                                    </tr>
                                    <?php $i++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> {{-- card body   --}}
                </div> {{-- card mb-3   --}}
            </div> {{--   col-sm-12 --}}
        </div> {{--  row  --}} 
    </section>
</main>
@endsection