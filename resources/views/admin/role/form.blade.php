@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
        @if ($jenis=="Tambah")
        <form enctype="multipart/form-data" action="{{route('role-store')}}" method="post">
            {{ csrf_field() }}
        @elseif ($jenis=="Edit")
        <form enctype="multipart/form-data" action="{{route('role-update',$role->id)}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
        @endif
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Data Role
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">{{$jenis}} Role</h4>
                            <div class="form-group">
                                <label>Nama Role</label>
                                <input type="text" required class="form-control" name="name" id="name" placeholder="Nama Role" value="@isset($role->name){{$role->name}}@endisset">
                            </div>

                            <div class="form-group">
                                <label for="exampleSelect1">Display Nama Role</label>
                                <input type="text" required class="form-control" name="display_name" id="display_name" placeholder="Display Nama Role" value="@isset($role->display_name){{$role->display_name}}@endisset">
                            </div>

                            <div class="form-group">
                                <label for="exampleSelect1">Deskripsi</label>
                                <input type="text" required class="form-control" name="description" id="description" placeholder="Deskripsi" value="@isset($role->description){{$role->description}}@endisset">
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection