@extends('admin.admin_layout.app')
@section('content')
<main class="py-md-3 px-md-2 sw-content" role="main">
        <form enctype="multipart/form-data" action="{{route('role-mapping-update',['id' => $user->id,'role_id'=>$role_id])}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
        <section class="content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            Role Mapping
                            <button type="button" id="sidebarCollapse" class="btn btn-link move">
                                <span class="oi oi-move"></span>
                            </button>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">Edit Role Mapping</h4>

                            <div class="form-group">
                                <label>User</label>
                                <input type="text" required class="form-control" name="user" id="user" readonly value="@isset($user->username){{$user->username}}@endisset">
                            </div>

                            <div class="form-group">
                                <label for="exampleSelect1">Role</label>
                                <select class="form-control" id="role_id" name="role_id" required>
                                    @foreach($roles as $role)
                                        @isset($role_id)
                                            @if($role->id == $role_id)
                                            <option value="{{$role->id}}"selected>{{$role->display_name}}</option>
                                            @else
                                            <option value="{{$role->id}}">{{$role->display_name}}</option>
                                            @endif
                                        @else
                                            <option value="{{$role->id}}">{{$role->display_name}}</option>
                                        @endisset
                                            
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div> {{--   card --}}
                </div> {{--   col-sm-12 --}}
            </div> {{--  row  --}}
        </section>
    </form>
</main>
@endsection
@section('custom_js')
<script>
    $(document).ready(function() {
    $('#role_id').select2();
});
</script>
@endsection