<div id="content" class="col-lg-9 pl-0 pr-2 content">
    <!--- Review pengguna -->
    <div id="review">
        <div class="col-lg-12 pt-0 px-0">
            <div class="row main-rev">
                <!-- review pertama -->
                <div class="col-lg-12 pb-2">
                <ul>
                    <li>
                    <div class="card ">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-7 ">
                                <b># TOP 1
                                </b>
                                <span>24 dari 32 (98%) orang mengatakan review ini membantu
                                </span>
                                </div>
                                <div class="col-sm-5">
                                <span class="mr-2">Apakah review ini membantu ?
                                </span>
                                <button class="btn btn-rate mr-1" type="button">
                                    <i class="fa fa-thumbs-up" aria-hidden="true">
                                    </i>&nbsp;&nbsp;Yes
                                </button>
                                <button class="btn btn-rate" type="button">
                                    <i class="fa fa-thumbs-down" aria-hidden="true">
                                    </i>&nbsp;&nbsp;No
                                </button>
                                <button class="btn btn-rate float-right" type="button">
                                    <i class="fa fa-flag" aria-hidden="true">
                                    </i>
                                </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- Lingkaran Rating -->
                                <div class="col-lg-4 pr-0">
                                    <div class="circle-big cir-middle">
                                        <div class="progress blue">
                                            <span class="progress-left">
                                                <span class="progress-bar">
                                                </span>
                                            </span>
                                            <span class="progress-right">
                                                <span class="progress-bar">
                                                </span>
                                            </span>
                                            <div class="progress-value">{{$rata2nilai_user}}</div>
                                        </div>
                                    </div>
                                    <!-- End Lingkaran Rating -->
                                    <!-- Profile reviewer -->
                                    <div class="col-lg-12">
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                @if($user->img <> NULL || $user->img <> '')
                                                    @if($user->provider <> NULL || $user->provider <> '')
                                                        <img class="user" src="{{$user->img}}"alt="">
                                                    @else
                                                        <img class="user" src="{{asset('user/images/user_profile/'.$user->img.'')}}"alt="">
                                                    @endif
                                                @else
                                                    @if($user->gender == "L")
                                                        <img class="user" src="{{asset('user/images/ava_default/merah0ranger.png')}}"alt="">
                                                    @elseif($user->gender == "P")
                                                        <img class="user" src="{{asset('user/images/ava_default/pink0ranger.png') }}"alt="">
                                                    @endif
                                                @endif
                                            </a>
                                            <div class="media-body pl-2">
                                                <a href="" class="label-username">{{$user->username}}
                                                </a>
                                                <button class="follow btn btn-outline-pilihan ">
                                                <span class="msg-follow">Follow
                                                </span>
                                                <span class="msg-following">Following
                                                </span>
                                                <span class="msg-unfollow">Unfollow
                                                </span>
                                                </button>
                                                <br>
                                                <span class="label-trending">
                                                <i class="fa fa-headphones" aria-hidden="true">
                                                </i>&nbsp;&nbsp;{{$user->biodata}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Profile reviewer -->
                                    <div class="row py-4">
                                        @foreach($rating_user as $ratUser)
                                            <!-- Kriteria pertama -->
                                            <div class="col-lg-5 pr-0">
                                                <span class="label-kreteria">{{$ratUser['nama']}}
                                                </span>
                                            </div>
                                            <div class="col-lg-7 p-progress">
                                                <div class="progress">
                                                <div class="progress-bar" style="width:{{$ratUser['nilai']}}0%">{{$ratUser['nilai']}}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End Kriteria pertama -->
                                        @endforeach
                                    </div>
                                    <h3>KEUNGGULAN</h3>
                                    <ul class="pro ">
                                        @foreach($fitur_keunggulan as $pro)
                                        <li>
                                            {{$pro->konten}}
                                        </li>
                                        @endforeach
                                    </ul>
                                    <h3 class="mt-4">KEKURANGAN</h3>
                                    <ul class="con ">
                                        @foreach($fitur_kekurangan as $minus)
                                        <li>
                                            {{$minus->konten}}
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-8 pl-0">
                                    <div class="col-lg-12 pt-4 rev-content">
                                        <h1 class="px-5 mb-4" style="font-size:22px; font-weight:bold;">{{$review->judul}}</h1>
                                        <?=$reviewUser->konten?>
                                        @if($reviewUser->isRecommended == 1)
                                            <table class="mx-5 my-4 thumb-up">
                                                <tr>
                                                    <td class="pl-4 py-3" width="22%">
                                                    <i class="fa fa-thumbs-up">
                                                    </i>
                                                    </td>
                                                    <td class="align-middle">
                                                        <span class="rekomendasi">Saya Merekomendasikan Ini</span>
                                                    <br>
                                                    <span class="footage">Diposkan 3 hari yang lalu
                                                    </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        @else
                                            <table class="mx-5 my-4 thumb-down">
                                                <tr>
                                                    <td class="pl-4 py-3" width="22%">
                                                    <i class="fa fa-thumbs-down">
                                                    </i>
                                                    </td>
                                                    <td class="align-middle">
                                                        <span class="rekomendasi">Saya Merekomendasikan Ini</span>
                                                    <br>
                                                    <span class="footage">Diposkan 3 hari yang lalu
                                                    </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        @endif
                                        <!-- Jika ada video -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </li>
                </ul>
                </div>
                <!-- end review pertama -->
            </div>
        </div>
        <!--form komentar review pengguna -->
        <!-- Komentar disqus -->
        <div class="disqus sp-content-fullreview px-5 py-2 " id="disqus-comment">
            <div id="disqus_thread">
            </div>
            <script>
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                var disqus_config = function () {
                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                */
                (function () {
                // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://ayoreview.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                }
                )();
            </script>
            <noscript>Please enable JavaScript to view the
                <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.
                </a>
            </noscript>
        </div>
        <!-- end komentar disqus -->
    </div>
    <!-- end Review Pengguna -->
</div>