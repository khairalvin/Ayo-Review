<!-- sidebar -->
<div id="sidebar" class="col-lg-3 p-0 sidebar col-md-offset-2">
    <div class="col-lg-12 sp-content-fullreview p-0 mb-2">
        <a href="fullreview.html">
        <div class="big-pic-side" style="background-image:url('{{asset('admin/img/review/'.$image.'')}}');">
        </div>
        <p class="label-kreteria px-4 py-3 m-0 text-center">{{$review->judul}}
        </p>
        </a>
    </div>
    <div class="col-lg-12 sp-content-fullreview  p-4">
        <h2>RATING
            <span>RATA-RATA</span>
        </h2>
        <div class="row pt-3">
            @foreach($meanIndikator as $mean)
                <!-- Progress kriteria penilaian 1 -->
                <div class="col-lg-5 pr-0 pb-2">
                    <span class="label-kreteria">{{$mean['nama']}}
                    </span>
                </div>
                <div class="col-lg-7 pl-0">
                    <div class="progress">
                    <div class="progress-bar" style="width:{{$mean['nilai']}}0%">{{$mean['nilai']}}
                    </div>
                    </div>
                </div>
                <!-- End Progress kriteria penilaian 1 -->
            @endforeach
        <div class="col-lg-4 pr-0">
            <div class="circle-big left">
            <div class="progress blue">
                <span class="progress-left">
                <span class="progress-bar">
                </span>
                </span>
                <span class="progress-right">
                <span class="progress-bar">
                </span>
                </span>
                <div class="progress-value">{{$total_ratingIndikator['nilai']}}
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-8 pl-0">
            <h3>{{$total_ratingIndikator['keterangan']}}
            </h3>
            <span class="tot-rev">25 Review Pengguna
            </span>
        </div>
        </div>
    </div>
</div>
<!-- End  sidebar -->