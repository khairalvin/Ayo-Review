@php
use App\Review;
@endphp
<!-- judul atas dari review -->
<div class="container-fluid top-fullreview h-review">
    <div class="container">
      <div class="row">
        <!-- isi judul dari review -->
        <div class="col-sm-8 pt-3 pb-1 m-0 ">
          <h1 class="d-block">Review : {{$review->judul}} </h1>
          <a href="" class="btn btn-outline-pilihan">#{{strtoupper(Review::find($review->id)->brand()->first()->nama)}}</a>
          @foreach(Review::find($review->id)->subkategori()->get() as $subkat)
            <a href="" class="btn btn-outline-pilihan">#{{$subkat->subkategori}}</a>
          @endforeach
          @foreach(explode(',',$review->tagar) as $tagar)
              <a href="" class="btn btn-outline-pilihan">#{{strtoupper($tagar)}}</a>
          @endforeach
        </div>
        <!--end isi judul dari review -->

        <!-- tombol share button -->
        <div class="col-sm-4 py-3 m-0 pos-rel">
          <div class="sharebutton">
            <a href="" class="btn btn-outline-facebook-theme ">
              <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a href="/jamtangan.html" class="btn btn-outline-twitter-theme">
              <i class="fa fa-twitter" aria-hidden="true"></i>
            </a>
            <a href="" class="btn btn-outline-googlep-theme">
              <i class="fa fa-google" aria-hidden="true"></i>
            </a>
            <a href="" class="btn btn-outline-pinterest-theme">
              <i class="fa fa-pinterest" aria-hidden="true"></i>
            </a>
            <a href="" class="btn btn-outline-linkshare-theme">
              <i class="fa fa-link" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <!-- end tombol share button -->
      </div>
    </div>
</div>
<!-- end judul atas dari review -->