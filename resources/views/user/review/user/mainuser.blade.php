@extends('user.layout_main.master')
@section('custom_css')
@endsection
@section('content')
@include('user.review.user.judulReview')
    <main role="main" class="container">
        <div class="row py-3">
            @include('user.review.user.kontenUser')
            @include('user.review.user.sidebarUser')
        </div>
        @include('user.review.user.produkTerkait')
    </main>
@endsection
@section('custom_js')
@endsection
