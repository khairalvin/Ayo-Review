@php
  use App\Kategori;
@endphp
<!-- rekomendasi konten -->
<div class="row sp-header mr-review-header">
    <div class="col-lg-12 sp-inside-review">
        <h1>Produk
            <span>&nbsp;Terkait
            </span>
        </h1>
    </div>
</div>
<div class="row px-0 pt-0 pb-2 sp-content-recomendation">
    @foreach($relateds as $related)
        <div class="col-sm-12 col-md-6 col-lg-3 px-2">
            <div class="card lil">
                <div class="tag-lil-review">
                    {{Kategori::getName($related->kategori_id)}}
                </div>
                <img class="card-img-top explore-review-img" src="{{ asset('admin/img/review/'.$related->image.'') }}" alt="Card image cap">
                <div class="card-body-lil">
                    <h4 class="card-title">Review : {{$related->judul}} 
                    </h4>
                    <div class="circle kategori-progress">
                        <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar">
                            </span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar">
                            </span>
                        </span>
                        <div class="progress-value">{{$related->rating}}
                        </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                <i class="fa fa-comments" aria-hidden="true">
                </i>
                <a href="">
                    <b>&nbsp; 25 Review Pengguna
                    </b>
                </a>
                <a href="/reviewadmin/{{$related->urlname}}" class="pull-right">Selengkapnya
                </a>
                </div>
            </div>
            <!-- end card kamera  1-->
        </div>
    @endforeach
</div>
