@php
    use App\User;
@endphp
<div id="review" class="dsn">
    <div class="col-lg-12 pt-4 px-0">
        <!-- Row Atas tool bar-->
        <div class="row p-0 top-nav-rev">
        <div class="col-lg-8">
            <span>Menampilkan 20 review pengguna dari produk ini </span>
        </div>
        <div class="col-lg-4">
            <div class="row">
            <div class="col-lg-3">
                <span>Urutkan:</span>
            </div>
            <div class="col-lg-9">
                <div class="form-group">
                <select class="form-control" id="sel1">
                    <option disabled selected>Terbaik</option>
                    <option>Terbaik</option>
                    <option>Terbaru</option>
                    <option>Terlama</option>
                </select>
                </div>
            </div>
            </div>
        </div>
        </div>
        <!-- End Row Atas tool bar-->
        <div class="row  pt-2 main-rev">
        <!-- Review Pengguna -->
        <div class="col-lg-12 pb-1">
            <ul>
                @isset($latest)
                    @foreach($latest as $rUser)
                    <!-- review pertama -->
                    <li>
                        <div class="card ">
                        <div class="card-header">
                            <div class="row">
                            <div class="col-sm-7 ">
                                <b># TOP 1</b>
                                <span>24 dari 32 (98%) orang mengatakan review ini membantu</span>
                            </div>
                            <div class="col-sm-5">
                                <span class="mr-2">Apakah review ini membantu ?</span>
                                <button class="btn btn-rate mr-1" type="button">
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;&nbsp;Yes</button>
                                <button class="btn btn-rate" type="button">
                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>&nbsp;&nbsp;No</button>
                                <button class="btn btn-rate float-right" type="button">
                                <i class="fa fa-flag" aria-hidden="true"></i>
                                </button>
                            </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                            <div class="col-lg-4 pr-0">
                                <div class="circle-big cir-middle">
                                <div class="progress blue">
                                    <span class="progress-left">
                                    <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                    <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value">{{$rUser->total}}</div>
                                </div>
                                </div>
                                <div class="col-lg-12">
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        @if($rUser->user->img <> NULL || $rUser->user->img <> '')
                                            @if($rUser->user->provider <> NULL || $rUser->user->provider <> '')
                                                <img class="user" src="{{$rUser->user->img}}">
                                            @else
                                                <img class="user" src="{{asset('user/images/user_profile/'.$rUser->user->img.'')}}">
                                            @endif
                                        @else
                                            @if($rUser->user->gender == "L")
                                                <img src="{{asset('user/images/ava_default/merah0ranger.png')}}" width="42px" height="33px" alt="">
                                            @elseif($rUser->user->gender == "P")
                                                <img src="{{asset('user/images/ava_default/pink0ranger.png') }}" width="42px" height="33px" alt="">
                                            @endif
                                        @endif
                                    </a>
                                    <div class="media-body pl-2">
                                    <a href="" class="label-username">{{User::where('id',$rUser->review->user_id)->first()->username}}</a>
                                    {{-- <button class="follow btn btn-outline-pilihan ">
                                        <span class="msg-follow">Follow</span>
                                        <span class="msg-following">Following</span>
                                        <span class="msg-unfollow">Unfollow</span>
                                    </button> --}}
                                    {{-- <br>
                                    <span class="label-trending">
                                        <i class="fa fa-headphones" aria-hidden="true"></i>&nbsp;&nbsp;
                                            @if($rUser->user->hasRole('moderator'))
                                                <p>Moderator</p>
                                            @elseif($rUser->user->hasRole('admin'))
                                                <p>Admin</p>
                                            @elseif($rUser->user->hasRole('regular'))
                                                <p>Regular</p>
                                            @endif
                                    </span> --}}
                                    </div>
                                </div>
                                </div>
                                <div class="row pt-4">
                                    @foreach($rUser->rating as $rate )
                                    <!-- Progress kriteria penilaian -->
                                    <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">{{$rate->nama}}</span>
                                    </div>
                                    <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                        <div class="progress-bar" style="width:{{$rate->nilai}}0%">{{$rate->nilai}}</div>
                                        </div>
                                    </div>
                                    <!-- Progress kriteria penilaian -->
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-lg-8 pl-0">
                                <a href="review_pengguna.html">
                                <div class="col-lg-12 pt-4 px-5 rev-content">
                                    <?=strip_tags(substr($rUser->review->konten,0,150))?>
                                </div>
                                </a>
                            </div>
                            </div>
                        </div>
                        <div class="card-footer text-muted">
                            <a href="">
                            <i class="fa fa-comment" aria-hidden="true"></i> &nbsp;25 Comment</a>
                            <a href="" class="ml-3">
                            <i class="fa fa-share" aria-hidden="true"></i>&nbsp;3 Share</a>
                            <a href="" class="pull-right">Selengkapnya</a>
                        </div>
                        </div>
                    </li>
                    <!--end review pertama -->
                    @endforeach
                @endisset
            </ul>
        </div>
        <!-- end Review Pengguna -->
        <div class="col-lg-12 mt-3">
            <div class="row">
                <div class="col-lg-10">
                <nav>
                    <ul class="pagination">
                    <li class="page-item">
                        <a href="#" class="page-link" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item active">
                        <a href="#" class="page-link">1</a>
                    </li>
                    <li class="page-item">
                        <a href="#" class="page-link">2</a>
                    </li>
                    <li class="page-item">
                        <a href="#" class="page-link">3</a>
                    </li>
                    <li class="page-item ">
                        <a href="#" class="page-link">4</a>
                    </li>
                    <li class="page-item">
                        <a href="#" class="page-link">5</a>
                    </li>
                    <li class="page-item">
                        <a href="#" class="page-link">6</a>
                    </li>
                    <li class="page-item">
                        <a href="#" class="page-link">7</a>
                    </li>
                    <li class="page-item">
                        <a href="#" class="page-link" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    </ul>
                </nav>
                </div>
                <div class="col-lg-2">
                <button class="btn btn-primary float-right" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal"
                    data-target="#buatreview">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Tulis Review</button>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>