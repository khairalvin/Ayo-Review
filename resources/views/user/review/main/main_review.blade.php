@extends('user.layout_main.master')

@section('custom_css')
<link rel="stylesheet" href="{{asset('user/barrating/themes/bars-1to10.css') }}">
@endsection

@section('content')

@include('user.review.main.headReview')
<!-- Kontent Dari full Review -->
<main role="main" id="main-content" class="container main">
  <!-- Main review Section -->
  <div class="row py-3">
    <!-- Row Konten Utama -->
    <div class="col-lg-9 pl-0 pr-2">

        <!-- Slider -->
        @include('user.review.main.sliderReview')
        <!-- end of slider -->

        <!-- inside sp content fullreview -->
        <div class="col-lg-12 mt-2 px-lg-5 py-3 sp-content-fullreview">

          <!-- Navigation tabs -->
          <ul class="nav nav-tabs pt-2 mb-4 pb-2">
            <li class="active mr-4 ml-2">
              <a data-toggle="tab" class="hide" href="#first">Detail Produk</a>
            </li>
            <li>
              <a data-toggle="tab" class="second" href="#second">Review Pengguna&nbsp;
                <span class="badge badge-primary">20</span>
              </a>
            </li>
          </ul>
          <!-- End Navigation tabs -->

          <!-- Tab content -->
          @include('user.review.main.tabcontentReview')
          <!-- end Tab content -->

        </div>
        <!-- end inside sp content fullreview -->

        <!-- Komentar disqus -->
        @include('user.review.main.komentardisqus')
        <!-- end komentar disqus -->

        <!-- Review pengguna  -->
        @include('user.review.main.reviewlain')
        <!-- end Review Pengguna -->

    </div>
    <!-- end Row Konten Utama -->
      
    <div class="col-lg-3 p-0 col-md-offset-2">
      <!-- <div class="col-lg-12 sp-content-fullreview  sidebar pt-4 px-4 pb-0">
        <h2>Apakah Produk ini Relevan ?</h2>
        <ul class="relev">
          <li class="checked">#10 Audiophile Terpopuler</li>
          <li class="haveitm">
            <a href="" class="highlight">9 Teman</a> Memiliki produk ini
            <table width="100%" border="1">
              <tr>
                <td>
                  <a href="">
                    <img class="user online" src="images/profile/ava1.jpg">
                  </a>
                </td>
                <td>
                  <a href="">
                    <img class="user online" src="images/profile/eggsy.jpg">
                  </a>
                </td>
                <td>
                  <a href="">
                    <img class="user online" src="images/profile/brad.jpg">
                  </a>
                </td>
                <td>
                  <a href="">
                    <img class="user" src="images/profile/oda.jpg">
                  </a>
                </td>
                <td>
                  <a href="">
                    <img class="user last" src="images/profile/keanu.jpg">
                  </a>
                  <span>5+</span>
                </td>
              </tr>
            </table>
          </li>
          <li class="haveitm">
            <a href="" class="highlight">3 Teman</a> Mereview produk ini
            <table width="100%" border="1">
              <tr>
                <td>
                  <a href="">
                    <img class="user online" src="images/profile/images.jpg">
                  </a>
                </td>
                <td>
                  <a href="">
                    <img class="user online" src="images/profile/ava1.jpg">
                  </a>
                </td>
                <td>
                  <a href="">
                    <img class="user" src="images/profile/keanu.jpg">
                  </a>
                </td>
                <td></td>
                <td></td>
              </tr>
            </table>
          </li>
          <li class="mt-2">
            <button class="follow btn btn-outline-pilihan ">
              <span class="msg-follow">Favoritkan Produk ini ?</span>
              <span class="msg-following">
                <i class="fa fa-star" aria-hidden="true" style="color: #fdbe00"></i>&nbsp;&nbsp;Favorit</span>
              <span class="msg-unfollow">
                <i class="fa fa-star-o" aria-hidden="true" style="color: #fdbe00"></i>&nbsp;&nbsp;Gak Favorit lagi</span>
            </button>
          </li>
        </ul>
      </div> -->

      {{-- RATING --}}
      @include('user.review.main.ratingReview')
      {{-- END RATING --}}

      {{-- KEUNGGULAN KEKURANGAN --}}
      @include('user.review.main.unggul-kurangReview')
      {{-- KEUNGGULAN KEKURANGAN --}}

    </div>
  </div>
  <!-- End Main review Section -->

  {{-- PRODUK TERKAIT --}}
  @include('user.review.main.produkterkait')
  {{-- end produk terkait --}}
</main>
@endsection

@section('custom_js')
  <script src="{{asset('user/js/sticky-sidebar.js') }}"></script>
  <script src="{{asset('user/ckeditor/ckeditor.js') }}"></script>
  <script src="{{asset('user/barrating/jquery.barrating.min.js') }}"></script>

  <script>
    // Fungsi barating
    // $(function () {
      rate.forEach(function(element){
        $('#rating-'+element).barrating({
          theme: 'bars-1to10'
        });
      });
    // });

    // Fungsi Readmore 
    $(document).ready(function () {
      var readMoreHtml = $(".read-more").html();

      var lessText = readMoreHtml.substr(0, 650);
      if (readMoreHtml.length > 100) {
        $(".read-more").html(lessText);
      } else {
        $(".read-more").html(readMoreHtml);
      }
    }); // end fungsi read-more


    // Fungsi Hide and show untuk konten Review Pada halaman fullreview
    $(document).ready(function () {
      $("#review").hide();
      $(".second").click(function () {
        $('html, body').animate({
          scrollTop: $(".tab-content").offset().top
        }, 500);
        $("#review").show();
        $("#second").show();
        $("#disqus-comment").hide();
      });
    });

    $(document).ready(function () {
      $(".hide").click(function () {
        $('html, body').animate({
          scrollTop: $("main").offset().top
        }, 500);
        $("#review").hide();
        $("#second").hide();
        $("#disqus-comment").show();
      });
    });
    // end Fungsi Hide and show untuk konten Review Pada halaman fullreview

    // Slider menggunakan slick slider pada halaman fullreview 
    $(document).ready(function () {
      $('.review-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        infinite: false,
        adaptiveHeight: true,
        cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        asNavFor: '.slider-nav'
      });

      $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.review-slider',
        dots: false,
        focusOnSelect: true,
        infinite: false,
        arrows: true,
      });

      $('.slider-nav img').click(function () {
        $('img').removeClass('active');
        $(this).addClass("active");
      })
    }); // End fungsi slider

    // Fungsi upload image pada wizard fullreview

    //Check File API support
    if (window.File && window.FileList && window.FileReader) {
      var filesInput = document.getElementById("fileInput");

      filesInput.addEventListener("change", function (event) {

        var files = event.target.files; //FileList object
        var output = document.getElementById("fileResult");

        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          //Only pics
          if (!file.type.match('image'))
            continue;

          var picReader = new FileReader();

          picReader.addEventListener("load", function (event) {

            var picFile = event.target;

            var div = document.createElement("div");
            div.style.display = "inline-block";

            div.innerHTML = "<img class='thumbnail' height='100px' width='100x' src='" + picFile.result + "'" +
              "title='" + picFile.name + "'/>";

            output.insertBefore(div, null);

          });

          //Read the image
          picReader.readAsDataURL(file);
        }

      });
    }
    else {
      console.log("Your browser does not support File API");
    }
    // end Fungsi upload image pada wizard fullreview

    // Create new PRO object
    class Pro{
      constructor(proDetail){
        this.proDetail = proDetail;
      }
    }

    class Cons{
      constructor(consDetail){
        this.consDetail = consDetail;
      }
    }

    class UI{
      addProToList(pro) {
        const list = document.getElementById('proResults');
        // Create tr Element
        const row = document.createElement('ul');
        row.className = 'list-group';
        // Insert cols
        row.innerHTML = `
          <li class="list-group-item mt-2"><input type="hidden" name="resultPros[]" value="${pro.proDetail}">${pro.proDetail}<a href="" class="delete pull-right fa fa-remove"></a></li>
        `;
        list.appendChild(row);
      }

      addConsToList(cons){
        const list = document.getElementById('consResults');
        // Create tr Element
        const row = document.createElement('ul');
        row.className = 'list-group';
        // Insert cols
        row.innerHTML = `
          <li class="list-group-item mt-2"><input type="hidden" name="resultCons[]" value="${cons.consDetail}">${cons.consDetail}<a href="" class="delete pull-right fa fa-remove"></a></li>
        `;
        list.appendChild(row);
      }
      deletePro(target){
        if (target.className === 'delete pull-right fa fa-remove') {
          target.parentElement.parentElement.remove();
        }
      }

      deleteCons(target){
        if (target.className === 'delete pull-right fa fa-remove') {
          target.parentElement.parentElement.remove();
        }
      }
      clearFields(){
        document.getElementById('keunggulan').value = '';
      }
    }

    class Storage{
      // Get Book from storage
      static getPros() {
        let pros;
        if (localStorage.getItem('pros') === null) {
          pros = [];
        } else {
          pros = JSON.parse(localStorage.getItem('pros'));
        }
        return pros;
      }

      // Display book from Storage
      static displayPros() {
        const pros = Storage.getPros();
        pros.forEach(function (pro) {
          const ui = new UI;
          // Add book to UI
          ui.addProToList(pro);
        });
      }

      // Add book from storage
      static addPros(pro) {
        const pros = Storage.getPros();
        pros.push(pro);
        localStorage.setItem('pros', JSON.stringify(pros));
      }

      static removePros(proDetail){
        const pros = Storage.getPros();
        pros.forEach(function (pro, index) {
          if (pro.proDetail === proDetail) {
            pros.splice(index, 1);
          }
        });

        localStorage.setItem('pros', JSON.stringify(pros));
      }
    }
    
    // DOM load Event
    document.addEventListener('DOMContentLoaded', Storage.displayPros);

    // event listener on click
    document.getElementById('btnPros').addEventListener('click', function(e){
      e.preventDefault();
      // Get form values
      const proDetail = document.getElementById('keunggulan').value;
      
      // Instantiate new pro
      const pro = new Pro(proDetail);

      // Instantiate UI
      const ui = new UI();

      // Validate
      if (proDetail === '') {
        alert('Error Form tidak boleh Kosong');
      }else{
        // add pro to list
        ui.addProToList(pro);
        // Add to local Storage
        // Storage.addPros(pro);
        // Clear Field
        ui.clearFields();
      }
    });

    // event listener on click
    document.getElementById('btnCons').addEventListener('click', function (e) {
      e.preventDefault();
      // Get form values
      const consDetail = document.getElementById('kekurangan').value;

      // Instantiate new pro
      const cons = new Cons(consDetail);

      // Instantiate UI
      const ui = new UI();

      // Validate
      if (consDetail === '') {
        alert('Error Form tidak boleh Kosong');
      } else {
        // add pro to list
        ui.addConsToList(cons);
        // Add to local Storage
        // Storage.addPros(pro);
        // Clear Field
        ui.clearFields();
      }
    });
    

    document.getElementById('proResults').addEventListener('click', function(e){
      e.preventDefault();
      const ui = new UI();
      ui.deletePro(e.target);
      Storage.removePros(e.target.parentElement.textContent);
    });

    document.getElementById('consResults').addEventListener('click', function(e){
      e.preventDefault();
      const ui = new UI();
      ui.deleteCons(e.target);
    })
    document.getElementById('btnRec').addEventListener('click', function(e){
      e.preventDefault();
      let result = document.getElementById('recResults');
      result.innerHTML = `
        <div class="text-center">
          <h3 style = "background-color: #e4f4fa;
          width: 85 % ;
          border: solid 1 px #3fc4f5;" class="py-4">Saya Merekomendasikan Ini</h3>
          <input type="hidden" name="recommend" value="1">
        </div>
      `;
    })

    document.getElementById('btnUnrec').addEventListener('click', function (e) {
      e.preventDefault();
      let result = document.getElementById('recResults');
      result.innerHTML = `
        <div class="text-center">
          <h3 style = " background-color: #f7d9d5;
          width: 85 % ;
          border: solid 1 px# F45944;" class="py-4">Saya Tidak Merekomendasikan Ini</h3>
          <input type="hidden" name="recommend" value="0">
        </div>
      `;
    })
  </script>
@endsection
