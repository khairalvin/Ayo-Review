<div class="col-lg-12 sp-content-fullreview sidebar p-4" id="sidebar">
    <h2>RATING
        <span>RATA-RATA</span>
    </h2>
    <div class="row pt-3">
        @foreach($ratings as $rating)
        <!-- Progress kriteria penilaian 1 -->
        <div class="col-lg-5 pr-0 pb-2">
            <span class="label-kreteria">{{$rating['nama']}}</span>
        </div>
        <div class="col-lg-7 pl-0">
            <div class="progress">
                <div class="progress-bar" style="width:{{$rating['nilai']}}0%">{{$rating['nilai']}}</div>
            </div>
        </div>
        <!-- End Progress kriteria penilaian 1 -->
        @endforeach

        <div class="col-lg-4 pr-0">
            <div class="circle-big left">
                <div class="progress blue">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                    <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                    <div class="progress-value">{{$total_rating['nilai']}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 pl-0">
        <h3>{{$total_rating['keterangan']}}</h3>
        <span class="tot-rev">25 Review Pengguna</span>
        </div>
    </div>
</div>