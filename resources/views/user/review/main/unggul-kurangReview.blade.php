{{-- KEUNGGULAN --}}
<div class="col-lg-12 sp-content-fullreview  sidebar mt-2 pt-4 px-4 pb-2">
    <h2>KEUNGGULAN</h2>
    <ul class="pro">
        @foreach($fitur_keunggulan as $plus)
            <li>{{$plus->konten}}</li>
        @endforeach
    </ul>
</div>
{{-- END KEUNGGULAN --}}

{{-- KEKURANGAN --}}
<div class="col-lg-12 sp-content-fullreview  sidebar mt-2 pt-4 px-4 pb-2">
    <h2>KEKURANGAN</h2>
    <ul class="con">
        @foreach($fitur_kekurangan as $minus)
            <li>{{$minus->konten}}</li>
        @endforeach
    </ul>
</div>
{{-- END KEKURANGAN --}}