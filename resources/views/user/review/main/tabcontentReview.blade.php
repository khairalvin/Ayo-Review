<div class="tab-content">
    <!-- Modal AddReview -->
    @include('user.review.main.addReview')
    <!-- end Modal -->

    <!-- Kontent tab pertama :  artikel -->
    <div id="first" class="tab-pane active px-3">
        <?=$review->konten?>
    </div>
    <!-- End Kontent tab pertama :  artikel -->

    <!-- Kontent Tab Kedua : review Pengguna -->
    <div id="second" class="tab-pane fade">
        <input  data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#buatreview" class="form-control post-review py-4 mb-2" type="text" placeholder="Berbagi review, foto, video atau pengalamanmu tentang produk ini">
          <!-- <button class="btn btn-post-review" type="button">
            <i class="fa fa-camera" aria-hidden="true"></i>&nbsp;&nbsp;Berbagi gambar</button> -->
          <!-- <button class="btn btn-post-review" type="button"> -->
        <i class="fa fa-video-camera" style="color:#fff;" aria-hidden="true"></i>
        @if(Auth::id())
            <button class="btn btn-primary float-right" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal"
                data-target="#buatreview">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Tulis Review
            </button>
        @else
            <button class="btn btn-primary float-right" type="button" data-backdrop="static" data-keyboard="false" data-toggle="modal"
                data-target="#myModal">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Tulis Review
            </button>
        @endif
    </div>
</div>