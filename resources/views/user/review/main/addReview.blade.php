<div id="buatreview" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 800px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <p>Ulasan : {{$review->judul}}</p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <form enctype="multipart/form-data" action="{{route('user-review-store')}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="review_id" value="{{$review->id}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3 pr-0">
                        <img src="{{ asset('admin/img/review/'.$image.'') }}" alt="" width="100%" height="160px">
                        <h2 class="text-center mt-2">{{$review->judul}}</h2>
                        <hr class="my-1">

                        <table>
                            <tbody>
                                @php($rates = array())
                                @foreach($ratings as $rating)
                                    @php(array_push($rates,$rating['indikator_id']))
                                    <tr>
                                        <th>{{$rating['nama']}}</th>
                                    </tr>
                                    <tr>
                                        <th>
                                        <select id="rating-{{$rating['indikator_id']}}" name="rating-{{$rating['indikator_id']}}">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5" selected>5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </th>
                                    </tr>
                                @endforeach
                                @php($rate =json_encode($rates))
                                <script>
                                    var rate = {{$rate}};
                                </script>
                            </tbody>
                        </table>

                    </div>
                    <div class="col-lg-9 ">
                        <ul class="nav nav-tabs mb-2" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home"
                                aria-selected="true">Berikan Ulasan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-pro-tab" data-toggle="pill" href="#pills-pro" role="tab" aria-controls="pills-pro"
                                aria-selected="false">Keunggulan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-cons-tab" data-toggle="pill" href="#pills-cons" role="tab" aria-controls="pills-cons"
                                aria-selected="false">Kekurangan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-cons-tab" data-toggle="pill" href="#pills-rec" role="tab" aria-controls="pills-rec" aria-selected="false">Rekomendasi Kamu</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="c-preview">
                                    <div>
                                        {{-- <form action="" methode="post" enctype="multipart/form-data"> --}}
                                        <textarea name="editor" class="ckeditor" style=" resize: none; line-height: unset;  width: 100%;">
                                            </textarea>
                                        {{-- </form> --}}
                                    </div>
                                </div>
                                <div class="c-pictures">
                                    <div class="c-upload">
                                        <label class="pull-left">
                                        <input type="file" name="fileInput[]" id="fileInput" multiple="multiple">
                                        + Lampirkan gambar
                                        </label>
                                    </div>
                                    <div class="c-result" id="fileResult"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-pro" role="tabpanel" aria-labelledby="pills-pro">
                                <div id="formPro">
                                    <div class="form-group mt-3">
                                        <label class="align-center">Berikan keunggulan mengenai produk ini ( maks 4 keunggulan )</label>
                                        <input type="text" id="keunggulan" class="form-control" placeholder="Masukan Keunggulan">
                                    </div>
                                    <button id="btnPros" class="btn btn-success" type="button">Tambah Keunggulan</button>
                                </div>
                                <div id="proResults" class="mt-3">
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-cons" role="tabpanel" aria-labelledby="pills-cons">
                                <div id="formCons">
                                    <div class="form-group mt-3">
                                        <label class="align-center">Berikan kekurangan mengenai produk ini  ( maks  keunggulan )</label>
                                        <input type="text" id="kekurangan" class="form-control" placeholder="Masukan Kekurangan">
                                    </div>
                                    <button id="btnCons" class="btn btn-danger" type="button">Tambah Kekurangan</button>
                                </div>
                                <div id="consResults" class="mt-3">
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-rec" role="tabpanel" aria-labelledby="pills-rec">
                                {{-- <div id="recFields"> --}}
                                    <div class="form-group text-center mt-4 mb-1">
                                        <label for="" >Berikan Rekomendasi kamu mengenai produk ini</label>
                                    </div>
                                    <center>
                                        <button id="btnRec" class="btn btn-outline-success" type="button" style="height: 80px; font-size: 24px;"><i class="fa fa-thumbs-up"></i> Recommended</button>
                                        <button id="btnUnrec" class="btn btn-outline-danger" type="button" style="height: 80px; font-size: 24px;"><i class="fa fa-thumbs-down"></i> Not Recommended</button>
                                    </center>
                                {{-- </form> --}}
                                <div id="recResults" class="mt-3">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success float-right">Selajutnya</button>
            </div>
        </form>
        </div>
    </div>
</div>