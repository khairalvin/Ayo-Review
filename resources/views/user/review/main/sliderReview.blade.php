<div class="col-lg-12 px-0 sp-content-fullreview">
    <div class="review-slider justify-content-centerr mb-2">

        <!-- Konten slider 1st -->
        {{-- <div>
        <div class="embed-responsive embed-responsive-21by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/K-kA95uQWeg?rel=0&amp;showinfo=0&amp;theme=light"
            allowfullscreen></iframe>
        </div>
        </div> --}}
        <!-- Konten slider 1st -->

        <!-- Konten Slider 2nd -->
        @foreach($images as $img1)
        <div>
            <center>
                <img src="{{ asset('admin/img/review/'.$img1->name.'') }}" alt="" style="width:100%;">
            </center>
        </div>
        @endforeach
        <!-- end Konten Slider 2nd -->
    </div>

    <!-- navigasi dari slider -->
    <div class="slider-nav row m-0 px-2">
        @foreach($images as $img2)
        <div>
            <img src="{{ asset('admin/img/review/'.$img2->name.'') }}" alt="" style="width:174px;height:114px;">
        </div>
        @endforeach
    </div>
    <!-- end navigasi dari slider -->
</div>