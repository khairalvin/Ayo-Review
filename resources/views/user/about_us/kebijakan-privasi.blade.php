@extends('user.layout_main.master')
@section('custom_css')
  {{-- GAK TAU WKWK --}}
  <link rel="stylesheet" href="{{asset('user/css/footerar.css') }}">
@endsection
@section('content')
<!-- ABOUT US -->
<main class="container kebijakan-margin">
  <div class="row"> 
  <!-- Cirlce Starts -->

    <div class="col-sm-3 wowload fadeInLeft">   
      <div class="vertical-menu">
            <a href="/aboutus">Tentang Kita</a>
            <a href="/syarat-ketentuan" >Syarat & Ketentuan</a>
            <a href="#" class="active">Kebijakan Privasi</a>
            <a href="/panduan">Panduan</a>
            <a href="/anti-bajakan">Anti Bajakan</a>
      </div>
    </div>
    <div class="col-sm-9 isi-footer">
          <h4><b>Kebijakan Privasi</b></h4>
            <hr>
            <p>
           <b> Para pengguna Ayoreview yang terhormat,</b><br><br>
           Terimakasih telah mengunjungi situs kami www.ayoreview.com. Kami selalu berupaya mewujudkan komitmen kami kepada para pengguna, dengan memberikan layanan yang terbaik antara lain berupa menghargai dan menjaga informasi pribadi milik Anda yang kami percaya bersifat pribadi, rahasia serta sangat bernilai bagi Anda. Oleh karena itu kebijakan privasi ini merupakan komitmen nyata dari kami untuk menghargai dan melindungi setiap informasi pribadi Anda.<br><br>
          Dengan mengunjungi & mendaftarkan diri Anda sebagai pengguna Ayoreview maka kami anggap Anda telah membaca, memahami dan menyetujui apapun - dan ketentuan terkait pengumpulan dan penggunaan data pribadi Anda baik pada - dan ketentuan yang berlaku ataupun yang akan diperbaharui pada masa mendatang. Kami menyarankan anda untuk membaca dan memeriksa halaman Kebijakan Privasi ini dari waktu kewaktu. Dengan tetap mengakses dan menggunakan situs Ayoreview, maka pengguna dianggap menyetujui adanya perubahan-perubahan yang dapat terjadi tanpa pemberitahuan sebelumnya.
          </p> 
            <hr>
            <p>
              <b>Informasi Pengguna  </b><br><br>
          Ayoreview membutuhkan informasi pengguna dengan tujuan untuk memproses penggunaan seluruh fitur pada situs Ayoreview. Informasi tersebut Kami kumpulkan ketika Pengguna melakukan pendaftaran ke situs Ayoreview, ketika menggunakan layanan Ayoreview dan ketika Anda menghubungi Ayoreview. Kami mengolah informasi pengguna untuk dapat digabungkan dengan para mitra usaha atau perusahaan lain.<br><br>
          Beberapa data pribadi yang kami kumpulkan sewaktu Anda melakukan pendaftaran pada situs Ayoreview antara lain, nama lengkap, alamat email, tempat dan tanggal lahir, nomer telepon, password dan informasi lainnya yang diperlukan. Dengan mendaftarkan diri Anda pada situs Ayoreview, Kami anggap anda memahami bahwa Ayoreview membutuhkan setiap informasi atau data pengguna untuk kelancaran dan keamanan situs ini. Setiap informasi atau data yang Anda berikan akan Kami simpan dan jaga sebaik-baiknya dengan memperhatikan faktor keamanan.
            </p>   
            <hr>
            <p>
              <b>Penggunaan Informasi</b><br><br>
          Ayoreview dapat menggunakan keseluruhan informasi atau data pengguna untuk peningkatan produk dan layanan. Informasi atau data pengguna digunakan dan diolah dengan maksud untuk menyesuaikan minat pengguna. Ayoreview dapat menggunakan keseluruhan informasi atau data pengguna untuk kebutuhan internal Kami mengenai riset pengguna terhadap suatu produk. Ayoreview dapat menghubungi Anda melalui email, surat atau telepon untuk kebutuhan internal. Situs Ayoreview mungkin memiliki keterhubungan dengan situs situs lain, dengan demikian pengguna dapat menyadari dan memahami Ayoreview tidak ikut bertanggung jawab terhadap kerahasiaan informasi pengguna setelah pengguna meninggalkan situs Ayoreview.
            </p>
            <hr>
            <p>
              <b>Pengungkapan Informasi</b><br><br>
          Ayoreview tidak akan menjual, mengalihkan, mendistribusikan atau membuka informasi dan data pribadi Anda kepada orang lain atau Pihak Ketiga yang tidak berkepentingan tanpa seizin Anda.<br><br>
          Namun Ayoreview dapat memberikan informasi atau data pengguna kepada pihak pihak yang masih berkaitan dengan Kami dengan  pihak-pihak tersebut wajib mematuhi Kebijakan Privasi ini. Beberapa pihak-pihak tersebut antara lain mitra usaha yang bekerjasama dengan Kami dalam membantu proses layanan, investigasi dan verifikasi data pribadi pengguna. Pihak lain yang Kami berikan tugas untuk  melaksanakan pekerjaan yang membutuhkan informasi atau data pengguna dalam melaksanakan pekerjaannnya.<br><br>
          Apabila Ayoreview secara keseluruhan atau sebagian asetnya diakuisisi atau merger dengan Pihak Ketiga, maka data pribadi yang Kami miliki akan menjadi salah satu aset yang dialihkan atau digabungkan. Apabila Ayoreview berkewajiban mengungkapkan atau berbagi data pribadi pengguna dalam upaya mematuhi kewajiban hukum, maka dalam rangka mematuhi hukum, Kami dapat membukan akses atas informasi atau data pengguna yang diperlukan. Sehubungan dengan hal tersebut, maka Kami harap Anda membebaskan Kami dari segala klaim, tuntutan dan atau gugatan yang berkaitan dengan pemberian akses atas informasi tersebut.
            </p>
            <hr>
            <p>
              <b>Cookies</b><br><br>
          Cookies adalah sejenis file yang berisi informasi yang dikirimkan oleh suatu situs ke perangkat penyimpanan komputer pengguna setiap kali pengaksesan situs tersebut untuk keperluan pencatatan oleh komputer pengakses seperti, alamat Internet Protocol (IP Address), tipe browser dan sistem operasi, tanggal dan waktu pengaksesan, data yang berfungsi untuk memonitor kegiatan, alamat situs yang diakses.<br><br>
          Ayoreview menggunakan cookies untuk berbagai tujuan antara lain membantu pengguna untuk mengunjungi kembali situs Ayoreview, menghitung jumlah orang yang melakukan pengaksesan ke situs Ayoreview.
          Walaupun perangkat komputer pengguna secara otomatis menerima cookies, pengguna dapat menentukan pilihan untuk menolak cookies pada pengaturan browser.<br><br>
          Ayoreview dapat dan berhak menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka kebutuhan internal untuk peningkatan mutu konten dan layanan Ayoreview, termasuk diantaranya penyesuaian oklan kepada setiap Pengguna berdasarkan minat dan riwayat kunjungan.
            </p>
            <hr>
            <p>
            <b>Hukum</b><br><br>
          - dan ketentuan dalam Kebijakan Privasi ini tunduk kepada hukum di wilayah Negara Kesatuan Republik Indonesia.
            </p>
            <hr>
            <p>
              <b>Kritik dan Saran</b><br><br>
          Segala jenis kritik dan saran yang membangun, ataupun keperluan lain dapat disampaikan ke Kontak Kami.

            </p>
          <center>
            <p>
              <b>Pembaruan Terakhir :</b><br>
          18 Desember 2017 12:30

            </p>
          </center>
    </div>
  </div>
</main>
@endsection
@section('custom_js')
  {{-- Mereun aya js tambahan --}}
@endsection