@extends('user.layout_main.master')
@section('custom_css')
    <link rel="stylesheet" href="{{asset('user/css/footerar.css') }}">
@endsection
@section('content')
<!-- Cirlce Starts -->
<div id="about" class="spacer_container about">
    <center>
        <img src="{{asset('user/images/arboxlogo.png') }}" class="text-center wowload fadeInUp" style="width: 4.1em">
        <img src="{{asset('user/images/arlogo.png') }}" class="text-center wowload fadeInUp" style="width: 16em">
    </center>
    <div class="row tentang_kami">
        <div class="col-sm-6 wowload fadeInLeft">
        <h4>
            <i class="fa fa-pencil"></i> Ayoreview</h4>
        <p>Ayoreview adalah platform website yang menyajikan pengalaman baru dalam memberikan informasi suatu produk. Seperti namanya
        ayoreview mengajak kita semua untuk saling memberikan review tentang suatu produk. Karena setiap orang bisa memberikan ulasan
        mereka dengan mudah, review suatu barang akan menjadi lebih objektif dari berbagai sudut pandang. Dengan ayoreview kita tidak
        perlu lagi repot mencari review produk di website lain. Cukup dengan ayoreview kita dapat dengan mudah menemukan berbagai
        macam review produk dari berbagai ulasan orang.</p>
        </div>
        <div class="col-sm-6 wowload fadeInRight">
        <h4>
            <i class="fa fa-history"></i> Sejarah</h4>
        <p>Ayoreview dikembangkan untuk menjawab permasalahan klasik pada suatu forum komunitas. Pertanyaan klasik yang sering muncul
        untuk menanyakan review suatu produk dan sulitnya mendapatkan review berbahasa indonesia. Kalaupun ada review tersebut tidak
        tersimpan dengan baik atau berada pada platform yang berbeda beda. Kami mencoba menyelesaikan permasalahan tersebut dengan
        mengembangkan sebuah website dimana setiap orang dapat menulis review mereka. Dan siapapun dapat dengan mudah menemukan dan
        membaca review yang mereka inginkan. Oleh karena itu ayoreview didirikan pada</p>
        </div>
    </div>
    <!-- <h3 class="text-center wowload fadeInUp">Career</h3>
    <center>
        <div class="ketengah-bawah wowload fadeInUp">
        <p>Kami selalu mencari orang-orang yang mencintai apa yang mereka lakukan dan memiliki motivasi untuk memajukan AyoReview.
            Jika anda merasa demikian, kami ingin bicara. Jelajahi peluang karir anda di Ayo Review.</p>
        </div>
    </center>

    <center>
        <div class="col-md-4">
        <button class="btn btn-send ketengah-bawah"> Gabung Bersama Kami</button>
        </div>
    </center> -->
    <br>
    <br>
    <br>
    <!-- About Starts -->
    <div class="highlight-info-aboutus">
        <div class="overlays spacer">
            <div class="container">
                <div class="row text-center  wowload fadeInDownBig">
                <div class="col-sm-3 col-xs-6">
                    <i class="fa fa-smile-o  fa-5x"></i>
                    <h4>2,432 Pengguna</h4>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <i class="fa fa-pencil-square-o  fa-5x"></i>
                    <h4>175 Reviews</h4>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <i class="fa fa-trophy  fa-5x"></i>
                    <h4>7 Penghargaan</h4>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <i class="fa fa-map-marker fa-5x"></i>
                    <h4>2 Kantor</h4>
                </div>
                </div>
            </div>
        </div>

    </div>
    <!-- About Ends -->
    <div id="partners" class="spacer-investor">
  <h3 class="text-center wowload fadeInUp">Investors</h3>
  <div class="clearfix">
    <div class="wowload fadeInLeft pl-5 pr-5">
        <div class="investor-img">
         <img src="{{asset('user/images/partners/1.jpg')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/2.jpg')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/3.jpg')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/4.jpg')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/5.png')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/5.png')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/4.jpg')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/5.png')}}" alt="partners" style="height: 40px">
        </div>
        <div class="investor-img">
         <img src="{{asset('user/images/partners/6.png')}}" alt="partners" style="height: 40px">
        </div>
        <!--
          <div class="col-sm-12 text-center wowload fadeInLeft">
        <span class="investor-img">
         <img src="images/partners/1.jpg" alt="partners" style="height: 40px">
        </span>
        <span class="investor-img">
         <img src="images/partners/2.jpg" alt="partners" style="height: 40px">
        </span>
        <span class="investor-img">
         <img src="images/partners/3.jpg" alt="partners" style="height: 40px">
        </span>
        <span class="investor-img">
         <img src="images/partners/4.jpg" alt="partners" style="height: 40px">
        </span>
        <br>
        <span class="investor-img">
         <img src="images/partners/5.png" alt="partners" style="height: 40px">
        </span>
        <span class="investor-img">
         <img src="images/partners/6.png" alt="partners" style="height: 40px">
        </span>
    </div> -->
    </div>
  </div>
</div>
    <!-- team -->
    <h3 class="text-center  wowload fadeInUp mt-5">Founders</h3>
    <p class="text-center  wowload fadeInLeft">Team kita yang kreatif membuat apapun menjadi mungkin</p>
    <div class="row grid team_members wowload fadeInUpBig">
        <div class="col">
        <figure class="effect-chico">
            <img src="{{asset('user/images/team/gema.jpg') }}" alt="img01" class="img-responsive" />
            <figcaption>
            <p>
                <b>GEMA SYAHIDAN</b>
                <br>FRONTEND developer
                <br>
                <br>
                <a href="#">
                <i class="fa fa-dribbble"></i>
                </a>
                <a href="#">
                <i class="fa fa-facebook"></i>
                </a>
                <a href="#">
                <i class="fa fa-twitter"></i>
                </a>
            </p>
            </figcaption>
        </figure>
        </div>
        <div class="col">
        <figure class="effect-chico">
            <img src="{{asset('user/images/team/fauzan.jpg') }}" alt="img01" />
            <figcaption>
            <p>
                <b>FAUZAN ADHI RACHMAN</b>
                <br>Office Boy
                <br>
                <br>
                <a href="#">
                <i class="fa fa-dribbble"></i>
                </a>
                <a href="#">
                <i class="fa fa-facebook"></i>
                </a>
                <a href="#">
                <i class="fa fa-twitter"></i>
                </a>
            </p>
            </figcaption>
        </figure>
        </div>
        <div class="col">
        <figure class="effect-chico">
            <img src="{{asset('user/images/team/agum.jpg') }}" alt="img01" />
            <figcaption>
            <p>
                <b>AGUM JUNIANTO</b>
                <br>FRONTEND developer
                <br>
                <br>
                <a href="#">
                <i class="fa fa-dribbble"></i>
                </a>
                <a href="#">
                <i class="fa fa-facebook"></i>
                </a>
                <a href="#">
                <i class="fa fa-twitter"></i>
                </a>
            </p>
            </figcaption>
        </figure>
        </div>
        <div class="col">
        <figure class="effect-chico">
            <img src="{{asset('user/images/team/hes.jpg') }}" alt="img01" />
            <figcaption>
            <p>
                <b>Fachrian Noor</b>
                <br>Backend Developer
                <br>
                <br>
                <a href="#">
                <i class="fa fa-dribbble"></i>
                </a>
                <a href="#">
                <i class="fa fa-facebook"></i>
                </a>
                <a href="#">
                <i class="fa fa-twitter"></i>
                </a>
            </p>
            </figcaption>
        </figure>
        </div>
        <div class="col">
        <figure class="effect-chico">
            <img src="{{asset('user/images/team/alvin.jpg') }}" alt="img01" />
            <figcaption>
            <p>
                <b>ALVIN KHAIR</b>
                <br>BACKEND developer
                <br>
                <br>
                <a href="#">
                <i class="fa fa-dribbble"></i>
                </a>
                <a href="#">
                <i class="fa fa-facebook"></i>
                </a>
                <a href="#">
                <i class="fa fa-twitter"></i>
                </a>
            </p>
            </figcaption>
        </figure>
        </div>
    </div>
    <!--Contact Starts-->
    <div id="contact" class="spacer">
        <div class="container contactform center">
        <h2 class="text-center  wowload fadeInUp">Hubungi Kami</h2>
        <div class="row wowload fadeInLeftBig">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            <input type="text" placeholder="Nama">
            <input type="text" placeholder="Status">
            <textarea rows="5" placeholder="Pesan"></textarea>
            <button class="btn btn-send">
                <i class="fa fa-paper-plane"></i> Kirim</button>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection