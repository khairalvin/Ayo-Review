@extends('user.layout_main.master')
@section('custom_css')
  {{-- GAK TAU WKWK --}}
  <link rel="stylesheet" href="{{asset('user/css/footerar.css') }}">
@endsection
@section('content')
<!-- ABOUT US -->
<main class=" container white-color kebijakan-margin">
  <div class="row"> 

  <!-- Cirlce Starts -->
    <div class="col-sm-3 wowload fadeInLeft">
      
    <div class="vertical-menu">
          <a href="/aboutus">Tentang Kita</a>
          <a href="#" class="active">Syarat & Ketentuan</a>
          <a href="/kebijakan-privasi" >Kebijakan Privasi</a>
          <a href="/panduan">Panduan</a>
          <a href="/anti-bajakan">Anti Bajakan</a>
        </div>

    </div>
    <div class="col-sm-9 wowload isi-footer fadeInRight">
  <h4><b>Syarat & Ketentuan</b></h4>
    <hr>
    <p>Kami mengucapkan selamat data disitus review nomor satu di Indonesia <a href="ayoreview.com"> www.Ayoreview.com</a>
  <br><br>
   Ayoreview merupakan situs review yang menjunjung tinggi nilai kebebasan berpendapat yang tetap memegang teguh asas bertanggungjawab. Sebelum mengakses dan menggunakan layanan Ayoreview, kami menyarankan Anda untuk membaca Syarat & ketentuan pada halaman ini. Syarat & ketentuan ini dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu.<br><br>
  Dengan melakukan pendaftaran atau menggunakan situs www.Ayoreview.com, maka Kami anggap Anda telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat & ketentuan. Apabila Anda tidak menyetujui salah satu, sebagian atau seluruh isi Syarat & ketentuan, maka Kami mohon untuk tidak mengakses dan menggunakan layanan dari Ayoreview. Syarat & ketentuan ini bersifat mutlak dan tidak dapat diganggu gugat.
  </p> 
    <hr>
    <p>
      <b>Definisi</b><br><br>
  Ayoreview membutuhkan informasi pengguna dengan tujuan untuk memproses penggunaan seluruh fitur pada situs Ayoreview. Informasi tersebut Kami kumpulkan ketika Pengguna melakukan pendaftaran ke situs Ayoreview, ketika menggunakan layanan Ayoreview dan ketika Anda menghubungi Ayoreview. Kami mengolah informasi pengguna untuk dapat digabungkan dengan para mitra usaha atau perusahaan lain.<br><br>
  Beberapa data pribadi yang kami kumpulkan sewaktu Anda melakukan pendaftaran pada situs Ayoreview antara lain, nama lengkap, alamat email, tempat dan tanggal lahir, nomer telepon, password dan informasi lainnya yang diperlukan. Dengan mendaftarkan diri Anda pada situs Ayoreview, Kami anggap anda memahami bahwa Ayoreview membutuhkan setiap informasi atau data pengguna untuk kelancaran dan keamanan situs ini. Setiap informasi atau data yang Anda berikan akan Kami simpan dan jaga sebaik-baiknya dengan memperhatikan faktor keamanan.
    </p>   
    <hr>
    <p>
      <b>Penggunaan Informasi</b><br><br>
  Ayoreview dapat menggunakan keseluruhan informasi atau data pengguna untuk peningkatan produk dan layanan. Informasi atau data pengguna digunakan dan diolah dengan maksud untuk menyesuaikan minat pengguna. Ayoreview dapat menggunakan keseluruhan informasi atau data pengguna untuk kebutuhan internal Kami mengenai riset pengguna terhadap suatu produk. Ayoreview dapat menghubungi Anda melalui email, surat atau telepon untuk kebutuhan internal. Situs Ayoreview mungkin memiliki keterhubungan dengan situs situs lain, dengan demikian pengguna dapat menyadari dan memahami Ayoreview tidak ikut bertanggung jawab terhadap kerahasiaan informasi pengguna setelah pengguna meninggalkan situs Ayoreview.
    </p>
    <hr>
    <p>
      <b>Pengungkapan Informasi</b><br><br>
  Ayoreview tidak akan menjual, mengalihkan, mendistribusikan atau membuka informasi dan data pribadi Anda kepada orang lain atau Pihak Ketiga yang tidak berkepentingan tanpa seizin Anda.<br><br>
  Namun Ayoreview dapat memberikan informasi atau data pengguna kepada pihak pihak yang masih berkaitan dengan Kami dengan syarat pihak-pihak tersebut wajib mematuhi Kebijakan Privasi ini. Beberapa pihak-pihak tersebut antara lain mitra usaha yang bekerjasama dengan Kami dalam membantu proses layanan, investigasi dan verifikasi data pribadi pengguna. Pihak lain yang Kami berikan tugas untuk  melaksanakan pekerjaan yang membutuhkan informasi atau data pengguna dalam melaksanakan pekerjaannnya.<br><br>
  Apabila Ayoreview secara keseluruhan atau sebagian asetnya diakuisisi atau merger dengan Pihak Ketiga, maka data pribadi yang Kami miliki akan menjadi salah satu aset yang dialihkan atau digabungkan. Apabila Ayoreview berkewajiban mengungkapkan atau berbagi data pribadi pengguna dalam upaya mematuhi kewajiban hukum, maka dalam rangka mematuhi hukum, Kami dapat membukan akses atas informasi atau data pengguna yang diperlukan. Sehubungan dengan hal tersebut, maka Kami harap Anda membebaskan Kami dari segala klaim, tuntutan dan atau gugatan yang berkaitan dengan pemberian akses atas informasi tersebut.
    </p>
    <hr>
    <p>
      <b>Cookies</b><br><br>
  Cookies adalah sejenis file yang berisi informasi yang dikirimkan oleh suatu situs ke perangkat penyimpanan komputer pengguna setiap kali pengaksesan situs tersebut untuk keperluan pencatatan oleh komputer pengakses seperti, alamat Internet Protocol (IP Address), tipe browser dan sistem operasi, tanggal dan waktu pengaksesan, data yang berfungsi untuk memonitor kegiatan, alamat situs yang diakses.<br><br>
  Ayoreview menggunakan cookies untuk berbagai tujuan antara lain membantu pengguna untuk mengunjungi kembali situs Ayoreview, menghitung jumlah orang yang melakukan pengaksesan ke situs Ayoreview.
  Walaupun perangkat komputer pengguna secara otomatis menerima cookies, pengguna dapat menentukan pilihan untuk menolak cookies pada pengaturan browser.<br><br>
  Ayoreview dapat dan berhak menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka kebutuhan internal untuk peningkatan mutu konten dan layanan Ayoreview, termasuk diantaranya penyesuaian oklan kepada setiap Pengguna berdasarkan minat dan riwayat kunjungan.
    </p>
    <hr>
    <p>
    <b>Hukum</b><br><br>
  Syarat-syarat dan ketentuan dalam Kebijakan Privasi ini tunduk kepada hukum di wilayah Negara Kesatuan Republik Indonesia.
    </p>
    <hr>
    <p>
      <b>Pembaharuan</b><br><br>
  Syarat & ketentuan dapat berubah seiring waktu tanpa pemberitahuna sebelumnya. Kami menyerankan agar anda membaca secara menyeluruh dan seksama halam Syarat & ketentuan ini dari waktu ke waktu untuk mengetahui adanya perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Ayoreview, maka Kami anggap Anda menyetujui perubahan dalam Syarat & ketentuan.


    </p>
  <center>
    <p>
      <b>Pembaruan Terakhir :</b><br>
  18 Desember 2017 12:30

    </p>
    </div>
  </div>
</main>
@endsection
@section('custom_js')
  {{-- Mereun aya js tambahan --}}
@endsection