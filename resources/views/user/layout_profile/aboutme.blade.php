<?php
use App\Provinsi;
?>
<div class=" mr-review-header bg-sidebar-profile col-lg-3 px-0 pt-0 mb-2">
        <!-- header dari profile sidebar -->
        <div class="col-lg-12 sp-inside-kat sp-header mb-0">
          <div class="col-lg-12 p-kat">
            <p>
              ABOUT ME
              @isset(Auth::user()->username)
                @if($user->username == Auth::user()->username)
                  <a href="/profile_edit" class="pull-right">
                @endisset
              @endisset
                <i class="fa fa-pencil-square-o p-0" aria-hidden="true"></i>
              </a>
            </p>
          </div> 
        </div>
        <!-- end dari header profile sidebar -->

        <!-- konten dari profile sidebar -->
        <div class="col-lg-12 profile-sidebar">
            @isset($user->biodata)<div class="quotes">{{$user->biodata}}</div>@endisset
          <table>
            <tr>
              @isset($user->first_name)
                <td>
                  <i class="fa fa-user" aria-hidden="true"></i>
                </td>
                <td>{{$user->first_name}}@isset($user->last_name){{$user->last_name}}@endisset</td>
              @endisset
            </tr>
            <tr>
              @isset($user->region)
                <td>
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                </td>
                <td>Asal dari
                    @if($user->region == NULL)
                    @else
                      {{Provinsi::provinsi($user->region)}}
                    @endif  
                </td>
              @endisset
            </tr>
            <tr>
              <td>
                <i class="fa fa-envelope" aria-hidden="true"></i>
              </td>
              <td>@isset($user->email){{$user->email}}@endisset</td>
            </tr>
            <tr>
              @isset($user->web)
                <td>
                  <i class="fa fa-link" aria-hidden="true"></i>
                </td>
                <td>
                <a href="{{$user->web}}">@isset($user->web){{$user->web}}@endisset</a>
                </td>
              @endisset
            </tr>
            <tr>
              <td>
                <i class="fa fa-calendar" aria-hidden="true"></i>
              </td>
              <td>Telah Bergabung Sejak : @isset($user->created_at){{date_format($user->created_at,'Y')}}@endisset</td>
            </tr>
          </table>
        </div>
        <!-- end konten dari profile sidebar -->
      </div>