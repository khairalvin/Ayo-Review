@php
    use App\Follow;
@endphp
<!-- top nav tab -->
<div class="container-fluid top-nav-tab " data-toggle="sticky-onscroll">
    <div class="container">
        <ul class="navbar-nav flex-row justify-content-sm-center">
        <li>
            <a href="/profile/{{$user->username}}" @if($jenis == "profile_review")class="active"@endif>REVIEW
            <span>120</span>
            </a>
        </li>
        <li>
            <a href="/profile_media/{{$user->username}}" @if($jenis == "profile_media")class="active"@endif>MEDIA</a>
        </li>
        <li>
            <a href="/profile_follower/{{$user->username}}" @if($jenis == "profile_followers")class="active"@endif>FOLLOWER
            <span>{{Follow::countFollower($user->id)}}</span>
            </a>
        </li>
        <li>
            <a href="/profile_following/{{$user->username}}" @if($jenis == "profile_following")class="active"@endif>FOLLOWING
            <span>{{Follow::countFollowing($user->id)}}</span>
            </a>
        </li>
        </ul>
    </div>
</div>
<!-- end top nav tab -->