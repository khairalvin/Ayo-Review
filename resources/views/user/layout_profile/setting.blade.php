<div class=" mr-review-header bg-sidebar-profile col-lg-3 px-0 pt-0 mb-2">
    <!-- header dari profile sidebar -->
    <div class="col-lg-12 sp-inside-kat sp-header mb-0">
        <div class="col-lg-12 p-kat">
        <p><i class="fa fa-cog" aria-hidden="true"></i>SETTINGS
            
        </div>
    </div> <!-- end dari header profile sidebar -->
    <!-- konten dari profile sidebar -->
    <div class="col-lg-12 profile-sidebar" >
        <ul class="nav flex-column mt-3 nav-pills">
        <li class="nav-item">
            <a class="nav-link " href="/profile_edit">Profile</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="/profile_pass">Ganti Password</a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link " href="profile_privasi.html">Privasi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="profile_delete.html">Danger Zone</a>
        </li> --}}
        </ul>
    </div><!-- end konten dari profile sidebar -->
</div> {{--  mr-review-header bg-sidebar-profile col-lg-3 px-0 pt-0 mb-2  --}}