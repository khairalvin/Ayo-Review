@extends('user.layout_main.master')
@section('content')
    @include('user.layout_profile.head_profile')
    @include('user.layout_profile.top_nav')
    <main role="main" class="container mb-4">
        <div class="row">
            @if($jenis == "edit")
                @include('user.layout_profile.setting')
            @else
                @include('user.layout_profile.aboutme')
            @endif
            @yield('content_profile')
        </div>
    </main>
@endsection 