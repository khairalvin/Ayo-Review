<?php
use App\Follow;
use App\User;
?>
<!-- Profile User -->
<div class="container-fluid top-pilihan-kat bg-kat-user ">
    <div class="container">
        <div class="row text-center">
        <div class="col-sm-12 p-0 m-0">
            <a href="">
            @if($user->img <> NULL || $user->img <> '')
                @if($user->provider <> NULL || $user->provider <> '')
                    <div class="profile-user" style="background-image:url('{{$user->img}}')"></div>
                @else
                    <div class="profile-user" style="background-image:url('{{asset('user/images/user_profile/'.$user->img.'')}}')"></div>
                @endif
            @else
                @if($user->gender == "L")
                    <div class="profile-user" style="background-image:url('{{asset('user/images/ava_default/merah0ranger.png')}}')"></div>
                @elseif($user->gender == "P")
                    <div class="profile-user" style="background-image:url('{{asset('user/images/ava_default/pink0ranger.png')}}')"></div>
                @endif
            @endif 
            </a>
            <h2 class="d-block">{{$user->username}}</h2>

            <input type="hidden" name="idTarget" id="idTarget" value="{{$user->id}}">
            @if($user->hasRole('moderator'))
                <p>Moderator</p>
            @elseif($user->hasRole('admin'))
                <p>Admin</p>
            @elseif($user->hasRole('regular'))
                <p>Regular</p>
            @endif
            @isset(Auth::user()->id)
                @if($user->id == Auth::user()->id)
                @else()
                    <input type="hidden" name="check" id="check" value="{{Follow::check_follow(Auth::user()->id,$user->id)}}">
                    <input type="hidden" name="idUser" id="idUser" value="{{Auth::user()->id}}">
                    <button class="follow btn btn-outline-pilihan" id="follow" data-following="false">
                            {{ csrf_field() }}
                        <span class="msg-follow">FOLLOW </span>
                        <span class="msg-following">
                            <i class="fa fa-star" aria-hidden="true"></i>&nbsp;&nbsp;FOLLOWED</span>
                        <span class="msg-unfollow">
                            <i class="fa fa-star-o" aria-hidden="true"></i>&nbsp;&nbsp;UNFOLLOW</span>
                    </button>
                @endif
            @else
                <input type="hidden" name="idUser" id="idUser" value="0">
                <input type="hidden" name="check" id="check" value="{{Follow::check_follow(0,$user->id)}}">
            @endisset
        </div>
        <!-- end col-sm-11 p-0 m-0 -->
        </div>
    </div>
</div>
<!-- end Profile User -->
@section('custom_js')
<script>

    document.addEventListener('DOMContentLoaded', displayFollows());

    function displayFollows(){
        const idUser = document.getElementById('idUser').value;
        const idTarget = document.getElementById('idTarget').value;
        let check = document.getElementById('check').value;
        if(check == true){
            $("#follow").addClass('following');
            $("#follow").attr('data-following', 'true');
        }
    }

    function setFollowing(idUser,idTarget,_token){
        $.ajax({
            type:'POST',
            url:"/follow",
            data:{
                '_token' :_token,
                'id_user':idUser,
                'id_user_follow':idTarget
            },
            success:function(data){
                $("#follow").addClass('following');
                $("#follow").attr('data-following', 'true');
            }
        });
    }

    function setUnfollowing(idUser,idTarget,_token) {
        $.ajax({
            type:'POST',
            url:"/unfollow",
            data:{
                '_token' :_token,
                'id_user':idUser,
                'id_user_follow':idTarget
            },
            success:function(data){
                $("#follow").attr('data-following', 'false');
                $("#follow").removeClass('following');
            }
        });
    }

    $("#follow").click(function (e) {
        const idUser = document.getElementById('idUser').value;
        const idTarget = document.getElementById('idTarget').value;
        let _token = $('input[name="_token"]').val();
        if ($(this).attr('data-following') === 'false') {
            // Create Object untuk di push ke storage
            setFollowing(idUser,idTarget,_token);
        }else if ($(this).attr('data-following') === 'true') {
            // Create object untuk di unFollow
            setUnfollowing(idUser,idTarget,_token);
        }
        e.preventDefault();
    });
</script>
@endsection