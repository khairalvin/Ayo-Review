<?php
use App\Kategori;
use App\User;
use Carbon\Carbon;
?>
<div class="container newsfull-container">
<!-- Advertisement top -->
    <div class="row justify-content-center py-3 ">
        <img src="{{asset('user/images/headerAds.jpg') }}">
    </div>
    <!-- Main Title -->
    <div class="row mx-0">
        <h1 class="main-title single-title entry-title">{{$news->judul}}</h1>
        <div class="info-block  col-12">
            <div class="row clearfix m-0">
                <div class="tag-news">{{Kategori::getName($news->kategori_id)}}</div>
                <div class="auth">{{User::find($news->user_id)->first()->username}}
                <span class="footage">/ {{ Carbon::parse($news->created_at)->format('d M Y')}}</span>
                </div>
                <div class="socialmedia">
                <a href="" class="btn btn-outline-facebook-theme">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="" class="btn btn-outline-twitter-theme">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="" class="btn btn-outline-googlep-theme">
                    <i class="fa fa-google" aria-hidden="true"></i>
                </a>
                <a href="" class="btn btn-outline-pinterest-theme">
                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                </a>
                <a href="" class="btn btn-outline-linkshare-theme">
                    <i class="fa fa-link" aria-hidden="true"></i>
                </a>
                </div>
            </div>
        </div>
    </div>
    <!-- main title -->
    <!-- Isi berita terkini -->
    <div class="row mt-4 ">
        @include('user.berita.detail.isi_berita')
        @include('user.berita.detail.rekomendasi_berita')
    </div>
    <!-- end berita terkini -->
</div>
<!-- End container -->