<!-- kolom Konten berita utama -->
<div class="col-sm-12 col-lg-9  pr-2">
    <div class="col-lg-12 sp-news-single-page">
        <!-- isi Konten Berita -->
        <img src="{{asset('admin/img/berita_thumb/'.$news->img.'')}}" style="width:100%">
        <?php echo $news->konten; ?>

        <div class="tag-news-footer row">
            <div class=" tag-oval news">
                <a href="">News</a>
            </div>
            <div class="tag-oval news">
                <a href="">Smartphone</a>
            </div>
            <div class="tag-oval news">
                <a href="">Samsung</a>
            </div>
            <div class="tag-oval galaxy-9">
                <a href="">Galaxy S9</a>
            </div>
        </div>
        @include('user.berita.detail.berita_selanjutnya')
        @include('user.berita.detail.komentar')
    </div>
    <!-- end isi konten berita -->
</div>
<!-- end Konten berita utama -->