@php 
use App\Kategori; 
use Carbon\Carbon;
use App\User;
@endphp
<div class="next-news-panel">
    <h2>Berita Selanjutnya</h2>
    <!-- row rekomendasi berita -->
    <div class="row">
        @foreach($nextNews as $next)
        <div class="col-4">
            <a href="/news/detail/{{$next->id}}"> <div class="recomendation" style="background-image:url('{{asset('admin/img/berita_thumb/'.$next->img.'')}}');">
                <div class="lil-tag">{{Kategori::getName($next->kategori_id)}}</div>
                <div class="title">{{$next->judul}}</div>
                <div class="footage">{{ Carbon::parse($next->created_at)->format('d M Y')}} &nbsp; | &nbsp;
                <span class="auth">{{User::find($next->user_id)->first()->username}}</span>
                </div>
                <div class="overlay"></div>
            </div></a>
        </div>
        @endforeach
    </div>
    <!-- End row rekomendasi berita -->
    <div class="comment-section">
        <!-- button showcomment -->
        <div class="btn btn-med-dark" id="showcomment"> Tinggalkan komentar</div>
        <!-- end button showcomment -->
    </div>
</div>