@php 
use App\Kategori; 
@endphp
<!-- kolom Side bar berita -->
<div class="col-sm-12 col-lg-3 pl-0 pt-3">
<!-- Google Andsense -->

    <!-- end Google Andsense -->
    <div class="col-lg-12 pr-2 sp-header p-0">
        <div class="sp-inside">
        <h1>Rekomendasi
            <span>Berita</span>
        </h1>
        </div>
    </div>
    <div class="col-lg-12  side-news">
        <ul>
            @foreach($recommendNews as $rekomen)
                <li>
                    <a href="/news/detail/{{$rekomen->id}}">
                    <div class="tag-lil">{{Kategori::getName($rekomen->kategori_id)}}</div>
                    <img src="{{asset('admin/img/berita_thumb/'.$rekomen->img.'')}}" alt="">
                    <div class="text-title">{{$rekomen->judul}}</div>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- End Side bar -->