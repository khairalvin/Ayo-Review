@php 
use App\Kategori; 
use Carbon\Carbon;
use App\User;
@endphp
<!-- Advertisement top -->
<div class="row justify-content-center py-3">
  <img src="{{asset('user/images/HeaderAds.jpg') }}">
</div>
<!-- end adsense top -->
<div class="row Slider-news mx-0 mt-3">
  <!-- big courosel -->
  <div class="col-md-6 pl-0 pr-2">
    <a href="/news/detail/{{$news_top[0]->id}}">
    <div class="big-metro-news" style="background-image:url('{{asset('admin/img/berita_thumb/'.$news_top[0]->img.'')}}');">
        <div class="big-tag">{{Kategori::getName($news_top[0]->kategori_id)}}</div>
        <div class="news-title">{{$news_top[0]->judul}}</div>
        <div class="news-footage">
            {{ Carbon::parse($news_top[0]->created_at)->format('d M Y')}} &nbsp;|&nbsp; {{User::find($news_top[0]->user_id)->first()->username}}
        </div>
        <div class="overlay"></div>
      </div>
    </a>
  </div>
  <!-- end big courosel -->
  <div class="col-md-6  px-0">
    <div class="row m-0">
      <div class="col-md-12 px-0">
        <a href="/news/detail/{{$news_top[1]->id}}">
        <div class="metro-news" style="background-image:url('{{asset('admin/img/berita_thumb/'.$news_top[1]->img.'')}}');">
          <div class="lil-tag">{{Kategori::getName($news_top[1]->kategori_id)}}</div>
          <div class="news-title">{{$news_top[1]->judul}}</div>
          <div class="news-footage">
              {{ Carbon::parse($news_top[1]->created_at)->format('d M Y')}} &nbsp;|&nbsp; {{User::find($news_top[1]->user_id)->first()->username}}
          </div>
          <div class="overlay"></div>
        </div>
        </a>
      </div>

    </div>
    <div class="row m-0 mt-2">
      <div class="col-md-6 pl-0 pr-1">
        <a href="/news/detail/{{$news_top[2]->id}}">
        <div class="metro-news" style="background-image:url('{{asset('admin/img/berita_thumb/'.$news_top[2]->img.'')}}');">
          <div class="lil-tag">{{Kategori::getName($news_top[2]->kategori_id)}}</div>
          <div class="news-title">{{$news_top[2]->judul}}</div>
          <div class="news-footage">
              {{ Carbon::parse($news_top[2]->created_at)->format('d M Y')}} &nbsp;|&nbsp; {{User::find($news_top[2]->user_id)->first()->username}}
          </div>
          <div class="overlay"></div>
        </div>
        </a>
      </div>
      <div class="col-md-6 pl-1 pr-0">
        <a href="/news/detail/{{$news_top[3]->id}}">
        <div class="metro-news" style="background-image:url('{{asset('admin/img/berita_thumb/'.$news_top[3]->img.'')}}');">
          <div class="lil-tag">{{Kategori::getName($news_top[3]->kategori_id)}}</div>
          <div class="news-title">{{$news_top[3]->judul}}</div>
          <div class="news-footage">
              {{ Carbon::parse($news_top[3]->created_at)->format('d M Y')}} &nbsp;|&nbsp; {{User::find($news_top[3]->user_id)->first()->username}}
          </div>
          <div class="overlay"></div>
        </div>
        </a>
      </div>
    </div>
  </div>
</div>
<!-- slider news top -->