@php 
use App\Kategori; 
use Carbon\Carbon;
use App\User;
@endphp
<!-- Header berita terkini -->
<div class="row mt-3 m-0">
    <div class="col-sm-12 col-lg-9 pl-0 pr-4">
        <div class="col-lg-12 pr-2 sp-header p-0">
        <div class="sp-inside">
            <h1>Berita
            <span>Terbaru</span>
            </h1>
        </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-3 p-0">
        <div class="col-lg-12 pr-2 sp-header p-0">
            <div class="sp-inside">
            </div>
        </div>
    </div>
</div>
<!-- end end berita terkini -->
<!-- Isi berita terkini -->
<div class="row m-0">
    <div class="col-sm-12 col-lg-9 pl-0 pr-3">
        <div class="col-lg-12 sp-news-new py-2 pl-0">
            <ul>
                <li>
                    <div class="wrapper">
                        <div class="tag-lil-news">Jam Tangan</div>
                        <a href="/news/detail/{{$news_latest[0]->id}}">
                            <img src="{{asset('admin/img/berita_thumb/'.$news_latest[0]->img.'')}}">
                        </a>    
                        <a href="/news/detail/{{$news_latest[0]->id}}" class="pl-3">
                            <div class="text-title">{{$news_latest[0]->judul}}</div> 
                            <p class="read-more">
                                {{strip_tags(substr($news_latest[0]->konten,0,150))}} . . .
                            </p>
                        </a>
                        <div class="text-footer pl-3">{{ Carbon::parse($news_latest[0]->created_at)->format('d M Y')}} &nbsp;|&nbsp;
                        <span>{{User::find($news_latest[0]->user_id)->first()->username}}</span> 
                        <span class="pull-right mr-3"><a href="/news/detail/{{$news_latest[0]->id}}">Read More </a></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="wrapper">
                        <div class="tag-lil-news">Jam Tangan</div>
                        <a href="/news/detail/{{$news_latest[1]->id}}">
                            <img src="{{asset('admin/img/berita_thumb/'.$news_latest[1]->img.'')}}">
                        </a>    
                        <a href="/news/detail/{{$news_latest[1]->id}}" class="pl-3">
                            <div class="text-title">{{$news_latest[1]->judul}}</div> 
                            <p class="read-more">
                                {{strip_tags(substr($news_latest[1]->konten,0,150))}} . . .
                            </p>
                        </a>
                        <div class="text-footer pl-3">
                            {{ Carbon::parse($news_latest[1]->created_at)->format('d M Y')}} &nbsp;|&nbsp;
                            <span>{{User::find($news_latest[1]->user_id)->first()->username}}</span> 
                            <span class="pull-right mr-3"><a href="/news/detail/{{$news_latest[1]->id}}">Read More </a></span>
                        </div>
                    </div>
                </li>
                <!-- konten news dua gambar besar -->
                <li>
                    <div class="row big-news">
                        <div class="col-lg-6 col-md-12 pb-md-4">
                        <a href="/news/detail/{{$news_latest[1]->id}}">
                            <div class="big-news-wrapper" style="background-image:url('{{asset('admin/img/berita_thumb/'.$news_latest[2]->img.'')}}');">
                                <div class="tag-lil">{{Kategori::getName($news_latest[2]->kategori_id)}}</div>
                                <div class="overlay"></div>
                                <div class="news-title">
                                    {{$news_latest[2]->judul}}
                                </div>
                                <div class="news-footage">
                                        {{ Carbon::parse($news_latest[2]->created_at)->format('d M Y')}} &nbsp; | &nbsp; {{User::find($news_latest[2]->user_id)->first()->username}}
                                </div>
                            </div>
                        </a>
                        </div>
                        <div class="col-lg-6 col-md-12">
                        <a href="/news/detail/{{$news_latest[1]->id}}">
                            <div class="big-news-wrapper" style="background-image:url('{{asset('admin/img/berita_thumb/'.$news_latest[3]->img.'')}}');">
                                <div class="tag-lil">{{Kategori::getName($news_latest[3]->kategori_id)}}</div>
                                <div class="overlay"></div>
                                <div class="news-title">
                                    {{$news_latest[3]->judul}}
                                </div>
                                <div class="news-footage">
                                        {{ Carbon::parse($news_latest[3]->created_at)->format('d M Y')}} &nbsp; | &nbsp; {{User::find($news_latest[3]->user_id)->first()->username}}
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>
                </li>
                <!-- End konten news dua gambar besar -->
                <li>
                    <div class="wrapper">
                        <div class="tag-lil-news">Jam Tangan</div>
                        <a href="/news/detail/{{$news_latest[4]->id}}">
                            <img src="{{asset('admin/img/berita_thumb/'.$news_latest[4]->img.'')}}">
                        </a>    
                        <a href="/news/detail/{{$news_latest[4]->id}}" class="pl-3">
                            <div class="text-title">{{$news_latest[4]->judul}}</div> 
                            <p class="read-more">
                                {{strip_tags(substr($news_latest[4]->konten,0,150))}} . . .
                            </p>
                        </a>
                        <div class="text-footer pl-3">
                            {{ Carbon::parse($news_latest[4]->created_at)->format('d M Y')}} &nbsp;|&nbsp;
                            <span>{{User::find($news_latest[4]->user_id)->first()->username}}</span> 
                            <span class="pull-right mr-3"><a href="/news/detail/{{$news_latest[4]->id}}">Read More </a></span>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="wrapper">
                        <div class="tag-lil-news">Jam Tangan</div>
                        <a href="/news/detail/{{$news_latest[5]->id}}">
                            <img src="{{asset('admin/img/berita_thumb/'.$news_latest[5]->img.'')}}">
                        </a>    
                        <a href="/news/detail/{{$news_latest[5]->id}}" class="pl-3">
                            <div class="text-title">{{$news_latest[5]->judul}}</div> 
                            <p class="read-more">
                                {{strip_tags(substr($news_latest[5]->konten,0,150))}} . . .
                            </p>
                        </a>
                        <div class="text-footer pl-3">
                            {{ Carbon::parse($news_latest[5]->created_at)->format('d M Y')}} &nbsp;|&nbsp;
                            <span>{{User::find($news_latest[5]->user_id)->first()->username}}</span> 
                            <span class="pull-right mr-3"><a href="/news/detail/{{$news_latest[5]->id}}">Read More </a></span>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="wrapper">
                        <div class="tag-lil-news">Jam Tangan</div>
                        <a href="/news/detail/{{$news_latest[6]->id}}">
                            <img src="{{asset('admin/img/berita_thumb/'.$news_latest[6]->img.'')}}">
                        </a>    
                        <a href="/news/detail/{{$news_latest[6]->id}}" class="pl-3">
                            <div class="text-title">{{$news_latest[6]->judul}}</div> 
                            <p class="read-more">
                                {{strip_tags(substr($news_latest[6]->konten,0,150))}} . . .
                            </p>
                        </a>
                        <div class="text-footer pl-3">
                            {{ Carbon::parse($news_latest[6]->created_at)->format('d M Y')}} &nbsp;|&nbsp;
                            <span>{{User::find($news_latest[6]->user_id)->first()->username}}</span> 
                            <span class="pull-right mr-3"><a href="/news/detail/{{$news_latest[6]->id}}">Read More </a></span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- sidebar -->
    @include('user.berita.sidebar_berita')
    <!-- end sidebar -->
</div>
<!-- end berita terkini -->