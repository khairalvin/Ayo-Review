@extends('user.layout_main.master')
@section('content')
  <div class="container news-container">
    @include('user.berita.berita_top')
    @include('user.berita.berita_terpopuler')
    @include('user.berita.iklan')
    @include('user.berita.berita_terbaru')
  </div>
@endsection
@section('custom_js')
@endsection