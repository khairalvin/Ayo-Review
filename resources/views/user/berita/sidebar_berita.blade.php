@php 
use App\Kategori; 
@endphp
<div class="col-sm-12 col-lg-3 p-0">
    <div class="col-lg-12 p-0 mb-4">
        <div class="giveaway-sidebar" style="background-image:url(https://cdn57.androidauthority.net/wp-content/uploads/2018/02/NUU-Mobile-G3-new-AA-300x200.jpg)">
            <div class="overlay"></div>
            <div class="giveaway-title">
            Nanti Disini ditaro Google ads
            </div>
            <div class="giveaway-footage">
            1 day ago
            </div>
        </div>
    </div>

    <div class="col-lg-12 pr-2 sp-header p-0">
        <div class="sp-inside">
            <h1>Rekomendasi
            <span>Berita</span>
            </h1>
        </div>
    </div>
    <div class="col-lg-12 side-news">
            <ul>
                @foreach($recommendNews as $rekomen)
                    <li>
                        <a href="/news/detail/{{$rekomen->id}}">
                        <div class="tag-lil">{{Kategori::getName($rekomen->kategori_id)}}</div>
                        <img src="{{asset('admin/img/berita_thumb/'.$rekomen->img.'')}}" alt="">
                        <div class="text-title">{{$rekomen->judul}}</div>
                        </a>
                    </li>
                @endforeach
            </ul>
    </div>
    {{-- <div class="col-lg-12 pr-2 sp-header p-0">
    <div class="sp-inside">
        <h1>Giveaway
        <span>Minggu ini</span>
        </h1>
    </div>
    </div>
    <div class="col-lg-12 p-0">
        <div class="giveaway-sidebar" style="background-image:url(https://cdn57.androidauthority.net/wp-content/uploads/2018/02/NUU-Mobile-G3-new-AA-300x200.jpg)">
            <div class="overlay"></div>
            <div class="giveaway-title">
            NUU Mobile G3 International Giveaway !
            </div>
            <div class="giveaway-footage">
            1 day ago
            </div>
        </div>
    </div> --}}
</div>