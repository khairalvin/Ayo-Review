@php 
use App\Kategori; 
use Carbon\Carbon;
use App\User;
@endphp
<!-- header news top -->
<div class="row sp-header mt-4 mx-0">
    <div class="col-lg-12 sp-inside">
        <h1>Berita
        <span>Terpopuler</span>
        </h1>
        {{-- <ul class="navbar-nav flex-row">
            <li class="nav-item ">
                <a class="nav-link sel-berita" href="#">SEMUA</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link " href="#">AUDIOPHILE</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link " href="#">JAM TANGAN</a>
            </li> --}}
        </ul>
    </div>
</div>
    <!-- end of  header popular news -->
    <!-- Popular News -->
<div class="row pop-news">
    @foreach($news_populer as $populer)
    <div class="col-6 col-xl-3">
        <a href="">
        <figure class="figure">
            <div class="tag-lil-news">{{Kategori::getName($populer->kategori_id)}}</div>
            <a href="/news/detail/{{$populer->id}}"><img src="{{asset('admin/img/berita_thumb/'.$populer->img.'')}}" class="figure-img img-thumbnail"> 
            <figcaption class="figure-caption">{{$populer->judul}}</figcaption></a>
            <div class="figfooter">{{ Carbon::parse($populer->created_at)->format('d M Y')}} &nbsp; | &nbsp;
            <span class="auth">{{User::find($populer->user_id)->first()->username}}</span>
            </div>
        </figure>
        </a>
    </div>
    @endforeach
</div>
<!-- End Popular News -->