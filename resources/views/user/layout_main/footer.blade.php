<!-- Footer Section -->
<footer>
    <div class='container  '>
        <div class='row '>
        <div class='col-md-12 col-lg-12 m-0 p-0'>
            <ul class="m-0 p-0">
            <li>
                <a href="/aboutus">Tentang Kami</a>
            </li>
            <li>
                <a href="/syarat-ketentuan">Syarat dan Ketentuan</a>
            </li>
            <li>
                <a href="/kebijakan-privasi">Kebijakan Privasi</a>
            </li>
            <!-- <li>
                <a href='#'>Bantuan</a>
            </li> -->
            <p class="pull-right">
                <a href="" class="aa_soc btn-googlep-theme ">
                <i class="fa fa-google-plus " aria-hidden="true"></i>
                </a>
                </i>
            </p>
            <p class="pull-right">
                <a href="" class="aa_soc btn-youtube-theme">
                <i class="fa fa-youtube " aria-hidden="true"></i>
                </a>
                </i>
            </p>
            <p class="pull-right">
                <a href="" class="aa_soc btn-instagram-theme">
                <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
                </i>
            </p>
            <p class="pull-right">
                <a href="" class="aa_soc  btn-twitter-theme">
                <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                </i>
                </i>
            </p>
            <p class="pull-right">
                <a href="" class="aa_soc  btn-facebook-theme">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                </i>
            </p>
            </ul>
        </div>
        <div class="reserved">© 2017 - 2018 Ayoreview All Rights Reserved</div>
        </div>
    </div>
</footer>
<!-- End Footer -->