<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="shortcut icon" type="image/x-icon" href="{{asset('user/images/favicon.ico') }}">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,800">
      <link rel="stylesheet" href="{{asset('user/css/font-awesome.min.css') }}">
      <link rel="stylesheet" href="{{asset('user/css/bootstrap.css') }}">
      <link rel="stylesheet" href="{{asset('user/css/styles.css') }}">
      <link rel="stylesheet" href="{{asset('user/css/gijgo.min.css') }}">
      @yield('custom_css')
      <script src="{{asset('user/js/modernizr.custom.js') }}"></script>
      <title>Ayoreview</title>
</head>