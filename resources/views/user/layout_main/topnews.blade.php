<?php 
use App\Berita;
?>
<!-- top slider news -->
<div class="container-fluid top-news">
    <div class="row">
        <div class="col-sm-3 top-news-title">
        TOP NEWS
        </div>
        <div class="col-sm-9 top-news-content">
            @php $top_news = Berita::get_topnews() @endphp
            <div>
                <a href="/news/detail/{{$top_news[0]->id}}">{{$top_news[0]->judul}}</a>
                <a href="/news/detail/{{$top_news[1]->id}}">{{$top_news[1]->judul}}</a>
            </div>
            <div>
                <a href="/news/detail/{{$top_news[2]->id}}">{{$top_news[2]->judul}}</a>
                <a href="/news/detail/{{$top_news[3]->id}}">{{$top_news[3]->judul}}</a>
            </div>
        </div>
    </div>
</div>
<!-- end top slider news -->