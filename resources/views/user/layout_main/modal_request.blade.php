<?php
use App\Kategori;
$getKategori = Kategori::all();
?>
<!-- modal request item -->
<div class="modal fade" id="reqmodal" tabindex="-1" role="dialog" aria-labelledby="reqmodal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="reqmodalLabel">Request Produk Untuk di Review</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form enctype="multipart/form-data" action="/sendRequest" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="form-group">
                    <label for="kategori">Kategori produk:</label>
                    <select class="form-control" id="kategori" name="kategori">
                        @foreach($getKategori as $kategori)
                            <option value="{{$kategori->id}}">{{$kategori->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Nama Item:</label>
                    <textarea class="form-control" id="item" name="item"></textarea>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Kirim Request</button>
            </div>
        </form>
        </div>
    </div>
</div><!-- end modal request item -->