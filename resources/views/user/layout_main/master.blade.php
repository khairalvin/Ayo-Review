<!DOCTYPE html>
<html lang="en">
    @include('user.layout_main.head')
    <body onload="loaded()">
      <div id="loader"></div>
      <div id="kontent">
          @include('user.layout_main.modal_request')
          @include('user.layout_main.modal_saran')
          @include('user.layout_main.modal_myforget')
          @include('user.layout_main.modal_regis')
          @include('user.layout_main.navbar')
          @include('user.layout_main.topnews')
          @yield('content')
          @include('user.layout_main.footer')
      </div>
          @include('user.layout_main.js')
    </body>
</html>




