<!-- The Modal Lupa Password -->
<div class="modal fade" id="myForget">
    <div class="modal-dialog">
        <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
            
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
            <form role="form" action="{{ route('user-password-request') }}" method="post" class="registration-form">
                {{ csrf_field() }}
                <center><h4  style=" color:#3d3d3d">Atur Ulang Password Kamu</h4>
                <p style="font-size: 13px; color:#8d8d8d">Masukkan email untuk mencari akun anda</p>
            </center>
            
            <div class="form-group">
                <label class="sr-only" for="emaile">Email</label>
                <input type="text" name="email" placeholder="Email" class="email form-control" id="email">
            </div>
            <button type="submit" class="btn btn-block btn-primary py-modal">Selanjutnya</button>
            </form>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
            
        </div>
        </div>
    </div>
</div>
<!-- End Modals Password -->