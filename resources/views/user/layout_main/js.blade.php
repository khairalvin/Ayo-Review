<script src="{{asset('user/js/jquery.min.js') }}"></script>
<script src="{{asset('user/js/popper.min.js') }}"></script>
<script src="{{asset('user/js/bootstrap.min.js') }}"></script>
<script src="{{asset('user/js/sticky.js') }}"></script>
<script src="{{asset('user/js/masonry.pkgd.min.js') }}"></script>
<script src="{{asset('user/js/imagesloaded.js') }}"></script>
<script src="{{asset('user/js/classie.js') }}"></script>
<script src="{{asset('user/js/AnimOnScroll.js') }}"></script>
<script src="{{asset('user/js/select2.js') }}"></script>
<script src="{{asset('user/js/slick.js') }}"></script>
<script src="{{asset('user/js/ayoreview.js') }}"></script>
<script src="{{asset('user/js/gijgo.min.js') }}"></script>
<script src="{{asset('user/js/ResizeSensor.js') }}"></script>
@yield('custom_js')
<script>
  (function () {
    var morphSearch = document.getElementById('morphsearch'),
      input = morphSearch.querySelector('input.morphsearch-input'),
      ctrlClose = morphSearch.querySelector('span.morphsearch-close'),
      navbar = document.querySelector('.navbar');
      isOpen = isAnimating = false,

      // show/hide search area
    toggleSearch = function (evt) {
      // return if open and the input gets focused
      if (evt.type.toLowerCase() === 'focus' && isOpen) return false;
      var offsets = morphsearch.getBoundingClientRect();
      if (isOpen) {
        navbar.classList.remove("nav-full");
        classie.remove(morphSearch, 'open');
        // trick to hide input text once the search overlay closes 
        // todo: hardcoded times, should be done after transition ends
        if (input.value !== '') {
          setTimeout(function () {
            classie.add(morphSearch, 'hideInput');
            setTimeout(function () {
              classie.remove(morphSearch, 'hideInput');
              input.value = '';
            }, 300);
          }, 500);
        }
        input.blur();
      } else {
        classie.add(morphSearch, 'open');
        setTimeout(() => {
          navbar.classList.add('nav-full');
        }, 400);
      }
      isOpen = !isOpen;
    };
    input.addEventListener('focus', toggleSearch);
    ctrlClose.addEventListener('click', toggleSearch);
    
    // esc key closes search overlay
    // keyboard navigation events
    document.addEventListener('keydown', function (ev) {
      var keyCode = ev.keyCode || ev.which;
      if (keyCode === 27 && isOpen) {
        toggleSearch(ev);
      }
    });
    /***** for demo purposes only: don't allow to submit the form *****/
    morphSearch.querySelector('button[type="submit"]').addEventListener('click', function (ev) {
      ev.preventDefault();
    });
  })();
</script>

{{-- SEARCHING --}}

<script>
    function search(id){
        var _token = $('input[name="_token"]').val();
        if(id == ''){
        $('#form-result-berita').empty();
        $('#form-result-akun').empty();
        $('#form-result-review').empty();
        }else{
        $.ajax({
            type:'POST',
            url:"/search",
            data:{
                '_token' : _token,
                'param':id
            },
            success:function(data){
                // console.log(data);
                parsingdata(data);
            }
        });
        }
    }

    function parsingdata(data){
      var jsonObj = $.parseJSON(data);
      var htmlakun = '';
      var htmlberita = '';
      // console.log(jsonObj.akun);
      $.each(jsonObj.akun,function(key,value){
        htmlakun += '<a class="dummy-media-object" href="/profile/' + value.username + '">';
        if(value.img == ""){
          if(value.gender == "L"){
            var image = "{{asset('user/images/ava_default/merah0ranger.png')}}";
          }else if(value.gender == "P"){
            var image = "{{asset('user/images/ava_default/pink0ranger.png')}}";
          }
        }else{
          var image = "{{asset('user/images/user_profile/')}}/"+value.img;
        }
        htmlakun += '<img class="round" src="'+image+'" alt="Sara Soueidan"/>';
        htmlakun += '<h3>' + value.username + '</h3>';
        htmlakun += '</a>';
        $('#form-result-akun').empty();
        $('#form-result-akun').append(htmlakun);
      });
      $.each(jsonObj.berita,function(key,value){
        htmlberita += '<a class="dummy-media-object" href="/news/detail/' + value.id + '">';
        var imageberita = "{{asset('admin/img/berita_thumb/')}}/"+value.img;
        htmlberita += '<img src="'+imageberita+'" alt="Sara Soueidan"/>';
        htmlberita += '<h3>' + value.judul + '</h3>';
        htmlberita += '</a>';
        $('#form-result-berita').empty();
        $('#form-result-berita').append(htmlberita);
      });
    }
</script>