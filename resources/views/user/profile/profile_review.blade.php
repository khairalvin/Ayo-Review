@extends('user.layout_profile.master')
@section('content_profile')
          <!-- Review Audiophile -->
          <div class=" mr-review-header col-lg-9 pl-2 pr-0">
  
            <!-- konten dari profile sidebar -->
            <div class="col-lg-12 p-0">
              <!--- Review pengguna -->
              <div id="review">
                <div class="col-lg-12 pt-0 px-0">
                  <div class="row main-rev">
  
                    <div class="col-lg-12 ">
                      <ul>
                          <li>
                            <div class="card ">
                              <div class="card-header">
                                <div class="row">
                                  <div class="col-sm-10 ">
                                    <span>
                                      <a href="">Husky Dusky</a> &nbsp;Membuat ulasan pada &nbsp;
                                      <a href="">Review : Fostex TH-X00 Premium Headphone</a>
                                    </span>
                                  </div>
                                  <div class="col-sm-2 text-right">
                                    <div class="dropdown">
                                      <i class="fa fa-ellipsis-v px-2" aria-hidden="true" type="button" data-toggle="dropdown"></i>
                                      <ul class="dropdown-menu">
                                        <li data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#buatreview">
                                          <a href="#" >
                                            <i class="fa fa-pencil"></i> Edit</a>
                                        </li>
                                        <div class="dropdown-divider"></div>
                                        <li>
                                          <a href="#">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body">
                                <div class="row">
                                  <!-- Lingkaran Rating -->
                                  <div class="col-lg-4 pr-0">
                                    <div class="circle-big cir-middle">
                                      <div class="progress blue">
                                        <span class="progress-left">
                                          <span class="progress-bar"></span>
                                        </span>
                                        <span class="progress-right">
                                          <span class="progress-bar"></span>
                                        </span>
                                        <div class="progress-value">7.0</div>
                                      </div>
                                    </div>
                                    <!-- End Lingkaran Rating -->
    
                                    <!-- Profile reviewer -->
                                    <div class="col-lg-12">
                                      <div class="media">
                                        <a class="pull-left" href="#">
                                          <img class="user" src="{{asset('user/images/ava_default/merah0ranger.png')}}">
                                        </a>
                                        <div class="media-body pl-2">
                                          <a href="" class="label-username">HUSKY DUSKY</a>
                                          <button class="follow btn btn-outline-pilihan ">
                                            <span class="msg-follow">Follow</span>
                                            <span class="msg-following">Following</span>
                                            <span class="msg-unfollow">Unfollow</span>
                                          </button>
                                          <br>
                                          <span class="label-superadmin">
                                            <i class="fa fa-star" aria-hidden="true"></i>&nbsp;&nbsp;Super Administrator</span>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- End Profile reviewer -->
    
                                    <div class="row py-4">
                                      <!-- Kriteria pertama -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Suara</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!--End Kriteria pertama -->
    
                                      <!-- Kriteria kedua -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Design</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- End kriteria kedua -->
    
                                      <!-- Kriteria ke tiga -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Durability</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- End kriteria ketiga -->
    
                                      <!-- Kriteria keempat -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Material</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- End kriteria keempat -->
    
                                      <!-- Kriteria kelima -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Kenyamanan</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- end kriteria kelima -->
                                    </div>
    
                                  </div>
                                  <div class="col-lg-8 pl-0">
                                    <a href="review_pengguna.html"></a>
                                    <div class="col-lg-12 pt-4 px-5 rev-content read-more">
                                      <h2>Aku terkejut dengan Kualitasnya</h2>
                                      <p>Saya adalah seorang musisi sekaligus penikmat orgasm, eh . . . Maksud saya penikmat eargasm.Pada
                                        review ini saya tidak terafiliasi dengan merk diatas dan juga tidak ada tekanan dari pihakmanapun
                                        dalam membuat review ini penikmat eargasm.Pada review ini saya tidak terafiliasi dengan merk
                                        diatas dan juga tidak ada tekanan dari pihakmanapun dalam membuat review ini. . </p>
                                      <h2>Kemasan dan design</h2>
                                      <p>Dari segi design suka banget karena bentuknya keren jarang </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card-footer text-muted">
                                <a href="" class="mr-3">
                                  <i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;10 Like</a>
                                <a href="" class="mr-3">
                                  <i class="fa fa-comment" aria-hidden="true"></i> &nbsp;25 Comment</a>
                                <a href="">
                                  <i class="fa fa-share" aria-hidden="true"></i>&nbsp;3 Share</a>
                                <div class=" float-right">
                                  <span>Januari 11, 2018</span>
                                </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="card ">
                              <div class="card-header">
                                <div class="row">
                                  <div class="col-sm-10 ">
                                    <span>
                                      <a href="">Husky Dusky</a> &nbsp;Membuat ulasan pada &nbsp;
                                      <a href="">Review : Fostex TH-X00 Premium Headphone</a>
                                    </span>
                                  </div>
                                  <div class="col-sm-2 text-right">
                                    <div class="dropdown">
                                      <i class="fa fa-ellipsis-v px-2" aria-hidden="true" type="button" data-toggle="dropdown"></i>
                                      <ul class="dropdown-menu">
                                        <li data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#buatreview">
                                          <a href="#" >
                                            <i class="fa fa-pencil"></i> Edit</a>
                                        </li>
                                        <div class="dropdown-divider"></div>
                                        <li>
                                          <a href="#">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body">
                                <div class="row">
                                  <!-- Lingkaran Rating -->
                                  <div class="col-lg-4 pr-0">
                                    <div class="circle-big cir-middle">
                                      <div class="progress blue">
                                        <span class="progress-left">
                                          <span class="progress-bar"></span>
                                        </span>
                                        <span class="progress-right">
                                          <span class="progress-bar"></span>
                                        </span>
                                        <div class="progress-value">7.0</div>
                                      </div>
                                    </div>
                                    <!-- End Lingkaran Rating -->
    
                                    <!-- Profile reviewer -->
                                    <div class="col-lg-12">
                                      <div class="media">
                                        <a class="pull-left" href="#">
                                          <img class="user" src="{{asset('user/images/ava_default/merah0ranger.png')}}">
                                        </a>
                                        <div class="media-body pl-2">
                                          <a href="" class="label-username">HUSKY DUSKY</a>
                                          <button class="follow btn btn-outline-pilihan ">
                                            <span class="msg-follow">Follow</span>
                                            <span class="msg-following">Following</span>
                                            <span class="msg-unfollow">Unfollow</span>
                                          </button>
                                          <br>
                                          <span class="label-superadmin">
                                            <i class="fa fa-star" aria-hidden="true"></i>&nbsp;&nbsp;Super Administrator</span>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- End Profile reviewer -->
    
                                    <div class="row py-4">
                                      <!-- Kriteria pertama -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Suara</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!--End Kriteria pertama -->
    
                                      <!-- Kriteria kedua -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Design</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- End kriteria kedua -->
    
                                      <!-- Kriteria ke tiga -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Durability</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- End kriteria ketiga -->
    
                                      <!-- Kriteria keempat -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Material</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- End kriteria keempat -->
    
                                      <!-- Kriteria kelima -->
                                      <div class="col-lg-5 pr-0">
                                        <span class="label-kreteria">Kenyamanan</span>
                                      </div>
                                      <div class="col-lg-7 p-progress">
                                        <div class="progress">
                                          <div class="progress-bar" style="width:70%">7.0</div>
                                        </div>
                                      </div>
                                      <!-- end kriteria kelima -->
                                    </div>
    
                                  </div>
                                  <div class="col-lg-8 pl-0">
                                    <a href="review_pengguna.html"></a>
                                    <div class="col-lg-12 pt-4 px-5 rev-content read-more">
                                      <h2>Aku terkejut dengan Kualitasnya</h2>
                                      <p>Saya adalah seorang musisi sekaligus penikmat orgasm, eh . . . Maksud saya penikmat eargasm.Pada
                                        review ini saya tidak terafiliasi dengan merk diatas dan juga tidak ada tekanan dari pihakmanapun
                                        dalam membuat review ini penikmat eargasm.Pada review ini saya tidak terafiliasi dengan merk
                                        diatas dan juga tidak ada tekanan dari pihakmanapun dalam membuat review ini. . </p>
                                      <h2>Kemasan dan design</h2>
                                      <p>Dari segi design suka banget karena bentuknya keren jarang </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card-footer text-muted">
                                <a href="" class="mr-3">
                                  <i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;10 Like</a>
                                <a href="" class="mr-3">
                                  <i class="fa fa-comment" aria-hidden="true"></i> &nbsp;25 Comment</a>
                                <a href="">
                                  <i class="fa fa-share" aria-hidden="true"></i>&nbsp;3 Share</a>
                                <div class=" float-right">
                                  <span>Januari 11, 2018</span>
                                </div>
                              </div>
                            </div>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
  
              </div>
              <!-- end Review Pengguna -->
            </div>
            <!-- end konten dari profile sidebar -->
          </div>
@endsection