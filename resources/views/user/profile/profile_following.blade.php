@extends('user.layout_profile.master')
@section('content_profile')
<?php
use App\Follow;
?>
<!-- trophy -->
<div class="mr-review-header bg-sidebar-profile  col-lg-9 pl-2 pr-0">
    <!-- konten dari profile sidebar -->
    <div class="col-lg-12 profile-sidebar">
        @foreach($following as $follow)
        <div class="media user-follower">
            @if($follow->img == NULL || $follow->img == '')
                @if($follow->gender == "L")
                    <img src="{{asset('user/images/ava_default/merah0ranger.png')}}" alt="User Avatar" class="media-object pull-left">
                @elseif($follow->gender == "P")
                    <img src="{{asset('user/images/ava_default/pink0ranger.png') }}" alt="User Avatar" class="media-object pull-left">
                @endif
            @else
                @if($follow->provider <> NULL || $follow->provider <> '')
                    <img src="{{$follow->img}}" alt="User Avatar" class="media-object pull-left">
                @else
                    <img src="{{asset('user/images/user_profile/'.$follow->img.'')}}" alt="User Avatar" class="media-object pull-left">
                @endif
            @endif
                <div class="media-body">
                <a href="/profile/{{$follow->username}}">{{$follow->username}}
                <br>
                <span class="text-muted username">{{$follow->biodata}}</span>
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <!-- end konten dari profile sidebar -->
</div>
@endsection