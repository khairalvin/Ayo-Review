@extends('user.layout_profile.master')
@section('content_profile')
    <!-- Isi dari media -->
    <div class="mr-review-header bg-sidebar-profile  col-lg-9 pl-2 pr-0">
      <div class="col-lg-12 sp-inside-kat sp-header mb-0">
        <div class="col-lg-12 p-kat">
          <p>Ubah Password</p>
        </div>
      </div> <!-- end dari header profile sidebar -->
      <!-- konten dari profile sidebar -->
      <div class="col-lg-12 profile-sidebar" >
        <div class="col-md-12 pt-3 personal-info">
          <div class="alert alert-primary alert-dismissable">
              <a class="panel-close close" data-dismiss="alert">×</a>
              @if (Session::has('success'))
                  <i class="fa fa-coffee"></i>
                  {{ Session::get('success') }}
              @else
                  <i class="fa fa-coffee"></i>
                 Silahkan menggunakan akses ubah password akun anda.
              @endif
          </div>
          <div class="row pt-2 justify-content-center ">
            <div class="col-md-7 col-lg-7">
              <form enctype="multipart/form-data" action="/change_password" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                  <label class="control-label">Password Lama:</label>
                  <input class="form-control" type="password" name="current_password" placeholder="Masukan Password Lama">
                </div>
                <hr >
                <div class="form-group">
                  <label class="control-label">Password baru :</label>
                  <input class="form-control" type="password" name="new_password" placeholder="Password Baru">
                </div>
                <div class="form-group">
                  <label class=" control-label">Konfirmasi Password baru:</label>
                  <input class="form-control" type="password" name="confirm_password" placeholder="Konfirmasi Password Baru">
                </div>
                <hr class="mt-4">
                <div class="form-group mt-2">
                  <div class="form-group ">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </div>
              </form>
            </div>   
        </div>
      </div><!-- end konten dari profile sidebar -->
    </div>
    </div>
 @endsection