@extends('user.layout_profile.master')
@section('content_profile')
    <!-- Isi dari media -->
    <div class="mr-review-header bg-sidebar-profile  col-lg-9 pl-2 pr-0">
    <div class="col-lg-12 sp-inside-kat sp-header mb-0">
        <div class="col-lg-12 p-kat">
        <p>EDIT PERSONAL INFORMATION</p>
        </div>
    </div> <!-- end dari header profile sidebar -->
    <!-- konten dari profile sidebar -->
    <div class="col-lg-12 profile-sidebar" >
        <div class="col-md-12 pt-3 px-5 personal-info">
        <div class="alert alert-primary alert-dismissable">
            <a class="panel-close close" data-dismiss="alert">×</a>
            @if (Session::has('success'))
                <i class="fa fa-coffee"></i>
                {{ Session::get('success') }}
            @else
                <i class="fa fa-coffee"></i>
                Dengan melengkapi <strong>Biodata</strong>, kamu sudah membantu kami untuk memberikan layanan yang lebih baik.
            @endif
        </div>
        <div class="row pt-2 px-2">
            <div class="col-md-8 col-lg-8 pr-5">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" action="/updateakun" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">Username <span style="color:red;">*</span></label>
                    <input class="form-control" name="username" type="text" placeholder="Masukan Username" value="@isset($user->username){{$user->username}}@endisset" required>
                </div>
                <div class="form-group">
                <label class="control-label">Nama Depan <span style="color:red;">*</span></label>
                <input class="form-control" name="first_name" type="text" placeholder="Masukan Nama Depan" value="@isset($user->first_name){{$user->first_name}}@endisset" required>
                </div>
                <div class="form-group">
                <label class=" control-label">Nama Belakang</label>
                <input class="form-control" name="last_name" type="text" placeholder="Masukan Nama Belakang" value="@isset($user->last_name){{$user->last_name}}@endisset">
                </div>
                <div class="form-group">
                <label class="control-label">Gender <span style="color:red;">*</span></label><br>
                    @isset($user->gender)
                        @if($user->gender == "L")
                        <input type="radio" name="gender" checked value="{{$user->gender}}"> Laki-Laki &nbsp;
                        <input type="radio" name="gender" value="P"> Perempuan
                        @else
                        <input type="radio" name="gender" value="L"> Laki-Laki &nbsp;
                        <input type="radio" name="gender" checked value="{{$user->gender}}"> Perempuan
                        @endif
                    @else
                        <input type="radio" name="gender" value="L"> Laki-Laki &nbsp;
                        <input type="radio" name="gender" value="P"> Perempuan
                    @endisset
                </div>
                <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span style="color:red;">*</span></label>
                    <br>
                    <input id="datepicker" width="276" name="tanggal_lahir" placeholder="YY-MM-DD" value="@isset($user->tanggal_lahir){{$user->tanggal_lahir}}@endisset" required />
                </div>
                <div class="form-group">
                    <label class=" control-label">Email <span style="color:red;">*</span></label>
                    <input class="form-control" name="email" type="text" placeholder="Youremail@mail.com" value="@isset($user->email){{$user->email}}@endisset" required>
                </div>
                <div class="form-group">
                    <label class="control-label">Asal Kamu</label>
                    <div class="ui-select">
                        <select id="user_time_zone" name="region" class="form-control">
                            <option value="#" disabled selected hidden>-Pilih Asal Anda-</option>
                            @foreach($provinsi as $asal)
                                @isset($user->region)
                                    @if($user->region == $asal->kode)
                                        <option value="{{$asal->kode}}" selected>{{$asal->nama}}</option>
                                    @endif
                                @endisset
                                <option value="{{$asal->kode}}">{{$asal->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class=" control-label">Nomor Handphone</label>
                    <input class="form-control" name="hp" type="number" placeholder="0888123123" value="@isset($user->hp){{$user->hp}}@endisset">
                </div>
                <div class="form-group">
                    <label class=" control-label">Website</label>
                    <input class="form-control" name="web" type="text" placeholder="www.yourwebsite.com" value="@isset($user->web){{$user->web}}@endisset">
                </div>
                <div class="form-group">
                <label class="control-label">Biodata</label>
                <textarea name="biodata" class="form-control" maxlength="180" placeholder="biodata" style="height: 190px;">@isset($user->biodata){{$user->biodata}}@endisset</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Simpan">
                    <span></span>
                    <input type="reset" class="btn btn-default" value="Batal">
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
            <center>
                <div class="square">
                        @if($user->img <> NULL || $user->img <> '')
                            @if($user->provider <> NULL || $user->provider <> '')
                                <img class="profile-pic" src="{{$user->img}}">
                            @else
                                <img class="profile-pic" src="{{asset('user/images/user_profile/'.$user->img)}}">
                            @endif
                        @else
                            @if($user->gender =="L")
                                <img class="profile-pic" src="{{asset('user/images/ava_default/merah0ranger.png')}}">
                            @elseif($user->gender =="P")
                                <img class="profile-pic" src="{{asset('user/images/ava_default/pink0ranger.png')}}">
                            @endif
                        @endif
                </div>
                <div class="p-image">
                    <i class="fa fa-camera upload-button"></i>
                    <input class="file-upload" type="file" id="img" name="img" accept="image/*" />
                </div>
            </center>
            </div>
            </form>
        </div>
        </div>
    </div><!-- end konten dari profile sidebar -->
</div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function () {
            var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
    
                reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                }
            }
            $(".file-upload").on('change', function () {
            readURL(this);
            });
    
            $(".upload-button").on('click', function () {
            $(".file-upload").click();
            });
        });
        
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });
    </script>
@endsection