@extends('user.layout_profile.master')
@section('content_profile')
<!-- Isi dari media -->
<div class="mr-review-header bg-sidebar-profile  col-lg-9 pl-3 pr-0">
  <div class="col-lg-12 sp-inside-kat sp-header mb-0">
    <div class="col-lg-12 p-kat">
      <p>GAMBAR & VIDEO
    </div>
  </div>
  <!-- end dari header profile sidebar -->
  <!-- konten dari profile sidebar -->
  <div class="col-lg-12 profile-sidebar">
    <div class="card-columns pt-3">
      <!-- card -->
      <div class="card">
        <img class="card-img-top img-fluid" data-src="holder.js/100px160/" alt="100%x160" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22242%22%20height%3D%22160%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20242%20160%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1610604d310%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1610604d310%22%3E%3Crect%20width%3D%22242%22%20height%3D%22160%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2290.2578125%22%20y%3D%2285.1%22%3E242x160%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
          data-holder-rendered="true">
        <div class="card-block pt-2">
          <h4 style="">ATH - MX150</h4>
          <p class="card-text">
            <small class="text-muted">3 Gambar 1 Videos | 15 Januari 2018</small>
          </p>
        </div>
      </div>
      <!-- end card -->

      <!-- card -->
      <div class="card">
        <img class="card-img-top img-fluid" data-src="holder.js/100px160/" alt="100%x160" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22242%22%20height%3D%22160%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20242%20160%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1610604d310%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1610604d310%22%3E%3Crect%20width%3D%22242%22%20height%3D%22160%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2290.2578125%22%20y%3D%2285.1%22%3E242x160%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
          data-holder-rendered="true">
        <div class="card-block pt-2">
          <h4 style="">ATH - MX150</h4>
          <p class="card-text">
            <small class="text-muted">3 Gambar 1 Videos | 15 Januari 2018</small>
          </p>
        </div>
      </div>
      <!-- end card -->

      <!-- card -->
      <div class="card">
        <img class="card-img-top img-fluid" data-src="holder.js/100px160/" alt="100%x160" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22242%22%20height%3D%22160%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20242%20160%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1610604d310%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1610604d310%22%3E%3Crect%20width%3D%22242%22%20height%3D%22160%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2290.2578125%22%20y%3D%2285.1%22%3E242x160%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
          data-holder-rendered="true">
        <div class="card-block pt-2">
          <h4 style="">ATH - MX150</h4>
          <p class="card-text">
            <small class="text-muted">3 Gambar 1 Videos | 15 Januari 2018</small>
          </p>
        </div>
      </div>
      <!-- end card -->

      <!-- card -->
      <div class="card">
        <img class="card-img-top img-fluid" data-src="holder.js/100px160/" alt="100%x160" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22242%22%20height%3D%22160%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20242%20160%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1610604d310%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1610604d310%22%3E%3Crect%20width%3D%22242%22%20height%3D%22160%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2290.2578125%22%20y%3D%2285.1%22%3E242x160%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
          data-holder-rendered="true">
        <div class="card-block pt-2">
          <h4 style="">ATH - MX150</h4>
          <p class="card-text">
            <small class="text-muted">3 Gambar 1 Videos | 15 Januari 2018</small>
          </p>
        </div>
      </div>
      <!-- end card -->

      <!-- card -->
      <div class="card">
        <img class="card-img-top img-fluid" data-src="holder.js/100px160/" alt="100%x160" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22242%22%20height%3D%22160%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20242%20160%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1610604d310%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1610604d310%22%3E%3Crect%20width%3D%22242%22%20height%3D%22160%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2290.2578125%22%20y%3D%2285.1%22%3E242x160%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
          data-holder-rendered="true">
        <div class="card-block pt-2">
          <h4 style="">ATH - MX150</h4>
          <p class="card-text">
            <small class="text-muted">3 Gambar 1 Videos | 15 Januari 2018</small>
          </p>
        </div>
      </div>
      <!-- end card -->

      <!-- card -->
      <div class="card">
        <img class="card-img-top img-fluid" data-src="holder.js/100px160/" alt="100%x160" src="{{asset('user/images/Review/MD-foxtex.jpg') }}" data-holder-rendered="true">
        <div class="card-block pt-2">
          <h4 style="">ATH - MX150</h4>
          <p class="card-text">
            <small class="text-muted">3 Gambar 1 Videos | 15 Januari 2018</small>
          </p>
        </div>
      </div>
      <!-- end card -->
    </div>
  </div>
<!-- end konten dari profile sidebar -->
</div>
@endsection