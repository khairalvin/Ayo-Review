 <!-- The Modal Lupa Password -->
 <div class="modal fade" id="myverifikasi">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <form role="form" action="" method="post" class="registration-form">
            @if (Session::has('status'))
            <center>
              <i class="fa fa-envelope" style="font-size: 80px; margin-bottom: 20px "></i>
              <br><h4  style=" color:#3d3d3d; margin-bottom: 20px;">Verifikasi Email</h4>
              <p style="font-size: 14px; color:#8d8d8d">{{Session::get('status')}}</p>
            </center>
            @endif
          
            <button type="submit" class="btn btn-block btn-primary py-modal" data-dismiss="modal">OK</button>
          </form>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
  <!-- End Modals Password -->