<!-- The Modal Login-->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Masuk di Ayoreview</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form role="form" action="{{ route('login') }}" method="POST" class="registration-form">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label class="sr-only" for="form-first-name">Facebook</label>
                        <a href="{{ url('/auth/facebook') }}" class="btn btn-block btn-facebook-theme py-modal"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;Masuk dengan Facebook</a>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-last-name">Google</label>
                        <a href="{{ url('/auth/google') }}" class="btn btn-block btn-white py-modal" style="border: 1px solid #00AEDA; color:  #00AEDA;"> <img src="{{asset('user/images/icon/google.png') }}" alt="">&nbsp;&nbsp;Masuk dengan Google</a>
                    </div>
                    <hr class="my-4">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="sr-only" for="form-last-name">Email</label>
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email" class="form-last-name form-control" id="form-last-name">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="sr-only" for="form-last-name">Password</label>
                        <input type="password" name="password" placeholder="Password" class="form-last-name form-control" id="form-last-name">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-block btn-primary py-modal">Masuk</button>
                        <a href="#" data-toggle="modal" data-target="#myForget" data-dismiss="modal" class="d-block pt-2" style="color:  #00AEDA; text-align: center;">Saya Lupa Password</a>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <span style="display: block;">Belum Menjadi member ? <a href="#" data-toggle="modal" data-target="#myRegister" data-dismiss="modal">Daftar Sekarang</a></span>
            </div>

            </div>
        </div>
    </div>