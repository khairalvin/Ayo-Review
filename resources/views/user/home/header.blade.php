<head>
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="shortcut icon" type="image/x-icon" href="{{asset('user/images/favicon.ico') }}">
      <link rel="stylesheet" href="{{asset('user/css/font-awesome.min.css') }}">
      <link rel="stylesheet" href="{{asset('user/css/styles.css') }}">
      <link rel="stylesheet" href="{{asset('user/css/bootstrap.css') }}">
      <title>Ayoreview</title>
</head>