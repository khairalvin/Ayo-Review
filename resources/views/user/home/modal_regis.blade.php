<!-- The Modal Register -->
    <div class="modal fade" id="myRegister">
        <div class="modal-dialog">
            <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Daftar di Ayoreview</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form role="form" action="{{ route('register') }}" method="POST" class="registration-form">
                {{ csrf_field() }}
                <div class="form-group">
                        <label class="sr-only" for="form-first-name">Facebook</label>
                        <a href="#" class="btn btn-block btn-facebook-theme py-modal"><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;Daftar dengan Facebook</a>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-last-name">Google</label>
                        <a href="#" class="btn btn-block btn-white py-modal" style="border: 1px solid #00AEDA; color:  #00AEDA;"> <img src="images/icon/google.png" alt="">&nbsp;&nbsp;Daftar dengan Google</a>
                    </div>
                    <hr class="my-4">
                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label class="sr-only" for="form-last-name">Username</label>
                        <input type="text" name="username" placeholder="Username" class="form-last-name form-control" id="username" value="{{ old('username') }}" required>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="sr-only" for="form-last-name">Email</label>
                        <input type="text" name="email" placeholder="Email" class="form-last-name form-control" id="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="ui-select">
                            <select id="gender" name="gender" class="form-control" required>
                                <option value="#" style="color:#4d4d4d" disabled selected hidden>-Pilih Jenis Kelamin-</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="sr-only" for="form-last-name">Password</label>
                        <input type="password" name="password" placeholder="Password" class="form-last-name form-control" id="password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-last-name">Konfrimasi Password</label>
                        <input type="password" name="password_confirmation" placeholder="Konfirmasi Password" class="form-last-name form-control" input id="password-confirm">
                    </div>
                    <button type="submit" class="btn btn-block btn-primary py-modal">Daftar</button>
                        <p class="d-block pt-2" style="font-size: 14px; text-align: center;">We will never spam you or sell your email to third parties.</p>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <span style="display: block;">Sudah menjadi member ? <a href="#" data-toggle="modal" data-target="#myModal" data-dismiss="modal">Login</a></span>
            </div>

            </div>
        </div>
    </div> {{-- End Modal Regis --}}