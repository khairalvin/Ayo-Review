<!-- The Modal Login-->
<div class="modal fade" id="resetPass">
    <div class="modal-dialog">
        <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Reset Password User</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
            <form role="form" action="{{ route('user-password-reset') }}" method="POST" class="registration-form">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ Session::get('token') }}">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="sr-only" for="form-last-name">Email</label>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="Email" class="form-last-name form-control" id="form-last-name">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="sr-only" for="form-last-name">Password</label>
                    <input type="password" name="password" placeholder="Password" class="form-last-name form-control" id="form-last-name">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="sr-only" for="form-last-name">Confirm Password</label>
                    <input type="password" name="password_confirmation" placeholder="Password Confirmation" class="form-control" id="password-confirm">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-block btn-primary py-modal">Reset Password</button>
            </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
        </div>

        </div>
    </div>
</div>