@php
use App\Kategori;
use Carbon\Carbon;
@endphp
<!-- Header News -->
<div class="row sp-header mr-news-header mx-0">
    <div class="col-lg-12 sp-inside">
      <h1>Berita
        <span>Terbaru</span>
      </h1>
      {{-- <ul class="navbar-nav flex-row ">
        <li class="nav-item ">
          <a class="nav-link sel-berita" href="#">SEMUA</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link " href="#">AUDIOPHILE</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link " href="#">JAM TANGAN</a>
        </li>
      </ul> --}}
    </div>
</div>
<!-- End Sp Header News -->

  <!-- Content News -->
  <div class="row sp-content-news m-0">
    @if(isset($beritas))
    {{-- BERITA 1 --}}
    <div class="col-xs-6 col-sm-2-5 col-lg-2-5 ">
      <div class="card">
      <a href="/news/detail/{{$beritas[0]->id}}"> <img class="card-img-top big-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[0]->img.'')}}" alt="Card image cap"> </a>
        <div class="card-body">
          <div class="tag-big-news">{{Kategori::getName($beritas[0]->kategori_id)}}</div>
          <p class="card-text-big"><a href="/news/detail/{{$beritas[0]->id}}">{{$beritas[0]->judul}}</a></p>
        </div>
        <div class="card-footer">
          <b>Admin</b>&nbsp; diposkan {{ Carbon::parse($beritas[0]->created_at)->format('d M Y')}}
        </div>
      </div>
    </div>
    {{-- BERITA 1 --}}

    {{-- KATEGORI --}}

    <div class="col-xs-6 col-sm-1-5 col-lg-2-5 col-half-offset3">
      <div class="card">
        <div class="tag-lil-news">{{Kategori::getName($beritas[1]->kategori_id)}}</div>
        <a href="/news/detail/{{$beritas[1]->id}}"> <img class="card-img-top lil-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[1]->img.'')}}" alt="Card image cap"> </a>
        <div class="card-body">
          <p class="card-text-lil"><a href="/news/detail/{{$beritas[1]->id}}">{{$beritas[1]->judul}}</a></p>
        </div>
      </div>
      <div class="card mt-2">
        <div class="tag-lil-news">{{Kategori::getName($beritas[2]->kategori_id)}}</div>
        <a href="/news/detail/{{$beritas[2]->id}}"> <img class="card-img-top lil-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[2]->img.'')}}" alt="Card image cap"> </a>
        <div class="card-body">
          <p class="card-text-lil"><a href="/news/detail/{{$beritas[2]->id}}">{{$beritas[2]->judul}}</a></p>
        </div>
      </div>
    </div>

    {{-- /KATEGORI --}}

    {{-- GRID BERITA --}}
    <div class="col-xs-6 col-sm-1-5 col-lg-3-5 col-half-offset3">
        {{-- BERITA 2 --}}
      <div class="card">
        <div class="tag-lil-news">{{Kategori::getName($beritas[3]->kategori_id)}}</div>
        <a href="/news/detail/{{$beritas[3]->id}}"> <img class="card-img-top lil-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[3]->img.'')}}" alt="Card image cap"> </a>
        <div class="card-body">
          <p class="card-text-lil"><a href="/news/detail/{{$beritas[3]->id}}">{{$beritas[3]->judul}}</a></p>
        </div>
      </div>
      {{-- BERITA 3 --}}
      <div class="card mt-2">
        <div class="tag-lil-news">{{Kategori::getName($beritas[4]->kategori_id)}}</div>
        <a href="/news/detail/{{$beritas[4]->id}}"> <img class="card-img-top lil-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[4]->img.'')}}" alt="Card image cap"> </a>
        <div class="card-body">
          <p class="card-text-lil"><a href="/news/detail/{{$beritas[4]->id}}">{{$beritas[4]->judul}}</a></p>
        </div>
      </div>
    </div>

    {{-- GRID BERITA --}}
    <div class="col-xs-6 col-sm-1-5 col-lg-1-5 col-half-offset3">
        {{-- BERITA 4 --}}
        <div class="card">
            <div class="tag-lil-news">{{Kategori::getName($beritas[5]->kategori_id)}}</div>
            <a href="/news/detail/{{$beritas[5]->id}}"> <img class="card-img-top lil-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[5]->img.'')}}" alt="Card image cap"> </a>
            <div class="card-body">
              <p class="card-text-lil"><a href="/news/detail/{{$beritas[5]->id}}">{{$beritas[5]->judul}}</a></p>
            </div>
        </div>

        {{-- BERITA 5 --}}
        <div class="card mt-2">
            <div class="tag-lil-news">{{Kategori::getName($beritas[6]->kategori_id)}}</div>
            <a href="/news/detail/{{$beritas[6]->id}}"> <img class="card-img-top lil-news-img" src="{{asset('admin/img/berita_thumb/'.$beritas[6]->img.'')}}" alt="Card image cap"> </a>
            <div class="card-body">
              <p class="card-text-lil"><a href="/news/detail/{{$beritas[6]->id}}">{{$beritas[6]->judul}}</a></p>
            </div>
        </div>
        @endif
    </div>
  </div>
  <!-- End Content News -->