@php
use App\Kategori;
@endphp
<nav class="navbar nav-index navbar-expand-sm navbar-dark flex-column flex-md-row bd-navbar py-3 navbar-down fixed-top" role="navigation">
    <div class="container-fluid p-x01">
        <a href="/" class="navbar-brand">
        <img id="" src="{{asset('user/images/Logo-white.png') }}" alt="">
        </a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav-scroll">
        <ul class="navbar-nav bd-navbar-nav flex-row ml-md-4">
            <li class="nav-item  dropdown">
            <a class="nav-item nav-link dropdown-toggle kategori-border" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
                REVIEW
            </a>
            @php($listKategori = Kategori::all())
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                    @foreach($listKategori as $listkat)
                        <a class="dropdown-item " href="/kategorireview/{{$listkat->urlname}}">{{$listkat->nama}}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item ml-md-3">
            <a class="nav-link " href="/news">BERITA</a>
            </li>
            <!-- <li class="nav-item">
                    <a class="nav-link " href="/videos.html">VIDEOS</a>
                </li> -->
            <!-- <li class="nav-item">
                    <a class="nav-link " href="/thebest.html">THE BEST</a>
                </li> -->
            {{-- <li class="nav-item">
            <a class="nav-link " href="/buyingGuide.html">BUYING GUIDES</a>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link " href="/aboutus">ABOUT US</a>
            </li>
            <!-- <li class="nav-item">
                    <a class="nav-link " href="/komunitas.html">KOMUNITAS</a>
                </li> -->
        </ul>
        </div>

        @if(Auth::id())
        <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
            <li id="hideSearch" class="unreveal">
                <div id="morphsearch" class="morphsearch">
                    <form class="morphsearch-form navbar-form">
                        <input id="bigSearch" class="morphsearch-input " type="search" placeholder="CARI DISINI" onkeyup="search(this.value)" />
                        <button class="morphsearch-submit" type="submit">Search</button>
                    </form>
                    <div class="morphsearch-content">
                        <div class="dummy-column" >
                            <h2>User</h2>
                            <div id="form-result-akun"></div>
                        </div>
                        <div class="dummy-column">
                            <h2>Review</h2>
                            <div id="form-result-review"></div>
                        </div>
                        <div class="dummy-column">
                            <h2>Berita</h2>
                            <div id="form-result-berita"></div>
                        </div>
                    </div>
                    <span class="morphsearch-close"></span>
                </div>
                <!-- /morphsearch -->
            </li>
            <li class="nav-item  dropdown notifikasi-menu">
                <a class="nav-item nav-link  login-border4 mr-2" id="dLabel" role="button" data-toggle="dropdown" href="#">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
                <div class="notification-heading">
                    <h4 class="menu-title">Kamu Bisa :</h4>
                </div>
                <div class="notifications-wrapper">
                    <a class="content"  data-toggle="modal" data-target="#reqmodal">
                        <div class="review-item">
                            <span class="fa fa-star icons"></span>
                            <h4 class="item-title">Request Produk</h4>
                            <p class="item-info">Usulkan produk yang ingin di review</p>
                        </div>
                    </a>
                    <a class="content" href="#">
                        <div class="review-item" data-toggle="modal" data-target="#saranmodal">
                            <span class="fa fa-thumbs-up icons"></span>
                            <h4 class="item-title">Memberikan Saran</h4>
                            <p class="item-info">Bagikan pengalaman kamu</p>
                        </div>
                    </a>
                </div>
                </ul>
            </li>
            <!-- end create review -->
            <!-- notification -->
            {{-- <li class="nav-item  dropdown notifikasi-menu">
                <a class="nav-item nav-link  login-border3" id="dLabel" role="button" data-toggle="dropdown" href="#">
                <i class="fa fa-bell-o" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
                <div class="notification-heading">
                    <h4 class="menu-title">Notifikasi</h4>
                    <a href="">
                    <!-- <h4 class="menu-title pull-right">View all &nbsp;
                        <i class="fa fa-arrow-circle-right"></i>
                    </h4> -->
                    </a>
                </div>
                <div class="notifications-wrapper">
                    <a class="content" href="#">
                    <div class="notification-item">
                        <img src="http://www.leapcms.com/images/100pixels1.gif">
                        <h4 class="item-title">Evaluation Deadline
                        <small> 1 day ago</small>
                        </h4>
                        <p class="item-info">Mr hassan has followed you!</p>
                    </div>
                    </a>
                    <a class="content" href="#">
                    <div class="notification-item">
                        <img src="http://www.leapcms.com/images/100pixels1.gif">
                        <h4 class="item-title">Evaluation Deadline
                        <small> 1 day ago</small>
                        </h4>
                        <p class="item-info">Marketing 101, Video Assignment</p>
                    </div>
                    </a>
                    <a class="content" href="#">
                    <div class="notification-item">
                        <img src="http://www.leapcms.com/images/100pixels1.gif">
                        <h4 class="item-title">Evaluation Deadline
                        <small> 1 day ago</small>
                        </h4>
                        <p class="item-info">Mr hassan has followed you!</p>
                    </div>
                    </a>
                    <a class="content" href="#">
                    <div class="notification-item">
                        <img src="http://www.leapcms.com/images/100pixels1.gif">
                        <h4 class="item-title">Evaluation Deadline
                        <small> 1 day ago</small>
                        </h4>
                        <p class="item-info">Mr hassan has followed you!</p>
                    </div>
                    </a>
                    <a class="content" href="#">
                    <div class="notification-item">
                        <img src="http://www.leapcms.com/images/100pixels1.gif">
                        <h4 class="item-title">Evaluation Deadline
                        <small> 1 day ago</small>
                        </h4>
                        <p class="item-info">Mr hassan has followed you!</p>
                    </div>
                    </a>
                </div>
                </ul>
            </li> --}}
            <!-- End notification -->
            <li class="nav-item  dropdown">
                <a href="" class="ml-2 nav-item" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth::user()->img <> NULL || Auth::user()->img <> '')
                        @if(Auth::user()->provider <> NULL || Auth::user()->provider <> '')
                            <img src="{{Auth::user()->img}}" width="42px" height="33px" alt="">
                        @else
                            <img src="{{asset('user/images/user_profile/'.Auth::user()->img.'')}}" width="42px" height="33px" alt="">
                        @endif
                    @else
                        @if(Auth::user()->gender == "L")
                            <img src="{{asset('user/images/ava_default/merah0ranger.png')}}" width="42px" height="33px" alt="">
                        @elseif(Auth::user()->gender == "P")
                            <img src="{{asset('user/images/ava_default/pink0ranger.png') }}" width="42px" height="33px" alt="">
                        @endif
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
                    <a class="dropdown-item" href="/profile/{{Auth::user()->username}}">Profile</a>
                    @if(Auth::user()->hasRole('admin'))
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/markas">Menuju Markas</a>
                    @endif
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/logout">Logout</a>
                    </div>
            </li>
        </ul>
        @else()
        <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
            <li id="hideSearch" class="unreveal">
                <div id="morphsearch" class="morphsearch">
                    <form class="morphsearch-form navbar-form">
                        <input class="morphsearch-input " type="search" placeholder="CARI DISINI" onkeyup="search(this.value)" />
                        <button class="morphsearch-submit" type="submit">Search</button>
                    </form>
                    <div class="morphsearch-content">
                        <div class="dummy-column" >
                            <h2>User</h2>
                            <div id="form-result-akun"></div>
                        </div>
                        <div class="dummy-column">
                            <h2>Review</h2>
                            <div id="form-result-review"></div>
                        </div>
                        <div class="dummy-column">
                            <h2>Berita</h2>
                            <div id="form-result-berita"></div>
                        </div>
                    </div>
                    <span class="morphsearch-close"></span>
                </div>
                <!-- /morphsearch -->
            </li>
        </ul>
            <a class=" nav-item nav-link login-border d-none d-lg-inline-block mb-3 mb-md-0 ml-md-3" href="#" data-toggle="modal" data-target="#myModal">LOGIN</a>
        @endif
        </div>
</nav><!-- End Navbar -->