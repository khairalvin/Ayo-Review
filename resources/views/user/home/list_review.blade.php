@php
use App\Kategori;
use Carbon\Carbon;
use App\ReviewImage;
@endphp
<!-- Sp header veriew -->
<div class="row sp-header mr-review-header mt-4  mx-0">
    <div class="col-lg-12 sp-inside-review">
      <h1>Review
        <span>Terbaru</span>
      </h1>
      <ul class="navbar-nav flex-row ">
        <li class="nav-item ">
          <a class="nav-link sel-berita" href="#">SEMUA</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link " href="#">AUDIOPHILE</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link " href="#">JAM TANGAN</a>
        </li>
      </ul>
    </div>
</div>
  <!-- End Sp HEader Review -->
  @isset($reviews)
  <!-- Sp Content Review -->
  <div class="row sp-content-review mb-4 mx-0 ">
    <div class="col-sm-3 col-md-4">
      <div class="card">
        <div class="tag-big-review">{{Kategori::getName($reviews[0]->review->kategori_id)}}</div>
        <a href="/kategorireview/{{$reviews[0]->review->urlname}}"><img class="card-img-top" src="{{asset('admin/img/review/'.ReviewImage::isThumbnail($reviews[0]->review->id).'') }}" alt="Card image cap"></a>
        <div class="card-title-big">
            <a href="/kategorireview/{{$reviews[0]->review->urlname}}">{{$reviews[0]->review->judul}}</a>
        </div>
        <div class="circle-progress">
          <div class="progress blue-90">
            <span class="progress-left">
              <span class="progress-bar"></span>
            </span>
            <span class="progress-right">
              <span class="progress-bar"></span>
            </span>
            <div class="progress-value">{{$reviews[0]->total}}</div>
          </div>
        </div>
        <div class="card-body">
            <p class="card-text-lil">Deskripsi</p>
            <p class="card-text-lil-deskripsi"><?=strip_tags(substr($reviews[0]->review->konten,0,150))?></p>
            <p class="card-text-lil mt-2 ">Nilai</p>
            @foreach($reviews[0]->rating as $rating)
                <p class="card-text-kriteria">{{$rating->nama}}</p>
                <div class="progress mb-1">
                <div class="progress-bar" role="progressbar" aria-valuenow="{{$rating->nilai}}0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    <p>{{$rating->nilai}}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="card-footer">
          <i class="fa fa-comments" aria-hidden="true"></i>
          <a href="">
            <b>&nbsp; 25 Review Pengguna</b>
          </a>
          <a href="/kategorireview/{{$reviews[0]->review->urlname}}" class="pull-right">Selengkapnya</a>
        </div>
      </div>
    </div>
    <div class="col-md-4 midle-review">
      <ul>
        @for($i=1; $i<5; $i++)
          @isset($reviews[$i])
          <li>
            <div class="circle-progress-lil index-progres">
              <div class="progress blue-100">
                <span class="progress-left">
                  <span class="progress-bar"></span>
                </span>
                <span class="progress-right">
                  <span class="progress-bar"></span>
                </span>
                <div class="progress-value">{{$reviews[$i]->total}}</div>
              </div>
            </div>
            <div class="tag-lil-review">
              {{Kategori::getName($reviews[$i]->review->kategori_id)}}
            </div>
            <a href="/kategorireview/{{$reviews[$i]->review->urlname}}"><img src="{{asset('admin/img/review/'.ReviewImage::isThumbnail($reviews[$i]->review->id).'') }}"></a>
            <a href="/kategorireview/{{$reviews[$i]->review->urlname}}"><div class="text-title ">{{$reviews[$i]->review->judul}}</div></a>
            <div class="text-footer">{{ Carbon::parse($reviews[$i]->review->created_at)->format('d M Y')}} | Ayoreview</div>
          </li>
          @endisset
        @endfor
      </ul>
    </div>
    <div class="col-sm-12 col-md-4 ">
      <ul>
          @for($i=5; $i<8; $i++)
            @isset($reviews[$i])
            <li>
              <div class="circle-progress-lil index-progres">
                <div class="progress blue">
                  <span class="progress-left">
                    <span class="progress-bar"></span>
                  </span>
                  <span class="progress-right">
                    <span class="progress-bar"></span>
                  </span>
                  <div class="progress-value">{{$reviews[$i]->total}}</div>
                </div>
              </div>
              <div class="tag-lil-review">
                {{Kategori::getName($reviews[$i]->review->kategori_id)}}
              </div>
              <a href="/kategorireview/{{$reviews[$i]->review->urlname}}"><img src="{{asset('admin/img/review/'.ReviewImage::isThumbnail($reviews[$i]->review->id).'') }}"></a>
              <a href="/kategorireview/{{$reviews[$i]->review->urlname}}"><div class="text-title ">{{$reviews[$i]->review->judul}}</div></a>
              <div class="text-footer">{{ Carbon::parse($reviews[$i]->review->created_at)->format('d M Y')}} | Ayoreview</div>
            </li>
            @endisset
          @endfor
        </ul>
    </div>
  </div>
  <!-- End Sp content Review -->
  @endisset