@extends('user.home.master')
@section('content')
  <!-- Navbar -->
  @include('user.home.navbar')
  <!-- The Modal Login-->
  @include('user.home.modal_login')
  @include('user.home.modal_regis')
  @include('user.home.modal_myforget')
  <!-- Big Search Review -->
  <div class="container-fluid big-logo-row">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 big-logo-container">
            @if (Session::has('status'))
              @php $status = Session::get('status'); @endphp
            @else
              @php $status = ''; @endphp
            @endif
            @if (Session::has('success'))
              @php $success = Session::get('success'); @endphp
            @else
              @php $success = ''; @endphp
            @endif
            @if (Session::has('fail'))
              @php $fail = Session::get('fail'); @endphp
            @else
              @php $fail = ''; @endphp
            @endif
          <h1>TEMUKAN ULASANNYA DISINI</h1>  
          <div id="search-container">
            <div class="input-group stylish-input-group">
              <input type="text" id="BigSearch" class="form-control">
              <span class="input-group-addon">
                <button type="submit fa fa-search">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </div>
          <p>Jelajahi berbagai berita dan ulasan produk yang kamu minati.</p>
          <a href="" class="btn btn-twitter-theme" data-toggle="modal" data-target="#myRegister"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;MASUK DENGAN EMAIL</a>
          <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook-theme">
            <i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;MASUK DENGAN FACEBOOK</a>
          <a href="{{ url('/auth/google') }}" class="btn btn-white">
            <img src="user/images/icon/google.png" alt="">&nbsp;&nbsp;MASUK DENGAN GOOGLE</a>
        </div>
      </div>
    </div>
  </div><!-- End Big Search Review -->
  <main class="container">
    <!-- Text starter -->
    <div class="starter">
      <h1>Temukan Kategori Produk Favoritmu</h1>
      <p class="lead">Yuk saling berbagi ulasan dan temukan berita terbaru dalam kategori dibawah ini </p>
    </div>
    <!-- End starter text-->
    <!-- Kategori besar didepan -->
    <div class="row grid-divider m-0">
      <div class="col-sm-3 bg-green-cus-1">
        <div class="col-padding-kategori">
          <h2>Jam Tangan</h2>
          <h3>ANALOG & DIGITAL</h3>
          <span>Mesin Automatic, Quartz, Solar dll</span>
        </div>
      </div>
      <div class="col-sm-3 bg-jamtangan">
        <div class="col-padding-kategori">
        </div>
      </div>
      <div class="col-sm-3 bg-primary bg-green-cus-2">
        <div class="col-padding-kategori">
          <h2>Audiophile</h2>
          <h3>HOME & PORTABLE</h3>
          <span>Headphone, Player, Amplifier dll</span>
        </div>
      </div>
      <div class="col-sm-3 bg-audio">
        <div class="col-padding-kategori">
        </div>
      </div>
    </div>
    <!-- <div class="show-grid">
            <div class="row m-0 ">
              <div class="col-xs-6 col-sm-1-5 col-lg-2-5 " style="position: relative;">
                <span class="bg-kamera">
                  <H4>Fotografi</H4>
                  <div class="overlay"></div>
                </span>
              </div>
              <div class="col-xs-6 col-sm-1-5 col-lg-1-5 col-half-offset " style="position: relative;">
                <span class="bg-vapor">
                  <H4>Vapor</H4>
                  <div class="overlay"></div>
                </span>
              </div>
              <div class="col-xs-6 col-sm-1-5 col-lg-2-5 col-half-offset " style="position: relative;">
                <span class="bg-smartphone">
                  <H4>Smartphone</H4>
                  <div class="overlay"></div>
                </span>
              </div>
              <div class="col-xs-6 col-sm-1-5 col-lg-3-5 col-half-offset " style="position: relative;">
                <span class="bg-tech">
                  <H4>Latop & Desktop</H4>
                  <div class="overlay"></div>
                </span>
              </div>
              <div class="col-xs-6 col-sm-1-5 col-lg-1-5 col-half-offset " style="position: relative;">
                <span class="bg-sneakers">
                  <h4>Sneakers</h4>
                  <div class="overlay"></div>
                </span>

              </div>
            </div>
            <button class="btn btn-outline-secondary btn-nav pull-right ">
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </button>
            <button class="btn btn-outline-secondary pull-right btn-nav mr-2">
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </button>
          </div> -->
    <!--End Kategori Besar didepan -->
    @include('user.home.list_berita')
    @include('user.home.list_review')
  </main><!-- Komunitas Section -->
@endsection