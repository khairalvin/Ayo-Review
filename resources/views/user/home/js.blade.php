<script src="{{asset('user/js/jquery.min.js') }}"></script>
<script src="{{asset('user/js/popper.min.js') }}"></script>
<script src="{{asset('user/js/bootstrap.min.js') }}"></script>
<script src="{{asset('user/js/classie.js') }}"></script>
<script src="{{asset('user/js/ayoreview.js') }}"></script>
<script>
    var status = "{{$status}}";
    var success = "{{$success}}";
    var fail = "{{$fail}}";
    if(status == ''){
    }else if(status == "reset"){
      $('#resetPass').modal('show');
    }else{
      $('#myverifikasi').modal('show');
    }

    if(success == ''){
    }else{
      $('#mysuccess').modal('show');
    }

    if(fail == ''){
    }else{
      $('#resetPass').modal('show');
    }

  (function () {
    var morphSearch = document.getElementById('morphsearch'),
      bigSearch = document.getElementById('BigSearch'),
      hideSearch = document.getElementById('hideSearch'),
      input = morphSearch.querySelector('input.morphsearch-input'),
      ctrlClose = morphSearch.querySelector('span.morphsearch-close'),
      navbar = document.querySelector('.navbar');
      isOpen = isAnimating = false,

      // show/hide search area
    toggleSearch = function (evt) {
      // Show/hide li search in index
      if ((top.location.pathname === '/')){
        hideSearch.classList.remove('unreveal');
      }
      // return if open and the input gets focused
      if (evt.type.toLowerCase() === 'focus' && isOpen) return false;
      var offsets = morphsearch.getBoundingClientRect();
      if (isOpen) {
        navbar.classList.remove("nav-full");
        classie.remove(morphSearch, 'open');
        // trick to hide input text once the search overlay closes 
        // todo: hardcoded times, should be done after transition ends
        if (input.value !== '') {
          setTimeout(function () {
            classie.add(morphSearch, 'hideInput');
            setTimeout(function () {
              classie.remove(morphSearch, 'hideInput');
              input.value = '';
            }, 300);
          }, 500);
        }
        input.blur();
        if ((top.location.pathname === '/')){
           setTimeout(() => {
             hideSearch.classList.add('unreveal');
           }, 300);
        }
      } else {
        classie.add(morphSearch, 'open');
        setTimeout(() => {
          navbar.classList.add('nav-full');
        }, 400);
      }
      isOpen = !isOpen;
    };

    // events
    if ((top.location.pathname === '/')){
      bigSearch.addEventListener('focus', toggleSearch);
    }
    
    input.addEventListener('focus', toggleSearch);
    ctrlClose.addEventListener('click', toggleSearch);
    
    // esc key closes search overlay
    // keyboard navigation events
    document.addEventListener('keydown', function (ev) {
      var keyCode = ev.keyCode || ev.which;
      if (keyCode === 27 && isOpen) {
        toggleSearch(ev);
      }
    });
    /***** for demo purposes only: don't allow to submit the form *****/
    morphSearch.querySelector('button[type="submit"]').addEventListener('click', function (ev) {
      ev.preventDefault();
    });
  })();
</script>

{{-- SEARCHING --}}

<script>
    function search(id){
        var _token = $('input[name="_token"]').val();
        if(id == ''){
        $('#form-result-berita').empty();
        $('#form-result-akun').empty();
        $('#form-result-review').empty();
        $('#bigSearch').empty();
        }else{
          $('#bigSearch').empty();
        $.ajax({
            type:'POST',
            url:"/search",
            data:{
                '_token' : _token,
                'param':id
            },
            success:function(data){
                // console.log(data);
                parsingdata(data);
            }
        });
        }
    }

    function parsingdata(data){
      var jsonObj = $.parseJSON(data);
      if(jsonObj.akun == "" && jsonObj.berita == ""){
        $('#form-result-akun').empty();
        $('#form-result-berita').empty();
        $('#bigSearch').empty();
      }
      var htmlakun = '';
      var htmlberita = '';
      // console.log(jsonObj.akun);
      $.each(jsonObj.akun,function(key,value){
        htmlakun += '<a class="dummy-media-object" href="/profile/' + value.username + '">';
        if(value.img == ""){
          if(value.gender == "L"){
            var image = "{{asset('user/images/ava_default/merah0ranger.png')}}";
          }else if(value.gender == "P"){
            var image = "{{asset('user/images/ava_default/pink0ranger.png')}}";
          }
        }else{
          var image = "{{asset('user/images/user_profile/')}}/"+value.img;
        }
        htmlakun += '<img class="round" src="'+image+'" alt="Sara Soueidan"/>';
        htmlakun += '<h3>' + value.username + '</h3>';
        htmlakun += '</a>';
        $('#form-result-akun').empty();
        $('#form-result-akun').append(htmlakun);
      });
      $.each(jsonObj.berita,function(key,value){
        htmlberita += '<a class="dummy-media-object" href="/news/detail/' + value.id + '">';
        var imageberita = "{{asset('admin/img/berita_thumb/')}}/"+value.img;
        htmlberita += '<img src="'+imageberita+'" alt="Sara Soueidan"/>';
        htmlberita += '<h3>' + value.judul + '</h3>';
        htmlberita += '</a>';
        $('#form-result-berita').empty();
        $('#form-result-berita').append(htmlberita);
      });
    }
</script>