<!DOCTYPE html>
<html class="no-js" lang="en">
    @include('user.home.header')
    <body onload="loaded()">
      <div id="loader">
      </div>
      <div id="kontent">
        @yield('content')
        @include('user.home.modalEmail')
        @include('user.home.modal_reset')
        @include('user.home.modal_request')
        @include('user.home.modal_saran')
        @include('user.home.modalSuccess')
          @include('user.home.footer')
      </div>
        @include('user.home.js')
    </body>
</html>




