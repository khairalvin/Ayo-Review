<!-- modal request item -->
<div class="modal fade" id="saranmodal" tabindex="-1" role="dialog" aria-labelledby="saranmodal" aria-hidden="true">
        <form enctype="multipart/form-data" action="/sendSaran" method="post">
        {{ csrf_field() }}
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="saranmodalLabel">Berikan Saran :</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Saran :</label>
                    <textarea class="form-control" name="saran" id="req-item" style="height: 150px; resize: none;"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Kirim Request</button>
            </div>
            </div>
        </div>
    </form>
</div>
<!-- end modal request item -->