@php
use App\ReviewIndikator;
use App\ReviewImage;
use App\Review;
use App\ReviewSubkategori;
@endphp
<main role="main" class="container px-0">
    <ul class="grid effect-3" id="grid">
        <!-- review item -->
        @foreach($reviews as $review)
        <li>
            <div class="card-review">
            <div class="review-image" style="background-image:url('{{asset('admin/img/review/'.ReviewImage::isThumbnail($review->id).'') }}');" style="">
                    <div class="circle-big review-rate2">
                        <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">{{ ReviewIndikator::sumValue($review->id) }}</div>
                        </div>
                    </div>
                    <a href="/reviewadmin/{{$review->urlname}}">
                        <img src="{{asset('admin/img/review/'.ReviewImage::isThumbnail($review->id).'')}}" style="visibility: hidden;" />
                        <div class="overlay3"></div>
                    </a>
                </div>
                <div class="tag-review">
                    <p>
                        <a href="audio.html" class="hov-brand">{{Review::find($review->id)->subkategori()->first()->subkategori}}</a>&nbsp;
                        <span class="dot">•</span>&nbsp;
                        <a href="audio.html" class="hov-sub-kat">{{Review::find($review->id)->brand()->first()->nama}}</a>
                        <span class="dot">•</span>&nbsp;
                        <a href="audio.html" class="hov-kat">Rp. {{$review->harga}}</a>
                        {{-- <a href="audio.html" class="label-trending pl-2">
                        <i class="fa fa-line-chart "></i>&nbsp;Trending</a> --}}
                    </p>
                </div>
                <div class="review-title2">
                <a href="/reviewadmin/{{$review->urlname}}">{{$review->judul}}</a>
                </div>
                <div class="review-footer">
                    <div class="fa fa-comment"></div> &nbsp; 5 Komentar
                    <div class="fa fa-star pl-2"></div> &nbsp; 5 Ulasan
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</main>