<!-- top nav tab -->
<div class="container-fluid top-nav-tab" data-toggle="sticky-onscroll">
    <div class="container">
        <ul class="navbar-nav flex-row justify-content-sm-center">

        <li>
            <select class="sub-catagories" name="Sub kategori" style="width: 140px">
            <option value="" selected disabled hidden>SUB KATEGORI</option>
            <option value="AM">DAC / AMP</option>
            <option value="EB">EARBUD</option>
            <option value="EP">EARPHONE</option>
            <option value="HP">HEADPHONE</option>
            <option value="HP">PLAYER</option>
            <option value="SP">SPEAKER</option>
            </select>
        </li>
        <li>
            <select class="brand" name="brand" style="width: 140px">
            <option value="" selected disabled hidden>BRAND</option>
            <option value="AM">AKG</option>
            <option value="AM">AUDIO TECHNICA</option>
            <option value="EB">SENNHEISER</option>
            <option value="EP">SHURE</option>
            <option value="HP">SONY</option>
            </select>
        </li>
        <li>
            <select class="price" name="harga" style="width: 140px">
            <option value="" selected disabled hidden>HARGA</option>
            <option value="AM">
                < Rp. 100 ribu</option>
                <option value="EB">Rp. 100 - 200 ribu</option>
                <option value="EP">Rp. 200 - 300 ribu</option>
                <option value="HP">Rp. 300 - 500 ribu</option>
                <option value="HP">Rp. 500 - 1 juta</option>
                <option value="SP">Rp. 1 - 2 juta</option>
                <option value="HP">Rp. 2 - 3 juta</option>
                <option value="HP">Rp. 3 - 5 juta</option>
                <option value="SP">Rp. 5 - 7 juta</option>
                <option value="SP">Rp. 7 - 10 juta</option>
                <option value="SP">Rp. 10 - 15 juta</option>
                <option value="SP">Rp. 20 - 30 juta</option>
                <option value="SP">Rp. 30 - 50 juta</option>
            </select>
        </li>
        <li>
            <select class="sort-review" name="harga" style="width: 140px">
            <option value="" selected disabled hidden>URUTKAN BERDASRKAN</option>
            <option value="new">TRENDING</option>
            <option value="new">TERBARU</option>
            <option value="new">TERLAMA</option>

            </select>
        </li>

        </ul>
    </div>
</div>
<!-- end top nav tab -->