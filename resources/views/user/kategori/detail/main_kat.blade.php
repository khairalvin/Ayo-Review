@extends('user.layout_main.master')
@section('content')
  @include('user.kategori.detail.pil_kategori')
  @include('user.kategori.detail.filter')
  @include('user.kategori.detail.kategori_detail')
@endsection
@section('custom_js')
<script>
  // FUNGSI PADA KATEGORI PRODUK UNTUK DIREVIEW
    new AnimOnScroll(document.getElementById('grid'), {
      minDuration: 0.4,
      maxDuration: 0.7,
      viewportFactor: 0.2
    });
    
    $(document).ready(function () {
      $('.reset-select').hide();

      $('.sub-catagories').select2({
        placeholder: "SUB KATEGORI",
      });

      $('.brand').select2({
        placeholder: "BRAND",
      });
      $('.price').select2({
        placeholder: "HARGA",
      });
      $('.sort-review').select2({
        placeholder: "URUTKAN",
      });

      $('.sub-catagories, .brand, .price, .sort-review').on('change', function () {
        $('.reset-select').show();
      })

      $('.reset-select').click(function () {
        $('.sub-catagories, .brand, .price, .sort-review').val(null).trigger('change');
        $('.reset-select').hide();
      });

    });
// END FUNGSI PADA KATEGORI PRODUK UNTUK DIREVIEW
</script>
@endsection 