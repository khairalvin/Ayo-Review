<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="email=no">
      
        <style type="text/css">
          @font-face {
            font-family: "FaktProSoft";
            font-weight: 300;
            font-style: normal;
          }
      
          @font-face {
            font-family: "FaktProSoft";
            font-weight: 400;
            font-style: normal;
          }
      
          @font-face {
            font-family: "FaktProSoft";
            font-weight: 500;
            font-style: normal;
          }
      
          .ReadMsgBody {
            width: 100%;
            background-color: #EFF2F7;
          }
      
          .ExternalClass {
            width: 100%;
            background-color: #EFF2F7;
          }
      
          .ExternalClass,
          .ExternalClass p,
          .ExternalClass span,
          .ExternalClass font,
          .ExternalClass td,
          .ExternalClass div {
            line-height: 110%;
          }
      
          .ExternalClass * {
            line-height: 110%;
          }
      
          body {
            margin: 0;
            padding: 0;
          }
      
          body,
          table,
          td,
          p,
          a,
          li {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
          }
      
          table td {
            border-collapse: collapse;
          }
      
          table {
            border-spacing: 0;
            border-collapse: collapse;
      
      
          }
      
          table,
          td {}
      
      
          @media screen {
      
            body {
              font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif !important;
            }
      
          }
      
          @media only screen and (max-width: 600px) {
            body {
              background: none !important;
              padding: 0px !important;
              margin: 0px !important;
            }
      
            td[class="margin_bottom"] {
              margin: 0px auto 20px !important;
              float: none !important;
            }
            td[class="small_margin_bottom"] {
              margin: 0px auto 10px !important;
              float: none !important;
            }
            td[class="frame"] {
              padding: 0px 20px !important;
              min-width: 0px !important;
              height: auto !important;
            }
            td[class="small_frame"] {
              padding: 0px 10px !important;
              min-width: 0px !important;
              height: auto !important;
            }
            td[class="frame_big"] {
              padding: 20px !important;
            }
            td[class="frame devicewidth"] {
              padding: 0px 20px !important;
              min-width: 0px !important;
              width: 100% !important;
            }
            table[class="no_float"] {
              float: none !important;
              margin: 0 auto !important;
              min-width: 0px !important;
            }
            table[class="no_float margin_bottom"] {
              float: none !important;
              margin: 0 auto 20px !important;
              min-width: 0px !important;
            }
            table[class="no_float small_margin_bottom"] {
              float: none !important;
              margin: 0 auto 10px !important;
              min-width: 0px !important;
            }
            td[class="none"],
            span[class="none"],
            table[class="none"] {
              display: none !important;
            }
            td[class="textCenter"] {
              text-align: center !important;
            }
            table[class="devicewidth"],
            td[class="devicewidth"] {
              min-width: 1% !important;
              width: 100% !important;
              float: none !important;
              margin: 0 auto !important;
            }
            table[class="devicewidth margin_bottom"] {
              min-width: 1% !important;
              width: 100% !important;
              float: none !important;
              margin: 0 auto 20px !important;
            }
            table[class="devicewidth small_margin_bottom"] {
              min-width: 1% !important;
              width: 100% !important;
              float: none !important;
              margin: 0 auto 10px !important;
            }
            td[class="devicewidthimage"],
            td[class="fullimage"] {
              min-width: 1% !important;
              width: 100% !important;
              margin: 0 auto !important;
            }
            td[class="devicewidth"] img,
            td[class="devicewidthimage"] img {
              margin: 0 auto !important;
            }
            td[class="fullimage"] img {
              width: 100% !important;
              height: auto !important;
              margin: 0 auto !important;
            }
            td[class="space"] {
              height: 20px !important;
            }
            td[class="small_space"] {
              height: 10px !important;
            }
            td[class="height_auto"] {
              height: auto !important;
            }
            td[class="button"] a {
              width: 100% !important;
            }
          }
      
          @media only screen and (max-width: 340px) {}
        </style>
      
        <title>Selamat bergabung di ayoreview.com platform review terbesar di Indonesia 👾</title>
        <meta name="robots" content="noindex, nofollow">
      </head>
      
      <body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #EFF2F7; font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif !important; margin: 0; padding: 0; width: 100%"
        bgcolor="#EFF2F7">
        <style type="text/css">
          .ReadMsgBody {
            width: 100%;
            background-color: #EFF2F7;
          }
      
          .ExternalClass {
            width: 100%;
            background-color: #EFF2F7;
          }
      
          .ExternalClass {
            line-height: 110%;
          }
      
          body {
            margin: 0;
            padding: 0;
          }
      
          body {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
          }
      
          body {
            font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif !important;
          }
        </style>
        <!--[if !mso]><!-- -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#EFF2F7" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0">
          <tbody>
            <tr>
              <td align="center" class="frame" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
      
      
      
                <span class="none" style="font: 15px courier; white-space: nowrap">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
                
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td height="25" class="small_space" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                  </tbody>
                </table>
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td align="center" class="fullimage" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                        <img src="https://www.ayoreview.com/image/WELCOME.jpg"
                          width="500" alt="from-the-ayoreview" style="display: block">
                      </td>
                    </tr>
                  </tbody>
                </table>
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td height="25" class="space" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                  </tbody>
                </table>
      
      
      
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-radius: 6px 6px 6px 6px; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td align="center" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-radius: 6px 6px 0px 0px"
                        class="fullimage">
                        <a
                          style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%" target="_blank">
                          <img src="https://www.ayoreview.com/image/top.jpg"
                            width="600" alt="" style=" display: block">
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td height="40" class="space" bgcolor="#ffffff" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                    <tr>
                      <td align="center" class="frame" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; padding: 0px 40px"
                        bgcolor="#ffffff">
      
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0">
                          <tbody>
                            <tr>
                              <td align="center" class="bodyTitle" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; color: #3c4858; font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 24px; font-style: normal; font-weight: normal; line-height: 34px; text-align: center; text-decoration: none">
                                <a style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3c4858; text-decoration: none"
                                  target="_blank">
                                  <span style="color: #3c4858"><strong>Selamat bergabung di Ayoreview</strong> </span>
                                  <div class="text">
                                    <p style ="font-size: 14px; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin-bottom: 10px; margin-top: 20px; line-height: 24px;     font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;font-weight: 300; border-collapse: collapse; color: #687585;text-align: center;">Ayoreview adalah platform untuk kamu yang mencari review produk dan berita terkini. Dengan membaca review kamu jadi lebih yakin kualitas produk yang sedang kamu incar. Kamu juga bisa ikut menulis reviewmu sendiri dan membantu orang lain menentukan pilihannya. </p>
                                  </div>
                                </a>
                              </td>
                            </tr>
                            <tr>
                              <td height="10" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; height: 10px; line-height: 10px">
                                <img src="https://bucket.mlcdn.com/images/default/spacer.gif" width="1" height="1" style="display: block">
                              </td>
                            </tr>
      
                            <tr>
                              <td align="center" class="bodyText" id="content-6" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; color: #8492a6; font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 18px; font-weight: 300; line-height: 25px; text-align: center">
                                <div class="text">
                                  <p style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin-bottom: 10px; margin-top: 0px">Silahkan klik tombol dibawah ini untuk memverifikasi alamat email kamu</p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td height="10" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; height: 10px; line-height: 10px">
                                <img src="https://bucket.mlcdn.com/images/default/spacer.gif" width="1" height="1" style="display: block">
                              </td>
                            </tr>
                            <tr>
                              <td align="center" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                                <table cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0">
                                  <tbody>
                                    <tr>
                                      <td height="50" align="center" class="button" valign="middle" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                           
      
                                        <a href="{{route('sendEmailDone',["email" => $user->email, "verifyToken"=>$user->verifyToken])}}"
                                          class="bodyTitle" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #13ce66; border: 1px solid #13ce66; border-radius: 4px; color: #ffffff; display: inline-block; font-family: sans-serif; font-size: 16px; font-weight: normal; line-height: 48px; text-align: center; text-decoration: none; width: 144px"
                                          target="_blank">
                                          <span style="color: #ffffff">Verifikasi Email</span>
                                        </a>
      
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
      
                      </td>
                    </tr>
                    <tr>
                      <td height="40" class="space" bgcolor="#ffffff" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-radius: 0px 0px 6px 6px"></td>
                    </tr>
      
                  </tbody>
                </table>
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td height="0" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                  </tbody>
                </table>
      
      
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td height="10" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                  </tbody>
                </table>
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td height="20" class="small_space" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                  </tbody>
                </table>
      
      
      
      
      
      
      
      
      
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td align="center" class="bodyText" id="content-27" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; color: #8492a6; font-family: 'FaktProSoft', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; font-weight: 300; line-height: 30px">
                        <div class="text">
                          <p style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin-bottom: 10px; margin-top: 0px">
                            <strong>Jika kamu memiliki pertanyaan, silahkan hubungi kami di support@ayoreview.com</strong>
                            <br> Tim Ayoreview</p>
                          <p style=" line-height: 25px; font-size:12px; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin-bottom: 10px; margin-top: 0px">
                            Email ini adalah notifikasi otomatis. Jangan membalas lewat email ini. Kamu menerima notifikasi email ini karena kamu telah
                            mendaftar menjadi user Ayoreview. Untuk terus dapat memberikan pelayanan yang terbaik, kami dapat mengirimkan beberapa notifikasi
                            penting mengenai produk dan layanan kami dari waktu ke waktu.</p>
      
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0">
                          <tbody>
                            <tr>
                              <td align="center" width="30" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                                <a href="https://twitter.com/ayoreview" target="_blank" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%">
                                  <img src="https://bucket.mlcdn.com/a/271/271421/templates/0/881/a6c41645513e11b680452970966d5babe2aeb8fc.png"
                                    width="30" style="display: block">
                                </a>
                              </td>
                              <td width="10" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                                <img src="https://bucket.mlcdn.com/images/default/spacer.gif" width="1" height="1" style="display: block">
                              </td>
                              <td align="center" width="30" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                                <a href="https://instagram.com/ayoreview" target="_blank" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%">
                                  <img src="https://www.ayoreview.com/image/ins.png"
                                    width="30" style="display: block">
                                </a>
                              </td>
                              <td width="10" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                                <img src="https://bucket.mlcdn.com/images/default/spacer.gif" width="1" height="1" style="display: block">
                              </td>
                              <td align="center" width="30" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse">
                                <a href="http://www.facebook.com/ayoreview" target="_blank" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%">
                                  <img src="https://bucket.mlcdn.com/a/271/271421/templates/0/881/4cbd1b872b1d22b3ea9d369b44ead9ea35b72c05.png"
                                    width="30" style="display: block">
                                </a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
      
      
      
                <table width="600" cellpadding="0" border="0" cellspacing="0" align="center" class="devicewidth" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse; border-spacing: 0; min-width: 600px; width: 600px">
                  <tbody>
                    <tr>
                      <td height="40" class="space" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse"></td>
                    </tr>
                  </tbody>
                </table>
      
              </td>
            </tr>
          </tbody>
        </table>
      
      
      
      
      </body>