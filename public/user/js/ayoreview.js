 /*
       ___   ____    ____  ______   .______       ___________    ____  __   ___________    __    ____ 
      /   \  \   \  /   / /  __  \  |   _  \     |   ____\   \  /   / |  | |   ____\   \  /  \  /   / 
     /  ^  \  \   \/   / |  |  |  | |  |_)  |    |  |__   \   \/   /  |  | |  |__   \   \/    \/   /  
    /  /_\  \  \_    _/  |  |  |  | |      /     |   __|   \      /   |  | |   __|   \            /   
   /  _____  \   |  |    |  `--'  | |  |\  \----.|  |____   \    /    |  | |  |____   \    /\    /    
  /__/     \__\  |__|     \______/  | _| `._____||_______|   \__/     |__| |_______|   \__/  \__/   


    	Version: 0.1
  	  Author:  Agum Junianto
   	Website: http://Ayoreview.com

   */
// Fungsi global 

 function loaded() {
   setTimeout( () => {
    document.getElementById("loader").style.display = "none";
    document.getElementById("kontent").style.visibility = "visible";
   },50);
 }
 
  // fungsi circle rate
  $(document).ready(function () {
    $('.progress .progress-bar').css("width",
      function () {
        return $(this).attr("aria-valuenow") + "%";
      }
    )
  });
  // end fungsi circle rate

  // follow button 
  // $('button').click(function () {
  //   var $this = $(this);
  //   $this.toggleClass('following')
  //   if ($this.is('.following')) {
  //     $this.addClass('wait');
  //   }
  // }).on('mouseleave', function () {
  //   $(this).removeClass('wait');
  // });
  // End follow button 
// end  Fungsi global

if ((top.location.pathname !== '/')){
  // Slick js untuk top news 
  $(document).ready(function () {
    $('.top-news-content').slick({
      dots: false,
      infinite: true,
      speed: 1000,
      autoplaySpeed: 7000,
      slidesToShow: 1,
      adaptiveHeight: true,
      autoplay: true,
      arrows: false
    });
  });
  // end Slick js untuk top news
}










 // FUNGSI PADA MENU INDEX

 if ((top.location.pathname === '/')) {
   $(window).scroll(function () {
     if ($('.nav-index').offset().top > 50) {
       $('.fixed-top').removeClass('navbar-dark py-3');
       $('.fixed-top').addClass('py-1 top-nav-collapse navbar-light top-nav-collapse');
       $('.navbar-brand').addClass('navbar-brand-scroll');
       $('.login-border').css({
         'color': 'rgba(48, 76, 84, 1)',
         'border': 'solid 1px rgba(48, 76, 84, 1)',
       });
       $('.kategori-border').css({
         'color': 'rgba(48, 76, 84, 1)',
         'border': 'solid 1px rgba(48, 76, 84, 1)',
       });
     } else {
       $('.fixed-top').removeClass('py-1 top-nav-collapse navbar-light top-nav-collapse');
       $('.fixed-top').addClass('navbar-dark py-3');
       $('.navbar-brand').removeClass('navbar-brand-scroll');
       $('.login-border').css({
         'color': 'rgba(255, 255, 255,0.9)',
         'border': 'solid 1px rgba(255, 255, 255,0.9)',
       });
       $('.kategori-border').css({
         'color': '#fff',
         'border': 'solid 1px rgba(255, 255, 255,0.9)',
       });
     }
   });
 } // end navigasi menu index
 // END FUNGSI PADA MENU INDEX









 



 //JAVASCRIPT PADA HALAMAN FULL-NEWS

 if ((top.location.pathname === '/news/news_full.html')) {
   // Untuk memunculkan kolom disqus
  //  const disqusUI = document.querySelector('.disqus');
  //  const komentar = document.getElementById('showcomment');
  //  disqusUI.style.display= "none";
  //  komentar.addEventListener('click', function () {
  //    disqusUI.style.display = "block";
  //    komentar.style.display = "none";
  //  }); 
   // End memunculkan Kolon disqus
 }

 // End JAVASCRIPT PADA HALAMAN FULL-NEWS












 // JAVASCRIPT Pada Halaman FULLREVIEW
  if ((top.location.pathname === '/fullreview.html')) {
    // Fungsi barating
    $(function () {
      $('#rate-1, #rate-2, #rate-3, #rate-4, #rate-5').barrating({
        theme: 'bars-1to10'
      });
    });

    // Fungsi Readmore 
    $(document).ready(function () {
      var readMoreHtml = $(".read-more").html();
      var lessText = readMoreHtml.substr(0, 650);
      if (readMoreHtml.length > 100) {
        $(".read-more").html(lessText);
      } else {
        $(".read-more").html(readMoreHtml);
      }
    }); // end fungsi read-more


    // Fungsi Hide and show untuk konten Review Pada halaman fullreview
    $(document).ready(function () {
      $("#review").hide();
      $(".second").click(function () {
        $('html, body').animate({
          scrollTop: $(".tab-content").offset().top
        }, 500);
        $("#review").show();
        $("#second").show();
        $("#disqus-comment").hide();
      });
    });

    $(document).ready(function () {
      $(".hide").click(function () {
        $('html, body').animate({
          scrollTop: $("main").offset().top
        }, 500);
        $("#review").hide();
        $("#second").hide();
        $("#disqus-comment").show();
      });
    });
    // end Fungsi Hide and show untuk konten Review Pada halaman fullreview

    // Slider menggunakan slick slider pada halaman fullreview 
    $(document).ready(function () {
      $('.review-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        infinite: false,
        adaptiveHeight: true,
        cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        asNavFor: '.slider-nav'
      });

      $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.review-slider',
        dots: false,
        focusOnSelect: true,
        infinite: false,
        arrows: true,
      });

      $('.slider-nav img').click(function () {
        $('img').removeClass('active');
        $(this).addClass("active");
      })
    }); // End fungsi slider

    // Fungsi upload image pada wizard fullreview

    //Check File API support
    if (window.File && window.FileList && window.FileReader) {
      var filesInput = document.getElementById("fileInput");

      filesInput.addEventListener("change", function (event) {

        var files = event.target.files; //FileList object
        var output = document.getElementById("fileResult");

        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          //Only pics
          if (!file.type.match('image'))
            continue;

          var picReader = new FileReader();

          picReader.addEventListener("load", function (event) {

            var picFile = event.target;

            var div = document.createElement("div");
            div.style.display = "inline-block";

            div.innerHTML = "<img class='thumbnail' height='100px' width='100x' src='" + picFile.result + "'" +
              "title='" + picFile.name + "'/>";

            output.insertBefore(div, null);

          });

          //Read the image
          picReader.readAsDataURL(file);
        }

      });
    }
    else {
      console.log("Your browser does not support File API");
    }
    // end Fungsi upload image pada wizard fullreview

    // Create new PRO object
    class Pro{
      constructor(proDetail){
        this.proDetail = proDetail;
      }
    }

    class Cons{
      constructor(consDetail){
        this.consDetail = consDetail;
      }
    }

    class UI{
      addProToList(pro) {
        const list = document.getElementById('proResults');
        // Create tr Element
        const row = document.createElement('ul');
        row.className = 'list-group';
        // Insert cols
        row.innerHTML = `
          <li class="list-group-item mt-2">${pro.proDetail} <a href="" class="delete pull-right fa fa-remove"></a></li>
        `;
        list.appendChild(row);
      }

      addConsToList(cons){
        const list = document.getElementById('consResults');
        // Create tr Element
        const row = document.createElement('ul');
        row.className = 'list-group';
        // Insert cols
        row.innerHTML = `
          <li class="list-group-item mt-2">${cons.consDetail} <a href="" class="delete pull-right fa fa-remove"></a></li>
        `;
        list.appendChild(row);
      }
      deletePro(target){
        if (target.className === 'delete pull-right fa fa-remove') {
          target.parentElement.parentElement.remove();
        }
      }

      deleteCons(target){
        if (target.className === 'delete pull-right fa fa-remove') {
          target.parentElement.parentElement.remove();
        }
      }
      clearFields(){
        document.getElementById('keunggulan').value = '';
      }
    }

    class Storage{
      // Get Book from storage
      static getPros() {
        let pros;
        if (localStorage.getItem('pros') === null) {
          pros = [];
        } else {
          pros = JSON.parse(localStorage.getItem('pros'));
        }
        return pros;
      }

      // Display book from Storage
      static displayPros() {
        const pros = Storage.getPros();
        pros.forEach(function (pro) {
          const ui = new UI;
          // Add book to UI
          ui.addProToList(pro);
        });
      }

      // Add book from storage
      static addPros(pro) {
        const pros = Storage.getPros();
        pros.push(pro);
        localStorage.setItem('pros', JSON.stringify(pros));
      }

      static removePros(proDetail){
        const pros = Storage.getPros();
        pros.forEach(function (pro, index) {
          if (pro.proDetail === proDetail) {
            pros.splice(index, 1);
          }
        });

        localStorage.setItem('pros', JSON.stringify(pros));
      }
    }
    
    // DOM load Event
    document.addEventListener('DOMContentLoaded', Storage.displayPros);

    // event listener on click
    document.getElementById('formPro').addEventListener('submit', function(e){
      e.preventDefault();
      // Get form values
      const proDetail = document.getElementById('keunggulan').value;
      
      // Instantiate new pro
      const pro = new Pro(proDetail);

      // Instantiate UI
      const ui = new UI();

      // Validate
      if (proDetail === '') {
        alert('Error Form tidak boleh Kosong');
      }else{
        // add pro to list
        ui.addProToList(pro);
        // Add to local Storage
        Storage.addPros(pro);
        // Clear Field
        ui.clearFields();
      }
    });

    // event listener on click
    document.getElementById('formCons').addEventListener('submit', function (e) {
      e.preventDefault();
      // Get form values
      const consDetail = document.getElementById('kekurangan').value;

      // Instantiate new pro
      const cons = new Cons(consDetail);

      // Instantiate UI
      const ui = new UI();

      // Validate
      if (consDetail === '') {
        alert('Error Form tidak boleh Kosong');
      } else {
        // add pro to list
        ui.addConsToList(cons);
        // Add to local Storage
        // Storage.addPros(pro);
        // Clear Field
        ui.clearFields();
      }
    });
    

    document.getElementById('proResults').addEventListener('click', function(e){
      e.preventDefault();
      const ui = new UI();
      ui.deletePro(e.target);
      Storage.removePros(e.target.parentElement.textContent);
    });

    document.getElementById('consResults').addEventListener('click', function(e){
      e.preventDefault();
      const ui = new UI();
      ui.deleteCons(e.target);
    });
    document.getElementById('btnRec').addEventListener('click', function(e){
      e.preventDefault();
      let result = document.getElementById('recResults');
      result.innerHTML = `
        <div class="text-center">
          <h3 style = "background-color: #e4f4fa;
          width: 85 % ;
          border: solid 1 px #3fc4f5;" class="py-4">Saya Merekomendasikan Ini</h3>
        </div>
      `;
    });

    document.getElementById('btnUnrec').addEventListener('click', function (e) {
      e.preventDefault();
      let result = document.getElementById('recResults');
      result.innerHTML = `
        <div class="text-center">
          <h3 style = " background-color: #f7d9d5;
          width: 85 % ;
          border: solid 1 px# F45944;" class="py-4">Saya Tidak Merekomendasikan Ini</h3>
        </div>
      `;
    });

 } 
 // end JAVASCRIPT Pada Halaman FULLREVIEW


 // Javascript untuk halaman profile_review
if ((top.location.pathname === '/profile')){
  // Fungsi barating
  $(function () {
    $('#rate-1, #rate-2, #rate-3, #rate-4, #rate-5').barrating({
      theme: 'bars-1to10'
    });
  });

}