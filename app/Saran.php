<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saran extends Model
{
    protected $table = 'request';
    protected $fillable = ['id_user', 'isi', 'status','tipe','kategori_id'];

    public static function toRead($id){
        $data = Saran::where('id', '=', $id)->firstOrFail();

        $data->status = "Y";
        $data->save();
    }
}
