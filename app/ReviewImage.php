<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewImage extends Model
{
    protected $table = 'review_image';
    protected $fillable = ['review_id', 'user_id', 'name', 'isThumbnail'];
    public $timestamps = false;

    public function review()
    {
      return $this->belongsTo('App\Review');
    }

    public static function isThumbnail($id){
        return ReviewImage::where('isThumbnail',1)->where('review_id',$id)->first()->name;
    }
}
