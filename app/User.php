<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','first_name','last_name','gender','tanggal_lahir','region','hp','biodata','web','img','verifyToken','provider','provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function search($param){
        return User::select('id', 'username', 'img','gender')->where('username', 'like', '%'.$param.'%')->orWhereRaw('email', 'like', '%'.$param.'%')->orWhereRaw('first_name', 'like', '%'.$param.'%')->orWhereRaw('last_name', 'like', '%'.$param.'%')->orderBy('first_name', 'asc')->limit(5)->get();
    }
    
    public function review(){
      return $this->hasMany('App\Review');
    }

    public static function verifyAccount($email,$token){
        return User::where(['email'=>$email,'verifyToken'=>$token])->update(['status'=>'1','verifyToken'=>NULL]);
    }

}
