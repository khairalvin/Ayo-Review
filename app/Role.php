<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use Illuminate\Support\Facades\DB;

class Role extends EntrustRole
{
    protected $table = 'roles';
    protected $fillable = [
        'name', 'display_name', 'description',
    ];
    public static function showAll()
    {
        return Role::join('role_user', 'roles.id', '=', 'role_user.role_id')
        ->join('users', 'role_user.user_id', '=', 'users.id')
        ->select('users.id as user_id','users.username','roles.id as role_id','roles.display_name')
        ->get();
    }

    public static function updateRole($user_id,$role_id,$role_lama)
    {
        return DB::table('role_user')->where(['user_id'=>$user_id,'role_id'=>$role_lama])->update(['user_id' => $user_id,'role_id'=>$role_id]);
    }
}
