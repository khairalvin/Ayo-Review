<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewUserFitur extends Model
{
    protected $table = 'review-user_fitur';
    protected $fillable = ['review-user_id', 'user_id', 'konten', 'isPositive'];
    public $timestamps = false;

}
