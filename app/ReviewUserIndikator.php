<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewUserIndikator extends Model
{
    protected $table = 'review-user_indikator';
    protected $fillable = ['review-user_id', 'indikator_id', 'nilai'];
    public $timestamps = false;

    public function indikator()
    {
      return $this->belongsTo('App\Indikator');
    }
}
