<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indikator extends Model
{
    protected $table = 'indikator';
    public $timestamps = false;
    protected $fillable = ['nama', 'deskripsi'];

    public function review()
    {
      return $this->belongsToMany('App\Review', 'review_indikator');
    }
}
