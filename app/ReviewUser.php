<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Review;

class ReviewUser extends Model
{
    protected $table = 'review-user';
    protected $fillable = ['review_id', 'user_id', 'konten', 'status','isRecommended'];

    public function indikator()
    {
      return $this->hasMany('App\ReviewUserIndikator','review-user_id');
    }

    public function fitur()
    {
      return $this->hasMany('App\ReviewUserFitur','review-user_id');
    }

    public function reviewimage()
    {
      return $this->hasMany('App\ReviewUserImage','review-user_id');
    }

    public static function getTotalRating($review){
      $i=0;
      $total_nilai = 0;
      foreach($review->indikator()->get() as $key){
        $total_nilai += $key->nilai;
        $i++;
      }
      return $rata2nilai = ($total_nilai/$i);
    }

    public static function relatedProduct($review_id,$user_id){
      $relateds = Review::select('id','judul','urlname','kategori_id')->where('user_id','like','%'.$user_id.'%')->where('id','like','%'.$review_id.'%')->latest()->limit(4)->get();

      $new_array = array();

      foreach($relateds as $rel){
        $rel->rating = ReviewUser::getTotalRating($rel);
        $rel->image = $rel->reviewimage()->where('isThumbnail',1)->first()->name;
      }

      return $relateds;
    }

    public static function getLatest($id_review){
      $review =  ReviewUser::latest()->where('review_id',$id_review)->limit(5)->get();
      $ar = array();
      $ar2 = array();
      foreach($review as $rUser){
        $total_nilai = 0;
        $count_r = 0;
        foreach($rUser->indikator()->get() as $rIndikator){
          $new = array(
            'nama' => $rIndikator->indikator()->first()->nama,
            'nilai' => $rIndikator->nilai,
            'indikator_id' =>$rIndikator->indikator_id,
          );
          array_push($ar2,$new);
          $total_nilai += $rIndikator->nilai;
          $count_r++;
        }
        $user = User::find($rUser->user_id)->firstOrFail();
        $zz['user'] = $user;
        $zz['review'] = $rUser;
        $zz['total'] = $total_nilai/$count_r;
        $zz['rating'] = $ar2;
        array_push($ar,$zz);
      }
      return $ar;
    }
}
