<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $fillable = ['thumbnail', 'judul','urlname', 'user_id', 'kategori_id', 'konten', 'tagar', 'brand_id', 'harga','harga2'];

    public function reviewUser(){
      return $this->hasMany('App\ReviewUser');
    }

    public function kategori()
    {
      return $this->belongsTo('App\Kategori');
    }

    public function brand()
    {
      return $this->belongsTo('App\Brands');
    }
    
    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function indikator()
    {
      return $this->hasMany('App\ReviewIndikator');
    }

    public function fitur()
    {
      return $this->hasMany('App\Fitur');
    }

    public function reviewimage()
    {
      return $this->hasMany('App\ReviewImage');
    }

    public function subkategori()
    {
      return $this->belongsToMany('App\Subkategori', 'review_subkategori');
    }

    public static function newReview($item){
      $i = 1;
      $ar = array();
      $ar2 = array();
      $reviews = Review::latest()->limit($item)->get();
      foreach($reviews as $review){
        $total_nilai = 0;
        if($i == 1){
          $count_r = 0;
          foreach($review->indikator()->get() as $key){
            $new = array(
              'nama' => $key->indikator()->first()->nama,
              'nilai' => $key->nilai,
              'indikator_id' =>$key->indikator_id,
            );
            array_push($ar2,$new);
            $total_nilai += $key->nilai;
            $count_r++;
          }
          $zz['review'] = $review;
          $zz['total'] = $total_nilai/$count_r;
          $zz['rating'] = $ar2;
          array_push($ar,$zz);
          $i++;
        }else{
          // Get Total Rating
          $count_r = 0;
          foreach($review->indikator()->get() as $key){
            $total_nilai += $key->nilai;
            $count_r++;
          }
          $ss['review'] = $review;
          $ss['total'] = $total_nilai/$count_r;
          array_push($ar,$ss);
        }
        $i++;
      }
      return $ar;
    }

    public static function getTotalRating($review){
      $i=0;
      $total_nilai = 0;
      foreach($review->indikator()->get() as $key){
        $total_nilai += $key->nilai;
        $i++;
      }
      return $rata2nilai = ($total_nilai/$i);
    }

    public static function relatedProduct($user_id,$kategori_id,$brand_id){
      $relateds = Review::select('id','judul','urlname','kategori_id')->where('user_id','like','%'.$user_id.'%')->where('kategori_id','like','%'.$kategori_id.'%')->where('brand_id','like','%'.$brand_id.'%')->latest()->limit(4)->get();

      $new_array = array();

      foreach($relateds as $rel){
        $rel->rating = Review::getTotalRating($rel);
        $rel->image = $rel->reviewimage()->where('isThumbnail',1)->first()->name;
      }

      return $relateds;
    }

    public static function getEditIndikator($indikator){
      $in_array = array();
      foreach ($indikator as $key) {
        $data = array(
          'Indikator' => $key->indikator_id,
          'Nilai' => $key->nilai
        );
        array_push($in_array,$data);
      }
      return json_encode($in_array);
    }

    public static function getEditFitur($fitur){
      $in_array = array();
      foreach ($fitur as $key) {
        $data = array(
          'Deskripsi' => $key->konten,
          'Tipe' => $key->isPositive
        );
        array_push($in_array,$data);
      }
      return json_encode($in_array);
    }
}
