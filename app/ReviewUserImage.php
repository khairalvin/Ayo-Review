<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewUserImage extends Model
{
    protected $table = 'review-user_images';
    protected $fillable = ['review-user_id', 'user_id', 'name', 'isThumbnail'];
    public $timestamps = false;
}
