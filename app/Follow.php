<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = 'following';
    protected $fillable = ['id_user', 'id_user_follow'];

    public static function get_following($id)
    {
        return Follow::join('users', 'following.id_user_follow', '=', 'users.id')->where('id_user',$id)->select('users.id', 'users.biodata', 'users.username', 'users.first_name', 'users.last_name', 'users.img','users.gender', 'users.provider')->get();
    }

    public static function get_followers($id)
    {
        return Follow::join('users', 'following.id_user', '=', 'users.id')->where('id_user_follow',$id)->select('users.id', 'users.biodata', 'users.username', 'users.first_name', 'users.last_name', 'users.img','users.gender','users.provider')->get();
    }

    public static function check_follow($id_user,$id_user_follow){
        $check = Follow::where('id_user_follow',$id_user_follow)->where('id_user',$id_user)->first();
        if($check == NULL){
            return false;
        }else{
            return true;
        }
    }

    public static function countFollower($id){
        $count = Follow::where('id_user_follow',$id)->count();
        return $count;
    }

    public static function countFollowing($id){
        $count = Follow::where('id_user',$id)->count();
        return $count;
    }
    
}
