<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewSubkategori extends Model
{
    protected $table = 'review_subkategori';
    protected $fillable = ['review_id', 'subkategori_id'];
    public $timestamps = false;

    public static function deleteSubReview($id){
        ReviewSubkategori::where('review_id',$id)->delete();
    }
}
