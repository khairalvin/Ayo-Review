<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table = 'iklan';
    public $timestamps = false;
    protected $fillable = ['nama','jenis','tempat','pengaju'];
}
