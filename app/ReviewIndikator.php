<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewIndikator extends Model
{
    protected $table = 'review_indikator';
    protected $fillable = ['review_id', 'indikator_id', 'nilai'];
    public $timestamps = false;

    public static function sumValue($id){
        $value = ReviewIndikator::where('review_id',$id);
        $total = $value->sum('nilai')/$value->count();
        return $total;
    }

    public function indikator()
    {
      return $this->belongsTo('App\Indikator');
    }
}
