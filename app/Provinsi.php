<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi_tbl';
    protected $fillable = ['kode', 'nama', 'singkatan'];

    public static function provinsi($id)
    {
        return Provinsi::where('kode',$id)->firstOrFail()->nama;
    }
}
