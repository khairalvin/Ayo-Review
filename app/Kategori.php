<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    public $timestamps = false;
    protected $fillable = ['nama', 'status', 'deskripsi','img','urlname'];

    public function subkategori()
    {
      return $this->hasMany(Subkategori::class,'id_kat');
    }

    public static function getName($id)
    {
        return Kategori::where('id', $id)->firstOrFail()->nama;
    }
    
    public function review()
    {
      return $this->hasMany('App\Review');
    }
}
