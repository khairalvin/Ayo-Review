<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fitur extends Model
{
    protected $table = 'fitur';
    public $timestamps = false;
    protected $fillable = ['review_id', 'user_id', 'konten', 'isPositive'];

    public function review()
    {
      return $this->belongsTo('App\Review');
    }
}
