<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';
    protected $fillable = [
        'judul', 'urlname','isActive', 'status',
        'konten', 'kategori_id', 'user_id',
        'tags','img','urutan_topnews','isActive',
    ];

    public static function get_latest(){
        return Berita::latest()->limit(7)->get();
    }

    public static function get_populer(){
        return Berita::orderBy('viewer','desc')->limit(4)->get();
    }

    public static function get_topnews(){
        return Berita::orderBy('urutan_topnews','asc')->limit(4)->get();
    }

    public static function recommendNews(){
        return Berita::orderBy('viewer','asc')->limit(5)->get();
    }

    public static function nextNews($kategori){
        return Berita::where('kategori_id',$kategori)->latest()->limit(3)->get();
    }

    public static function search($param){
        return Berita::select('id','judul','img')->where('judul','like','%'.$param.'%')->orderBy('judul', 'asc')->limit(5)->get();
    }
}
