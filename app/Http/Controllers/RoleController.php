<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis="Tambah";
        return view('admin.role.form', compact('jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role(array(
            'name' => $request->name,
            'display_name' => $request->display_name,
            'description' => $request->description,
        ));
    
        $role->save();
        return redirect('/role')->with('status', 'Role Baru Telah Dibuat');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis="Edit";
        $role = Role::where('id', '=', $id)->firstOrFail();

        return view('admin.role.form', compact('jenis','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->name         = $request->name;
        $role->display_name = $request->display_name;
        $role->description  = $request->description;

        $role->save();

        return redirect('/role')->with('status', 'Role Baru Telah Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::where('id', '=', $id)->firstOrFail();

        $role->delete();

        return redirect('/role')->with('status', 'Role dihapus');
    }

    // Role Mapping Index
    public function indexMapping()
    {
        $roles = Role::showAll();

        return view('admin.role.index_mapping', compact('roles'));
    }

    public function editMapping($user_id,$role_id)
    {
        $user = User::findOrFail($user_id);
        $roles = Role::all();
        return view('admin.role.form_mapping', compact('user','roles','role_id'));
    }

    public function updateMapping(Request $request,$user_id,$role_id)
    {
        $id_role = $request->role_id;
        Role::updateRole($user_id,$id_role,$role_id);
        return redirect('/role/mapping')->with('status', 'Role diubah');
    }
}
