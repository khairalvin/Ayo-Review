<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brands::all();
        return view('admin.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $action = route('admin-brand-create');
        return view('admin.brand.form', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brands = new Brands(array(
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'urlname' =>   str_replace(' ','-',strtolower($request->nama)),
        ));
        $brands->save();
        return redirect('/brands')->with('status', 'Brand ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brands::where('id', '=', $id)->firstOrFail();
        $action = route('admin-brand-edit', $id);
        return view('admin.brand.form', compact('brand','action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $brand = Brands::where('id', '=', $id)->firstOrFail();

            $brand->nama = $request->nama;
            $brand->deskripsi = $request->deskripsi;
            $brand->urlname = str_replace(' ','-',strtolower($request->nama));

            $brand->save();

        return redirect('/brands')->with('status', 'Brands diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brands::where('id', '=', $id)->firstOrFail();
        $brand->delete();
        return redirect('/brands')->with('status', 'Brands dihapus');
    }
}
