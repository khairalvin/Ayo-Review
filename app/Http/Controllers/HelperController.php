<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Kategori;
use App\Subkategori;
use App\Brands;

class HelperController extends Controller
{
    public function brand(Request $request)
    {
        $term = $request->query('term');

        return Brands::where('nama', 'LIKE', $term)->get();
    }

    public function kategori(Request $request)
    {
        $term = $request->query('term');

        return Kategori::where('nama', 'LIKE', $term)->get();
    }

    public function subkategori($id, Request $request)
    {
        $term = $request->query('term');

        return Subkategori::where('id_kat', $id)->where('subkategori', 'LIKE', $term)->get();
    }
}
