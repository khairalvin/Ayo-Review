<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iklans = Iklan::all();
        return view('admin.iklan.index', compact('iklans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis="create";
        return view('admin.berita.form', compact('jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $iklan = new Iklan(array(
            'nama' => $request->nama,
            'jenis' => $request->jenis,
            'tempat' => $request->tempat,
            'pengaju' => $request->pengaju,
        ));
    
        $iklan->save();
        return redirect('/iklan')->with('status', 'Iklan ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis="edit";
        $iklan = Iklan::where('id', '=', $id)->firstOrFail();

        return view('admin.iklan.form', compact('jenis','iklan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $iklan = Iklan::where('id', '=', $id)->firstOrFail();
        $iklan->nama = $request->nama;
        $iklan->jenis = $request->jenis;
        $iklan->tempat = $request->tempat;
        $iklan->pengaju = $request->pengaju;

        $iklan->save();

        return redirect('/iklan')->with('status', 'Iklan Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $iklan = Iklan::where('id', '=', $id)->firstOrFail();

        $iklan->delete();

        return redirect('/iklan')->with('status', 'Iklan dihapus');
    }
}
