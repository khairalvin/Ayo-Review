<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Review;
use App\ReviewIndikator;

class KategoriUserController extends Controller
{
    public function index(){

    }

    public function detail($id){
        $id_kat = Kategori::where('urlname',$id)->first()->id;
        $reviews = Review::where('kategori_id',$id_kat)->get();
        return view('user.kategori.detail.main_kat',compact('reviews'));
    }
}
