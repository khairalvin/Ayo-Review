<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review;
use App\ReviewImage;
use App\ReviewIndikator;
use App\Kategori;
use App\Indikator;
use App\Fitur;
use App\ReviewSubkategori;

use Auth;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::all();
        return view('admin.review.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route('admin-review-create');
        $review = new Review();
        $kategories = Kategori::all();
        $indikators = Indikator::all();
        $fiturs = Fitur::all();
        return view('admin.review.form', compact('action', 'review', 'kategories', 'indikators', 'fiturs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # Step 0: Initiate Variable        
        $indikator = json_decode($request->indikatorPenilaian[0]);
        $fitur = json_decode($request->kelebihanKekurangan[0]);

        # Step 1: Save Data Review
        $review = new Review(array(
            'judul' => $request->judul,
            'urlname' => str_replace(' ','-',strtolower($request->judul)),
            'user_id' => Auth::user()->id,
            'kategori_id' => $request->kategori,
            'konten' => $request->deskripsi,
            'tagar' => $request->tags,
            'harga' => $request->harga1,
            'harga2' => $request->harga2,
            'brand_id' => $request->brand
        ));
    
        $review->save();

        # Step 2: Save All Subkategori
        $review->subkategori()->attach($request->subkategori);

        # Step 3: Save All Indikator
        foreach ($indikator as $i) {
            # Save to database
            $reviewIndikator = new ReviewIndikator(array(
                'review_id' => $review->id,
                'indikator_id' => $i->Indikator,
                'nilai' => $i->Nilai
            ));
        
            $reviewIndikator->save();
        }

        # Step 4: Save Kelebihan Kekurangan
        foreach ($fitur as $f) {
            # Save to database
            $reviewFitur = new Fitur(array(
                'review_id' => $review->id,
                'user_id' => Auth::user()->id,
                'konten' => $f->Deskripsi,
                'isPositive' => $f->Tipe
            ));
        
            $reviewFitur->save();
        }

        # Step 5: Save All Image Media
        $countImg = 1;
        foreach ($request->gambar as $image) {
            # Move all file
            $imgNames = $image->getClientOriginalName();
            $image->move(public_path('admin/img/review/'),$imgNames);

            # Save to database
            if($countImg == 1){
                $reviewImage = new ReviewImage(array(
                    'review_id' => $review->id,
                    'user_id' => Auth::user()->id,
                    'name' => $imgNames,
                    'isThumbnail' => 1,
                ));
            }else{
                $reviewImage = new ReviewImage(array(
                    'review_id' => $review->id,
                    'user_id' => Auth::user()->id,
                    'name' => $imgNames,
                    'isThumbnail' => 0,
                ));
            }
            $countImg++;
            $reviewImage->save();
        }
        
        # Step 6: Redirect with Message
        return redirect('/review')->with('status', 'Review ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route('admin-review-edit', $id);
        $review = Review::where('id', '=', $id)->firstOrFail();
        $review['subkategori'] = $review->subkategori()->get();
        $review['indikator'] = Review::getEditIndikator($review->indikator()->get());
        $review['brand'] = $review->brand()->first();
        $review['fitur'] = Review::getEditFitur(Fitur::where('review_id', $review->id)->get());
        $review['gambar'] = ReviewImage::where('review_id', $review->id)->get();

        $kategories = Kategori::all();
        $indikators = Indikator::all();
        $fiturs = Fitur::all();

        return view('admin.review.form', compact('action', 'review', 'kategories', 'indikators', 'fiturs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $indikator = json_decode($request->indikatorPenilaian[0]);
        $fitur = json_decode($request->kelebihanKekurangan[0]);

        // Update to Review
        $review = Review::where('id', '=', $id)->firstOrFail();
        $review->judul = $request->judul;
        $review->user_id = Auth::user()->id;
        $review->kategori_id = $request->kategori_id;
        $review->konten = $request->konten;
        $review->tagar = ($request->tags).toString();
        $review->brand_id = $request->brand;
        $review->harga = $request->harga1;
        $review->harga2 = $request->harga2;

        $review->save();

        // Update Subkategori
            // Delete old Subkategori
                $review->subkategori()->detach(['review_id' => $review->id]);
            // Insert New
                $review->subkategori()->attach($request->subkategori);

        // Update Gambar
            // Delete Old Pic
                // get data old pic
                $images = ReviewImage::where('review_id', $review->id)->get();
                // Delete it
                foreach($images as $imgs){
                    unlink(public_path('admin/img/review/').$imgs);
                    $imgs->delete();
                }
            // Insert New
                $countImg = 1;
                foreach ($request->gambar as $image) {
                    # Move all file
                    $imgNames = $image->getClientOriginalName();
                    $image->move(public_path('admin/img/review/'),$imgNames);
        
                    # Save to database
                    if($countImg == 1){
                        $reviewImage = new ReviewImage(array(
                            'review_id' => $review->id,
                            'user_id' => Auth::user()->id,
                            'name' => $imgNames,
                            'isThumbnail' => 1,
                        ));
                    }else{
                        $reviewImage = new ReviewImage(array(
                            'review_id' => $review->id,
                            'user_id' => Auth::user()->id,
                            'name' => $imgNames,
                            'isThumbnail' => 0,
                        ));
                    }
                    $countImg++;
                    $reviewImage->save();
                }

        // Update Fitur
            #delete first
            $dFitur = Fitur::where('review_id',$review->id)->delete();
            #insert new
            foreach ($fitur as $f) {
                # Save to database
                $reviewFitur = new Fitur(array(
                    'review_id' => $review->id,
                    'user_id' => Auth::user()->id,
                    'konten' => $f->Deskripsi,
                    'isPositive' => $f->Tipe
                ));
            
                $reviewFitur->save();
            }


        return redirect('/review')->with('status', 'Review Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete 
        $review = Review::where('id', '=', $id)->firstOrFail();

        $review->delete();

        return redirect('/review')->with('status', 'Review dihapus');
    }
}
