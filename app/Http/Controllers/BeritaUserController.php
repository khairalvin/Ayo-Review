<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

class BeritaUserController extends Controller
{
    public function index(){
        $news_latest = Berita::get_latest();
        $news_populer = Berita::get_populer();
        $news_top = Berita::get_topnews();
        $recommendNews = Berita::recommendNews();
        return view('user.berita.main_list',compact('news_latest','news_populer','news_top','recommendNews'));
    }

    public function detail($id){
        $news = Berita::where('id', '=', $id)->firstOrFail();
        $nextNews = Berita::nextNews($news->kategori_id);
        $recommendNews = Berita::recommendNews();
        return view('user.berita.detail.main_detail',compact('news','nextNews','recommendNews'));
    }
}
