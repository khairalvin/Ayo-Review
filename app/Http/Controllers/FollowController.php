<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;

class FollowController extends Controller
{
    public function follow(Request $request){
        $follow = new Follow(array(
            'id_user' => $request->id_user,
            'id_user_follow' => $request->id_user_follow,
        ));
        $follow->save();
    }
    
    public function unfollow(Request $request){
        $follow = new Follow(array(
            'id_user' => $request->id_user,
            'id_user_follow' => $request->id_user_follow,
        ));
        $follow = Follow::where('id_user_follow',$request->id_user_follow)->where('id_user',$request->id_user)->firstOrFail();
        $follow->delete();
    }
}
