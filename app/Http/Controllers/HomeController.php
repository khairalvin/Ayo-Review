<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Berita;
use App\User;
use App\Review;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = json_decode (json_encode (Review::newReview(9)), FALSE);
        $beritas = Berita::get_latest();
        return view('user/home/home',compact('beritas','reviews'));
    }

    public function aboutus()
    {
        return view('user/about_us/main');
    }

    public function syaratketentuan()
    {
        return view('user/about_us/syarat-ketentuan');
    }

    public function kebijakanprivasi()
    {
        return view('user/about_us/kebijakan-privasi');
    }

    public function logout(){
        Auth::logout();
    
        session()->flash('message', 'Some goodbye message');
    
        return redirect('/');
    }

    public function search(Request $request){
        $param = $request->param;
        // Get Akun
        $akun = User::search($param);
        // Get Berita
        $berita = Berita::search($param);
        // Get Review

        $data = array(
            'akun' => $akun,
            'berita' => $berita
        );

        return json_encode($data);
    }
}
