<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Subkategori;

class SubkategoriUserController extends Controller
{
    public function index(){
        $subcategories = Kategori::all();
        return view('user.subkategori.main', compact('subcategories'));
    }
}
