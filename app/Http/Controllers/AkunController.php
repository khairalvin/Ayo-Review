<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class AkunController extends Controller
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
    }

    public function change_password(Request $request)
    {
        $validator = Validator::make($this->request->all(), [
            'current_password' => 'required|min:6',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6',
        ]);
  
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
            ]);
        }else{
            if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
                // The passwords matches
                return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            }
    
            if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }
        
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new_password'));
            $user->save();
        
            return redirect()->back()->with("success","Password changed successfully !");
        }
    }

    public function edit(Request $request)
    {   
        $validator = Validator::make($this->request->all(), [
            'username' => 'required|max:25',
            'first_name' => 'required|max:25',
            'email' => 'required|max:100',
            'gender' => 'required',
            'tanggal_lahir' => 'required',
        ]);
  
        if ($validator->fails()) {
            return redirect()->back()->with(["success"=>"Data Anda masih ada yang belum terisi"]);
        }else{
            $user = User::findOrFail(Auth::user()->id);


            if($request->img <> NULL|| $request->img <> ''){
                if (file_exists(public_path('user/images/user_profile/').$user->img) && $user->img <> NULL) {
                    unlink(public_path('user/images/user_profile/').$user->img);
                }

                $imgNames = $request->username.'.'.$request->img->getClientOriginalExtension();
                $request->img->move(public_path('user/images/user_profile/'),$imgNames);
                $user->img = $imgNames;
            }

            $user->username = $request->username;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->gender = $request->gender;
            $user->tanggal_lahir = $request->tanggal_lahir;
            $user->email = $request->email;
            $user->region = $request->region;
            $user->web = $request->web;
            $user->hp = $request->hp;
            $user->biodata = $request->biodata;
            
            $user->save();
            return redirect()->back()->with("success","Akun anda berhasil diubah!");
        }
    }
}
