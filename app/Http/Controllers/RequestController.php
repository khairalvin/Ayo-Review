<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Saran;

class RequestController extends Controller
{
    public function sendSaran(Request $request){
        $saran = new Saran(array(
            'id_user' => Auth::id(),
            'isi' => $request->saran,
            'status' => "N",
            'tipe' => "saran",
        ));
        $saran->save();

        return redirect()->back()->with("success","Saran anda Telah dikirimkan!");
    }

    public function getSaran(){
        $sarans = Saran::where('tipe','saran')->get();
        return view('admin/saran/saran',compact('sarans'));
    }

    public function sendRequest(Request $request){
        $reqquest = new Saran(array(
            'id_user' => Auth::id(),
            'isi' => $request->item,
            'status' => "N",
            'tipe' => "request",
            'kategori_id' => $request->kategori,
        ));
        $reqquest->save();

        return redirect()->back()->with("success","Request anda Telah dikirimkan!");
    }

    public function getRequest(){
        $requests = Saran::where('tipe','request')->get();
        return view('admin/saran/request',compact('requests'));
    }

    public function toRead(Request $request){
        Saran::toRead($request->id);
        $saran = Saran::find($request->id);
        if($saran->tipe == "saran"){
            return view('admin/saran/modal_saran',compact('saran'));
        }elseif ($saran->tipe == "request") {
            return view('admin/saran/modal_request',compact('saran'));
        }
    }
}
