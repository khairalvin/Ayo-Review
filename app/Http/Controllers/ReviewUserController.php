<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\ReviewIndikator;
use App\ReviewUser;
use App\ReviewUserFitur;
use App\ReviewUserImage;
use App\ReviewUserIndikator;
use App\User;

use Auth;

class ReviewUserController extends Controller
{
    public function index(){
        return view('user.review.main.main_review');
    }

    // DISPLAY HALAMAN FULL REVIEW
    public function reviewadmin($id){
        // Get Data Review
        $review = Review::where('urlname',$id)->firstOrFail();
        $review_user = $review->reviewUser()->get();

        // Get Rating
        $ratings = array();
        $total_nilai = 0;
        $i = 0;
        foreach($review->indikator()->get() as $key){
            $new = array(
                'nama' => $key->indikator()->first()->nama,
                'nilai' => $key->nilai,
                'indikator_id' =>$key->indikator_id,
            );
            array_push($ratings,$new);
            $total_nilai += $key->nilai;
            $i++;
        }

        $rata2nilai = ($total_nilai/$i);

        if($rata2nilai >= 9 ){
            $keterangan = "Sangat Bagus";
        }elseif($rata2nilai >= 8.5){
            $keterangan = "Bagus";
        }elseif($rata2nilai >= 7){
            $keterangan = "Menarik";
        }elseif($rata2nilai >= 5){
            $keterangan = "Kurang Menarik";
        }else{
            $keterangan = "Tidak Menarik";
        }
        $total_rating = array(
            'nilai' =>$rata2nilai,
            'keterangan' =>$keterangan,
        );

        // GET KEUNGGULAN DAN KEKURANGAN
        $fitur_kekurangan = $review->fitur()->where('isPositive',0)->get();
        $fitur_keunggulan = $review->fitur()->where('isPositive',1)->get();

        // GET IMAGES
        $images = $review->reviewimage()->get();
        $image = $review->reviewimage()->where('isThumbnail',1)->first()->name;

        // GET RELATED REVIEW
        $relateds = Review::relatedProduct($review->user_id,$review->kategori_id,$review->brand_id);

        // GET Related Review by User
        $latest = json_decode (json_encode (ReviewUser::getLatest($review->id)), FALSE);
        
        return view('user.review.main.main_review',compact('review','ratings','total_rating','fitur_kekurangan','fitur_keunggulan','images','relateds','image','latest'));
    }

    // STORE DATA REVIEW USER
    public function storeUser(Request $request){
        $username = Auth::user()->username;
        // STORE REVIEW USER
        $reviewUser = new ReviewUser(array(
            'review_id' => $request->review_id,
            'user_id' => Auth::user()->id,
            'konten' => $request->editor,
            'status' => 'enable',
            'isRecommended' => $request->recommend,
        ));
    
        $reviewUser->save();

        // STORE RATING FROM USER
        $review = Review::find($request->review_id);
        foreach($review->indikator()->get() as $key){

            $reviewUserIndikator = new ReviewUserIndikator(array(
                'review-user_id' => $reviewUser->id,
                'indikator_id' => $key->indikator_id,
                'nilai' => $request->input('rating-'.$key->indikator_id)
            ));
        
            $reviewUserIndikator->save();
        }
        // STORE PLUS AND MINUS

        // STORE PLUS
        $resultpro = $request->resultPros;
        foreach($resultpro as $pro){
            $userFiturPro = new ReviewUserFitur(array(
                'review-user_id' => $reviewUser->id,
                'user_id' => Auth::user()->id,
                'konten' => $pro,
                'isPositive' => 1,
            ));
        
            $userFiturPro->save();
        }
        // STORE MINUS
        $resultcon = $request->resultCons;
        foreach($resultcon as $con){
            $userFiturCon = new ReviewUserFitur(array(
                'review-user_id' => $reviewUser->id,
                'user_id' => Auth::user()->id,
                'konten' => $con,
                'isPositive' => 0,
            ));
        
            $userFiturCon->save();
        }

        // STORE IMAGES
        $countImg = 1;
        foreach ($request->fileInput as $image) {
            # Move all file
            $imgNames = $image->getClientOriginalName();
            $image->move(public_path('admin/img/review/user'),$imgNames);

            # Save to database
            if($countImg == 1){
                $reviewUserImage = new ReviewUserImage(array(
                    'review-user_id' => $reviewUser->id,
                    'user_id' => Auth::user()->id,
                    'name' => $imgNames,
                    'isThumbnail' => 1,
                ));
            }else{
                $reviewUserImage = new ReviewUserImage(array(
                    'review-user_id' => $reviewUser->id,
                    'user_id' => Auth::user()->id,
                    'name' => $imgNames,
                    'isThumbnail' => 0,
                ));
            }
            $countImg++;
            $reviewUserImage->save();
        }

        // GO TO REVIEW USER PAGE
        return redirect('reviewadmin/'.$review->urlname.'/'.$username);
    }

    public function getReviewUser($review_id,$username){
        // Get REVIEW BASED ON REVIEW URLNAME
        $review = Review::where('urlname',$review_id)->firstOrFail();

        // Get User Id based On Username
        $user = User::where('username',$username)->firstOrFail();

        // Get Review User Based on Review ID and User Id
        $reviewUser = ReviewUser::where('review_id',$review->id)->where('user_id',$user->id)->firstOrFail();
        
        // Get Rating User
        $rating_user = array();
        $total_nilai_user = 0;
        $i = 0;
        foreach($reviewUser->indikator()->get() as $key){
            $new = array(
                'nama' => $key->indikator()->first()->nama,
                'nilai' => $key->nilai,
                'indikator_id' =>$key->indikator_id,
            );
            array_push($rating_user,$new);
            $total_nilai_user += $key->nilai;
            $i++;
        }

        $rata2nilai_user = ($total_nilai_user/$i);

        // Get Rata-Rata User Review
        $meanIndikator = array();
        $indikator = 0;
        $t_indikator = 0;
        $c_indikator = 0;

        foreach($review->indikator()->get() as $keyReview){
            $namaIndikator = $keyReview->indikator()->first()->nama;
            foreach($review->reviewUser()->get() as $keyReviewUser){
                $indikator += $keyReviewUser->indikator()->where('indikator_id',$keyReview->indikator_id)->firstOrFail()->nilai;
            }
            $sum_indikator = array(
                'nama' => $namaIndikator,
                'nilai' => $indikator
            );
            array_push($meanIndikator,$sum_indikator);
            $t_indikator += $indikator;
            $indikator = 0;
            $c_indikator++;
        }
        $rataTIndikator =$t_indikator/$c_indikator;

        if($rataTIndikator >= 9 ){
            $keterangan = "Sangat Bagus";
        }elseif($rataTIndikator >= 8.5){
            $keterangan = "Bagus";
        }elseif($rataTIndikator >= 7){
            $keterangan = "Menarik";
        }elseif($rataTIndikator >= 5){
            $keterangan = "Kurang Menarik";
        }else{
            $keterangan = "Tidak Menarik";
        }
        $total_ratingIndikator = array(
            'nilai' =>$rataTIndikator,
            'keterangan' =>$keterangan,
        );

        // GET KEUNGGULAN DAN KEKURANGAN
        $fitur_kekurangan = $reviewUser->fitur()->where('isPositive',0)->get();
        $fitur_keunggulan = $reviewUser->fitur()->where('isPositive',1)->get();

        // GET IMAGES
        $images = $review->reviewimage()->get();
        $image = $review->reviewimage()->where('isThumbnail',1)->first()->name;

        // GET RELATED REVIEW
        $relateds = ReviewUser::relatedProduct($review->id,$reviewUser->user_id);

        return view('user.review.user.mainuser',compact('review','user','reviewUser','rating_user','rata2nilai_user','meanIndikator','total_ratingIndikator','fitur_kekurangan','fitur_keunggulan','images','image','relateds'));
    }
}
