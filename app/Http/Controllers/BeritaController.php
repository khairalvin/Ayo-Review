<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Berita;
use App\Kategori;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beritas = Berita::all();
        return view('admin.berita.index', compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis="create";
        $categories = Kategori::all();
        return view('admin.berita.form', compact('categories','jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgNames = $request->judul.'.'.$request->img->getClientOriginalExtension();
        $request->img->move(public_path('admin/img/berita_thumb/'),$imgNames);
        $news = new Berita(array(
            'judul' => $request->judul,
            'urlname' => str_replace(' ','-',strtolower($request->judul)),
            'img' => $imgNames,
            'status' => "oke",
            'konten' => $request->konten,
            'kategori_id' => $request->kategori,
            'user_id' => Auth::id(),
            'tags' => implode(",",$request->ms1),
            'isActive' => 1,
        ));
        $news->save();
        return redirect('/berita')->with('status', 'Berita ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis="edit";
        $categories = Kategori::all();
        $news = Berita::where('id', '=', $id)->firstOrFail();

        return view('admin.berita.form', compact('categories','jenis','news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->img == NULL){
            $news = Berita::where('id', '=', $id)->firstOrFail();

            $news->judul = $request->judul;
            $news->urlname = str_replace(' ','-',strtolower($request->judul));
            $news->status = "oke";
            $news->konten = $request->konten;
            $news->kategori_id = $request->kategori;
            $news->tags = "#NNN";
            $news->user_id = Auth::id();

            $news->save();
        }else {
            $news = Berita::where('id', '=', $id)->firstOrFail();
            unlink(public_path('admin/img/berita_thumb/').$news->img);

            $imgNames = $request->judul.'.'.$request->img->getClientOriginalExtension();
            $request->img->move(public_path('admin/img/berita_thumb/'),$imgNames);
            $news->judul = $request->judul;
            $news->urlname = str_replace(' ','-',strtolower($request->judul));
            $news->status = "oke";
            $news->konten = $request->konten;
            $news->kategori_id = $request->kategori;
            $news->tags = "#NNN";
            $news->img = $imgNames;
            $news->user_id = Auth::id();

            $news->save();
        }

        return redirect('/berita')->with('status', 'Berita berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = Berita::where('id', '=', $id)->firstOrFail();
        unlink(public_path('admin/img/berita_thumb/').$news->img);
        $news->delete();

        return redirect('/berita')->with('status', 'Berita ditambahkan');
    }

    public function top_news(){
        $news = Berita::get_topnews();
        return view('admin.berita.top_news',compact('news'));
    }

    public function top_news_post(Request $request){ 
        for($i = 1; $i < 5; $i++){
            $id = $request->get('berita'.$i);

            $news = Berita::where('id', '=', $id)->firstOrFail();
            $news->urutan_topnews = $i;
            $news->save();
        }
        return redirect()->back()->with("success","Topnews Telah diatur!");
    }

    public function search(){
        $judul = strip_tags(trim($_GET['judul']));
		$query = Berita::search($judul);
		$data = array();
		foreach ($query as $key) {
			$arrayName = array('id' =>$key->id,'text' =>$key->judul);
			array_push($data,$arrayName);
		}
		$myJSON = json_encode($data);

        echo $myJSON;
    }
}
