<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kategori;
use App\Subkategori;

class SubkategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = DB::select("SELECT subkategori.id,subkategori,nama,subkategori.status,subkategori.deskripsi FROM subkategori INNER JOIN kategori ON subkategori.id_kat=kategori.id");
        return view('admin.subkategori.index',compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis = "create";
        $categories = Kategori::all();
        return view('admin.subkategori.form',compact('categories','jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgNames = $request->nama.'.'.$request->img->getClientOriginalExtension();
        $request->img->move(public_path('admin/img/subkategori/'),$imgNames);
        $sub = new Subkategori(array(
            'subkategori' => $request->subkategori,
            'status' => "TOP AE",
            'deskripsi' => $request->deskripsi,
            'id_kat' => $request->kategori,
            'img' => $imgNames,
            'urlname' => str_replace(' ','-',strtolower($request->subkategori)),
        ));

        $sub->save();
        return redirect('/subkategori')->with('status', 'Kategori ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subkategori = Subkategori::where('id', '=', $id)->firstOrFail();
        $categories = Kategori::all();
        $jenis = "edit";
        return view('admin.subkategori.form', compact('subkategori','jenis','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub = Subkategori::where('id', '=', $id)->firstOrFail();

        $sub->subkategori = $request->subkategori;
        $sub->status = "TOP AEEE";
        $sub->deskripsi = $request->deskripsi;
        $sub->id_kat = $request->kategori;
        $sub->urlname = str_replace(' ','-',strtolower($request->subkategori));

        $sub->save();

        return redirect('/subkategori')->with('status', 'Kategori diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub = Subkategori::where('id', '=', $id)->firstOrFail();
        unlink(public_path('/images/Kategori/Subkategori').$sub->img);
        $sub->delete();

        return redirect('/subkategori')->with('status', 'Kategori dihapus');
    }
}
