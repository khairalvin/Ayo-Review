<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Indikator;

class IndikatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indikators = Indikator::all();
        return view('admin.indikator.index', compact('indikators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route('admin-indikator-create');
        $indikator = new Indikator();
        return view('admin.indikator.form', compact('action', 'indikator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $indikator = new Indikator(array(
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
        ));
    
        $indikator->save();
        return redirect('/indikator')->with('status', 'Indikator ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route('admin-indikator-edit', $id);
        $indikator = Indikator::where('id', '=', $id)->firstOrFail();

        return view('admin.indikator.form', compact('action', 'indikator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $indikator = Indikator::where('id', '=', $id)->firstOrFail();
        $indikator->nama = $request->nama;
        $indikator->deskripsi = $request->deskripsi;

        $indikator->save();

        return redirect('/indikator')->with('status', 'Indikator Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indikator = Indikator::where('id', '=', $id)->firstOrFail();

        $indikator->delete();

        return redirect('/indikator')->with('status', 'Indikator dihapus');
    }
}
