<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Provinsi;
use App\Follow;
// use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index($username)
    {
        $user = User::where('username',$username)->firstOrFail();
        $jenis = "profile_review";
        return view('user/profile/profile_review',compact('jenis','user'));
    }

    public function media($username)
    {
        $user = User::where('username',$username)->firstOrFail();
        $jenis = "profile_media";
        return view('user/profile/profile_media',compact('jenis','user'));
    }

    public function follower($username)
    {
        $user = User::where('username',$username)->firstOrFail();
        $jenis = "profile_followers";
        $follower = Follow::get_followers($user->id);

        return view('user/profile/profile_follower',compact('jenis','user','follower'));
    }

    public function following($username)
    {
        $user = User::where('username',$username)->firstOrFail();
        $jenis = "profile_following";
        $following = Follow::get_following($user->id);

        return view('user/profile/profile_following',compact('jenis','user','following'));
    }

    public function settings()
    {
        $jenis = "edit";
        return view('user/profile/profile_settings',compact('jenis'));
    }

    public function edit()
    {
        $user = User::where('username',Auth::user()->username)->firstOrFail();
        $provinsi = Provinsi::all();
        $jenis = "edit";
        return view('user/profile/profile_edit',compact('jenis','user','provinsi'));
    }

    public function pass()
    {
        $user = User::where('username',Auth::user()->username)->firstOrFail();
        $jenis = "edit";
        return view('user/profile/profile_changepass',compact('jenis','user'));
    }
}
 