<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Review;
use App\ReviewIndikator;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Kategori::all();
        return view('admin.kategori.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $jenis = "create";
        return view('admin.kategori.form', compact('jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgNames = $request->nama.'.'.$request->img->getClientOriginalExtension();
        $request->img->move(public_path('admin/img/kategori/'),$imgNames);
        $category = new Kategori(array(
            'nama' => $request->nama,
            'status' => "TOP NEWS",
            'deskripsi' => $request->deskripsi,
            'img' => $imgNames,
            'urlname' => str_replace(' ','-',strtolower($request->nama)),
        ));
        $category->save();
        return redirect('/kategori')->with('status', 'Kategori ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::where('id', '=', $id)->firstOrFail();
        $jenis = "edit";
        return view('admin.kategori.form', compact('kategori','jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->img == NULL){
            $category = Kategori::where('id', '=', $id)->firstOrFail();

            $category->nama = $request->nama;
            $category->urlname  = str_replace(' ','-',strtolower($request->nama));
            $category->status = "TOP NEWS";
            $category->deskripsi = $request->deskripsi;

            $category->save();
        }else {
            $category = Kategori::where('id', '=', $id)->firstOrFail();
            unlink(public_path('/images/Kategori/').$category->img);

            $imgNames = $request->nama.'.'.$request->img->getClientOriginalExtension();
            $request->img->move(public_path('images/Kategori'),$imgNames);
            $category->nama = $request->nama;
            $category->urlname  = str_replace(' ','-',strtolower($request->nama));
            $category->status = "TOP NEWS";
            $category->deskripsi = $request->deskripsi;
            $category->img = $imgNames;

            $category->save();
        }
        

        return redirect('/kategori')->with('status', 'Kategori diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Kategori::where('id', '=', $id)->firstOrFail();
        unlink(public_path('/images/Kategori/').$category->img);
        $category->delete();
        return redirect('/kategori')->with('status', 'Kategori dihapus');
    }

    public function kategorireview($id){
        $id_kat = Kategori::where('urlname',$id)->first()->id;
        $reviews = Review::where('kategori_id',$id_kat)->get();
        return view('user.kategori.detail.main_kat',compact('reviews'));
    }
}
