<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subkategori extends Model
{
    protected $table = 'subkategori';
    public $timestamps = false;
    protected $fillable = ['subkategori', 'status', 'deskripsi', 'id_kat','img','urlname'];

    public function review()
    {
      return $this->belongsToMany('App\Review', 'review_subkategori');
    }
}
